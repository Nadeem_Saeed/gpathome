Feature: Signing up
  In order to user the site
  As a user
  I want to be able to sign up

  Background:
    Given there are the following groups:
      | admin_id | plan_id |
      | 1        | 0       |
    Given there are the following users:
      | email                 | password | first_name | last_name | approved | group_id | role |
      | admin@gpathome.com    | password | James      | Duncombe  | true     | 1        | 4    |
      | practice@gpathome.com | password | James      | Duncombe  | true     | 1        | 4    |
    Given there are the following plans:
      | name       | description             | price | plan_code | status |
      | Individual | Single plan for people  | 24.99 | ind001    | 1      |
      | Couple     | Couple plan for people  | 34.99 | cpl001    | 2      |
      | Family     | Family plan for people  | 44.99 | fam001    | 1      |
    Given there are the following credit types:
      | name                |
      | Online Consultation |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 3                 |
      | 2       | 1         | 6                 |
      | 3       | 1         | 8                 |
    Given a clear email queue

  @javascript
  Scenario: Signing up - invitations only
    Given I am on the home page
    When I follow "Sign Up"
    When I follow sign up for the "individual" plan
    And I fill in "Email" with "james@jamesduncombe.com"
    And I fill in "Phone number" with "07794 187581"
    And I fill in "First name" with "James"
    And I fill in "Last name" with "Duncombe"
    And I press "Request Invite"
    And I wait 2 seconds
    Then I should see "Thank you, your request has been sent."
    And "practice@gpathome.com" opens the email with subject: "New patient requesting invite"
    Then I should see "Individual" in the email body
    Then I should see "james@jamesduncombe.com" in the email body
    Then I should see "07794 187581" in the email body
    Then I should see "James" in the email body
    Then I should see "Duncombe" in the email body

  @selenium
  Scenario: Signing up - invitations only
    Given I am on the home page
    When I follow "Sign Up"
    When I follow sign up for the "individual" plan
    And I wait 2 seconds
    And I press "Request Invite"
    And I wait 2 seconds
    Then I should see "Sorry, there was a problem"
