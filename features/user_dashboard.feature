Feature: Viewing of consultations
  In order to preserve user security
  Users should not be able to view other users consultations
  They should also be able to see if a consultation is closed or not

  Background:
    Given there are the following plans:
      | name    | description                 | price |
      | Single  | Single plan for people      | 30.00 |
      | Premier | Premier plan for 92 members | 30.00 |
    Given there are the following credit types:
      | name                | price |
      | Online Consultation | 100   |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 3                 |
      | 2       | 1         | 999               |
    Given there are the following groups:
      | admin_id | plan_id |
      | 1        | 1       |
      | 2        | 2       |
      | 3        | 0       |
    Given there are the following users:
      | email               | password | role | first_name | last_name | group_id | plan |
      | user@gpathome.com   | password | 2    | James      | Duncombe  | 1        | 1    |
      | user2@gpathome.com  | password | 2    | Test       | Person    | 2        | 2    |
      | gp@gpathome.com     | password | 3    | Justine    | Setchell  | 3        | 0    |
    Given there are the following consultations:
      | user_id | subject              |
      | 1       | Feet hurt            |
      | 2       | I've got a cold      |
      | 1       | Another consultation |
    Given there are the following messages:
      | consultation_id | user_id | message                                                   |
      | 1               | 1       | My feet really hurt, can you help?                        |
      | 2               | 1       | Hi, I've had a cold for months. What can you do about it? |
      | 3               | 1       | Another consultation message                              |
    And I am on the home page

  Scenario: Viewing main Dashboard as a normal user
    And I follow "Login"
    And I fill in "Email" with "user@gpathome.com"
    And I fill in "Password" with "password"
    And I press "Login"
    Then I should see "My feet really hurt, can you help?"
    And I should not see "Hi, I've had a cold for months. What can you do about it?"
