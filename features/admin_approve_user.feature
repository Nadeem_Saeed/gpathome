Feature: Approve user
  As an admin
  I need to be able to approve a user
  When they sign up
  
  Background:
    Given there are the following plans:
      | name    | description             | price  |
      | Single  | Single plan for people  | 300.00 |
      | Couple  | Couple plan for people  | 450.00 |
      | Family  | Family plan for people  | 650.00 |
    Given there are the following credit types:
      | name                |
      | Online Consultation |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 3                 |
    Given there are the following groups:
      | admin_id | plan_id |
      | 1        | 1       |
    Given there are the following users:
      | email				        | password | role | first_name | last_name | unapproved | group_id |
      | user@gpathome.com 	| password | 2    | James      | Duncombe  | true       | 1        |
      | admin@gpathome.com  | password | 4    | James      | Duncombe  | false      | 0        |
      | gp@gpathome.com     | password | 3    | Justine    | Setchell  | false      | 0        |
    Given there are the following medical profiles:
      | user_id | medical_history                                   | family_history      | regular_medication | allergies  | smoking | alcohol_units | exercise |
      | 1       | This is a really long and fun medical history...  | Lots of fun history | Paracetamol        | None       | Yes     | 10            | none     |
    And I am on the home page
    And I login as "admin@gpathome.com"
    Then I follow "Users"
    Then I follow "View" within "#user_1"

  Scenario: View user profile
    Then I should see "This is a really long and fun medical history..."
    Then I should see "Approve"
    And I click on "Approve"
    Then I should see "User approved"