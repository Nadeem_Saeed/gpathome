Feature: Signing in
  If I sign in
  As a user
  Then I should see different messages depending on my state

  Background:
    Given there are the following plans:
      | name    | description             | price  |
      | Single  | Single plan for people  | 300.00 |
    Given there are the following credit types:
      | name                |
      | Online Consultation |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 3                 |
      | 1       | 2         | 1                 |
    Given there are the following groups:
      | plan_id |
      | 1       |


  Scenario: Unapproved account
    Given there are the following users:
      | email               | password | unapproved | first_name | last_name | group_id | role |
      | user@gpathome.com   | password | true       | James      | Duncombe  | 1        | 2    |
    And I am on the home page
    When I follow "Login"
    And I fill in "Email" with "user@gpathome.com"
    And I fill in "Password" with "password"
    And I press "Login"
    Then I should see "Your account has not been approved by a GP yet. You will receive an email when it is ready."

  Scenario: Disabled account
    Given there are the following users:
      | email               | password | first_name | last_name | group_id | role | active |
      | user@gpathome.com   | password | James      | Duncombe  | 1        | 2    | false  |
    And I am on the home page
    When I follow "Login"
    And I fill in "Email" with "user@gpathome.com"
    And I fill in "Password" with "password"
    And I press "Login"
    Then I should see "Your account has been disabled. Please contact us at hello@gpathome.com"
