def path_to(page_name)
  case page_name
  
  when /home/
    root_path
  
  # Add more page name => path mappings here
  
  when /dashboard/
    dashboard_path
  
  when /consultations/
    consultations_path

  when /medical_profile/
    edit_medical_profile_path
    
  when /profile/
    edit_profile_path
    
  when /feedback/
    admin_feedbacks_path
  
  when /account/
    user_account_path
    
  when /credits/
    user_credits_path

  when /admin user profile/
    admin_user_path

  
  
  else
    raise "Can't find mapping from \"#{page_name}\" to a path."
  end
end