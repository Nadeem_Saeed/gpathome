# user records
Given /^there are the following (.*):$/ do |x, table|

  case x

  when 'users'

    table.hashes.each do |attributes|
      unapproved = attributes.delete("unapproved") == "true" # removes the unconfirmed key

      # create the new user using the attributes we are passing through
      user = User.new(attributes)

      user.plan   = attributes.fetch('plan') { 1 }
      user.active = attributes.fetch('active') { true }
      user.accepted_terms = attributes.fetch('accepted_terms') { true }

      unless unapproved
        user.approved = true
      end

      if [1,5,6].include? user.role
        if user.group_id
          group = Group.find(attributes['group_id'])
          user.invited_by = User.find(group.admin_id)
        else
          user.invited_by = User.first
        end
      end

      # build the user profile
      user.build_profile(
        :first_name         => attributes[:first_name],
        :last_name          => attributes[:last_name],
        :phone_1            => '01372 459858',
        :date_of_birth      => Time.now,
        :property_number    => '4B',
        :street_address     => 'Oakdene Close',
        :postal_code        => 'KT23 4PT'
      )

      # save the user model which in turn saves all the other attributes declared above
      user.save!

    end

  when 'groups'

    table.hashes.each do |attributes|
      @group = FactoryGirl.create(:group, attributes)
    end

  when 'plans'

    table.hashes.each do |attributes|
      @plan = FactoryGirl.create(:plan, attributes)
    end

  when 'plan credits'

    table.hashes.each do |attributes|
      @plan_credit = FactoryGirl.create(:plan_credit, attributes)
    end

  when 'credit types'

    table.hashes.each do |attributes|
      @credit = FactoryGirl.create(:credit, attributes)
    end

  when 'medical profiles'

    table.hashes.each do |attributes|
      @user = User.find_by_id(attributes[:user_id])
      @user.medical_profile.update_attributes(attributes)
    end

  when 'consultations'

    table.hashes.each do |attributes|
      @consultation = FactoryGirl.create(:consultation, attributes)
    end

  # messages
  when 'messages'

    table.hashes.each do |attributes|
      @message = FactoryGirl.create(:message, attributes)
    end

  when 'feedbacks'

    table.hashes.each do |attributes|
      @feedback = FactoryGirl.create(:feedback, attributes)
    end

  end

end

# testing for a message inside consultation
# TODO: Tidy this up as it's reference to @consultation is VERY ambiguous
And /^"([^\"]*)" has sent a message:$/ do |email, table|
  table.hashes.each do |attributes|
    @user = User.find_by_email(email)
    @consultation.messages.create!(attributes.merge!(:user => @user, :attachments => '' ))
  end
end
