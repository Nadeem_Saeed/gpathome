Then /^show me the last user record$/ do
  require 'ap'
  ap User.last
end

Then /^show me each group$/ do
  require 'ap'
  ap Group.all
end

# used to show all records of x from y where y.id = z
Then /^show me all (.*) of (.*) (\d+)$/ do |children_model, parent_model, record_id|
  require 'ap'
  ap parent_model.classify.constantize.find(record_id).send(children_model)
end