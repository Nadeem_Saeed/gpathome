
# Steps for Recurly

And /^I close the recurly account for "(.*)"$/ do |account|
  user = User.find_by_email(account)
  account = Recurly::Account.find(user.id)
  account.destroy
end