# login steps for users
Given /^I login as "(.*)"$/ do |email|
  steps %{And I follow "Login"
    When I fill in "Email" with "#{email}" within ".new_user"
    And I fill in "Password" with "password" within ".new_user"
    And I press "Login"
  }
end

Given /^I have accepted the medical terms$/ do
  steps %{When I follow "Profiles"
    When I follow "Medical Profile"
    And I check the "The information provided is correct to the best of my knowledge" checkbox
    And I press "Update Medical Profile"
    And I am on the home page
  }
end

# role checking
Then /^"(.*)" role should be account holder$/ do |email|
  current_user = User.find_by_email(email)
  current_user.role.should eq 2
end
