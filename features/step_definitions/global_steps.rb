# Global steps used throughout the app

Given /^I am on the (.+) page$/ do |page_name|
  visit path_to(page_name)
end

When /^I follow "([^\"]*)"$/ do |link|
  first(:link, link).click
end

Then /^I follow "([^\"]*)" within "([^\"]*)"$/ do |link, selector|
  within selector do
    first(:link, link).click
  end
end

When /^I click on "([^\"]*)"$/ do |link|
  first(:link, link).click
end

When /^I follow sign up for the "([^\"]*)" plan$/ do |plan|
  case plan
    when 'individual'
      find('a[href$="plan=1"]').click
    when 'couple'
      find('a[href$="plan=2"]').click
    when 'family'
      find('a[href$="plan=3"]').click
  end
end

# fill in
When /^(?:|I )fill in "([^\"]*)" with "([^\"]*)"$/ do |field, value|
  fill_in(field, with: value)
end

When /^(?:|I )fill in "([^\"]*)" with "([^\"]*)" within "([^\"]*)"$/ do |field, value, selector|
  within(selector) do
    #fill_in(field, with: value)
    first(:field, field).set value
  end
end

When /^I press "([^\"]*)"$/ do |button|
  first(:button, button).click
end

# Should see x...

Then /^(?:|I )should see "([^\"]*)"$/ do |text|
  if page.respond_to? :should
    page.should have_content(text)
  else
    assert page.has_content?(text)
  end
end

Then /^I should not see "([^\"]*)"$/ do |message|
  if message.empty?
    assert true
  else
    if page.respond_to? :should
      page.should have_no_content(message)
    else
      assert page.has_no_content?(message)
    end
  end
end

# Doc string type strings
Then /^I should see:$/ do |string|
  if page.respond_to? :should
      page.should have_content(string)
    else
      assert page.has_content?(string)
    end
end

Then /^I should see "([^\"]*)" within field "([^\"]*)"$/ do |value,field|
  find_field(field).value.should eq value
end

# Same as above but specifically in an element on the page
# see: http://stackoverflow.com/questions/5425723/then-i-should-see-capybara-step-is-being-too-specific-about-scope
Then /^I should see "([^\"]*)" within "([^\"]*)"$/ do |text, selector|
  page.should have_selector(selector, :text => text)
end

# show me the page
Then /^show me the page$/ do
  save_and_open_page
end

# dropdown selections
# SEE: http://pullmonkey.com/2011/03/03/capybara-does-not-trigger-js-change-event-with-celertiy-driver/
When /^(?:|I )select "([^\"]*)" from "([^\"]*)"$/ do |value, field|
  select(value, :from => field)
  # this is what changed .. 1) need to find the ID of the field that was passed in and then trigger a change event
  id = "#" + find_field(field)[:id]
  page.execute_script("$('#{id}').trigger('change');")
end

# check for class
Then /^there should be the class "([^\"]*)"$/ do |selector|
  page.find selector
end

# js wait for page to load
# wait for JS
When /^I wait (\d+) seconds?$/ do |seconds|
  sleep seconds.to_i
end

Then /^I should see the following content:$/ do |table|
  table.hashes.each do |attributes|
    if page.respond_to? :should
      page.should have_content(attributes[:content])
    else
      assert page.has_content?(attributes[:content])
    end
  end
end

Given /^I am not authenticated$/ do
  visit('/logout') # ensure that at least
end

When /^I check the "([^\"]*)" checkbox$/ do |arg|
  check(arg)
end

Given /^I choose "([^\"]*)"$/ do |option|
  choose(option)
end

And /^I fill in "([^\"]*)" within "([^\"]*)" with "([^\"]*)"$/ do |field, selector, text|
  within selector do
    find(field).set(text)
  end
end

And /^I select "([^\"]*)" from "([^\"]*)" within "([^\"]*)"$/ do |text, field, selector|
  # using jQuery here to do the selection on Mexico
  page.execute_script("$('#{ selector } select').find('option[value=\"MX\"]').attr('selected', true);")
end
