# Dealing with all image attachments

# attach file
Then /^I attach an image file$/ do
  filepath = "#{Rails.root}/features/upload_files/test.jpg"
  script = "document.getElementById('asset_attachment').style.opacity = 1;"
  page.driver.browser.execute_script(script)
  attach_file 'asset_attachment', filepath
end

# check file is attached
Then /^there should be an image attached$/ do
  Message.last.assets.count.should eq 1
end
