Feature: Seperation of users using groups
  I should be seperated by groups on the system
  As a user

  Background:
    Given there are the following plans:
      | name    | description             | price  | max_users |
      | Single  | Single plan for people  | 300.00 | 1         |
      | Couple  | Couple plan for people  | 450.00 | 2         |
      | Family  | Family plan for people  | 650.00 | 5         |
    Given there are the following credit types:
      | name                |
      | Online Consultation |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 3                 |
      | 2       | 1         | 6                 |
      | 3       | 1         | 8                 |
    Given there are the following groups:
      | admin_id | plan_id |
      | 1        | 2       |
      | 3        | 3       |
      | 5        | 1       |
      | 6        | 2       |
      | 7        | 0       |
    Given there are the following users:
      | email                      | password | role | first_name | last_name | group_id | plan |
      | user1_group1@gpathome.com  | password | 2    | Jill       | Jolly     | 1        | 2    |
      | user2_group1@gpathome.com  | password | 1    | Bob        | Jolly     | 1        | 2    |
      | user1_group2@gpathome.com  | password | 2    | Rebecca    | Cummins   | 2        | 3    |
      | user2_group2@gpathome.com  | password | 1    | John       | Cummins   | 2        | 3    |
      | user3_group3@gpathome.com  | password | 2    | James      | Duncombe  | 3        | 1    |
      | user1_group4@gpathome.com  | password | 2    | James      | Duncombe  | 4        | 2    |
      | gp@gpathome.com            | password | 3    | Justine    | Setchell  | 5        | 0    |
    Given there are the following consultations:
      | user_id | subject              |
      | 1       | Feet hurt            |
      | 2       | I've got a cold      |
      | 1       | Another consultation |
    Given there are the following messages:
      | consultation_id | user_id | message                                                   |
      | 1               | 1       | My feet really hurt, can you help?                        |
      | 2               | 2       | Hi, I've had a cold for months. What can you do about it? |
      | 3               | 1       | Another consultation message                              |
    Given a clear email queue
    And I am on the home page

  Scenario Outline: Logging in and viewing users within my group if I have access
    Given I login as "<user>"
    And I follow "Account"
    Then I should see "<should_see>"
    Then I should not see "<should_not_see>"

  Examples: User accounts trying to access the system
    | user                      | plan_should_be | should_see      | should_not_see  |
    | user1_group1@gpathome.com | Couple         | Account Users   |                 |
    | user1_group2@gpathome.com | Family         | Account Users   |                 |
    | user3_group3@gpathome.com | Individual     |                 | Account Users   |
    | user2_group2@gpathome.com | Family         |                 | Account Users   |

  @slow @javascript
  Scenario Outline: Add a new user account
    Given I login as "<user>"
    And I follow "Account"
    And I follow "Account Users"
    Then I should see "and can add a further <remaining_members> users to"
    Then I follow "Add a new user"
    # Fill out the core details
    Then I fill in "First name" with "Lucy" within "#new_user"
    Then I fill in "Last name" with "Mindy" within "#new_user"
    Then I fill in "Email" with "lucy@gpathome.com" within "#new_user"
    Then I fill in "Phone" with "01372 459858" within "#new_user"
    # Users date of birth
    And I select "16" from "user_profile_attributes_date_of_birth_3i"
    And I select "August" from "user_profile_attributes_date_of_birth_2i"
    And I select "1988" from "user_profile_attributes_date_of_birth_1i"
    # Send the invite
    Then I press "Send Invite"
    Then I should see "Thank you, once a GP approves this member they will receive an email with login details"
    And I follow "Logout"
    And "lucy@gpathome.com" opens the email with subject: "Invitation to GP at Home"
    Then I should see "Someone has invited you to GP at Home, you can accept the invite through the link below" in the email body
    And I click the first link in the email
    And I fill in "Password" with "12345678" within ".edit_user"
    And I fill in "Password confirmation" with "12345678" within ".edit_user"
    And I press "Set my password"
    Then I should see "Your password has now been set. Your account will now be checked by a GP"
    And "admin@gpathome.com" opens the email with subject: "GP at Home - User Awaiting Approval"
    Then they should see "New User" in the email body
    Then they should see "Someone has just signed up for GP at Home" in the email body
    Then I login as "gp@gpathome.com"
    And I should see "Awaiting Approval"
    Then I follow "Lucy Mindy"
    Then I click on "Approve"
    Then "lucy@gpathome.com" should receive an email with subject "GP at Home account has been approved"
    And "lucy@gpathome.com" opens the email with subject: "GP at Home account has been approved"
    Then they should see "Your account has been approved. Welcome to GP at Home!" in the email body

  Examples: User account combinations
    Are people allowed to add new users to the account or not?
    Depends on number of allowed users per plan (:max_users)
    | user                      | plan_should_be | can_add_members | remaining_members |
    | user1_group2@gpathome.com | Family         | true            | 3                 |
    | user1_group4@gpathome.com | Couple         | true            | 1                 |

  @javascript @slow
  Scenario: Add a dependent account
    Given I login as "user1_group2@gpathome.com"
    And I follow "Account"
    And I follow "Account Users"
    And I follow "Add a new user"
    # Core user details
    And I choose "Dependent"
    And  I fill in "First name" with "Lucy"
    And I fill in "Last name" with "Mindy"
    Then I fill in "Phone" with "01372 459858"
    # Users date of birth
    And I select "16" from "user_profile_attributes_date_of_birth_3i"
    And I select "August" from "user_profile_attributes_date_of_birth_2i"
    And I select "1988" from "user_profile_attributes_date_of_birth_1i"
    # Users address
    And I check the "user_has_same_address" checkbox
    And I press "Add Dependent"
    Then I should see "New dependent user created"
