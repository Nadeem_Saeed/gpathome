Feature: Add payment to user account
  As an admin
  I need to be able to add payments to a user account

  Background:
    Given there are the following plans:
      | name       | description                  | price  | max_users |
      | Individual | Single plan for people       | 300.00 | 1         |
    Given there are the following credit types:
      | name                 | description                  |
      | Online Consultation  | Online consultations for you |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 3                 |
    Given there are the following groups:
      | admin_id |
      | 1        |
      | 2        |
    Given there are the following users:
      | email               | password | role | first_name | last_name |
      | admin@gpathome.com  | password | 4    | James      | Duncombe  |
      | user@gpathome.com   | password | 2    | James      | Duncombe  |
    And I am on the home page
    And I login as "admin@gpathome.com"
    Then I follow "Users"
    Then I follow "Payments"

  @javascript
  Scenario: Add a new account
    And I fill in "Amount" with "20"
    And I select "6 months" from "Payment term"
    And I press "Add payment"
    Then I should see "Added payment to account"
    And I should see "20.0"
    And I should see "6 months"
    And I follow "Logout"

  Scenario: Add a new account but enter the wrong amount
    And I fill in "Amount" with "this is invalid"
    And I press "Add payment"
    Then I should see "Sorry, there was a problem adding payment (did you enter a decimal number in the amount box?)"
    And I follow "Logout"

  Scenario: Delete a payment
    And I fill in "Amount" with "30"
    And I press "Add payment"
    And I follow "Delete"
    Then I should see "Payment deleted"
    Then I should not see "30.0"

