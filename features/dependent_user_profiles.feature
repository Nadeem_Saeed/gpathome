Feature: View / edit dependent user profiles
  As an authorised user
  I should be able to view / edit a dependent users profile (both medical and main profile) 

  Background:
    Given there are the following plans:
      | name    | description             | price  | max_users |
      | Single  | Single plan for people  | 300.00 | 1         |
    Given there are the following credit types:
      | name                |
      | Online Consultation |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 3                 |
    Given there are the following groups:
      | admin_id | plan_id |
      | 1        | 2       |
      | 3        | 3       |
      | 5        | 1       |
      | 6        | 2       |
      | 7        | 0       |
    Given there are the following users:
      | email                      | password | role | first_name | last_name | group_id |
      | jill@gpathome.com          | password | 2    | Jill       | Jolly     | 1        | 
      | bob@gpathome.com           | password | 1    | Bob        | Jolly     | 1        |
      |                            |          | 6    | Child      | Name      | 1        |
      | gp@gpathome.com            | password | 3    | Justine    | Setchell  | 5        |
    Given I am on the home page

  @javascript @slow
  Scenario: Check childs profile
    Given I login as "jill@gpathome.com"
    And I follow "Profiles"
    Then I should see "Select profile"
    Then I select "Child Name" from "user_profile"
    Then I should see "Child" within field "First name"