Feature: Adding a message to a consultation
  In order to send a message to the GPs
  As a user
  I need to be able to send them a new message
  I also need to be able to attach an image to a message

  Background:
    Given there are the following plans:
      | name    | description             | price  |
      | Single  | Single plan for people  | 300.00 |
    Given there are the following credit types:
      | name                |
      | Online Consultation |
      | Home Visit          |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 3                 |
      | 1       | 2         | 1                 |
    Given there are the following groups:
      | admin_id | plan_id |
      | 1        | 1       |
      | 2        | 1       |
      | 3        | 0       |
    Given there are the following users:
      | id | email              | password | unconfirmed | first_name | last_name | group_id | role |
      | 1  | user@gpathome.com  | password | false       | James      | Duncombe  | 1        | 2    |
      | 2  | user2@gpathome.com | password | true        | Bob        | Jib       | 2        | 2    |
      | 3  | admin@gpathome.com | password | false       | Justine    | Setchell  | 3        | 3    |
    Given a clear email queue
    And I am on the home page
    Given I login as "user@gpathome.com"
    Given there are the following consultations:
      | subject   | assigned_to_id |
      | Feet Hurt | 3              |
    And "user@gpathome.com" has sent a message:
      | message                |
      | My feet hurt like mad! |
    And I am on the dashboard page
    When I follow "View / Reply"

  Scenario: Adding a new message
    And I fill in "Message" with "This is a follow up message to a GP"
    And I press "Send"
    Then I should see "Message has been sent."
    And "admin@gpathome.com" opens the email with subject: "GP at Home - New Message"
    Then I should see "Someone has just replied to a consultation" in the email body

  Scenario: Adding a new message without a message field filled out
    And I press "Send"
    Then I should see "Message was not sent."

  @selenium @slow @attachment
  Scenario: Adding a new message with image attached
    And I fill in "Message" with "I've attached an image below of what I'm talking about."
    Then I attach an image file
    And I wait 2 seconds
    And I press "Send"
    Then I should see "Message has been sent."
    And there should be an image attached
