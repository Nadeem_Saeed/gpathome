Feature: Assign Consultations
  In order to manage consultations
  As an admin or gp
  I need to be able to assign a consultation to myself
  And I need to be able to un-assign a consultation back to the global pool
  
  Background:
    Given there are the following plans:
      | name    | description             | price  |
      | Single  | Single plan for people  | 300.00 |
    Given there are the following credit types:
      | name                |
      | Online Consultation |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 3                 |
    Given there are the following groups:
      | admin_id | plan_id |
      | 1        | 1       |
    Given there are the following users:
      | email				        | password | role | first_name | last_name | group_id |
      | user@gpathome.com 	| password | 2    | James      | Duncombe  | 1        |
      | user2@gpathome.com 	| password | 2    | Test       | Person    | 1        |
      | gp@gpathome.com     | password | 3    | Justine    | Setchell  | 1        |
    Given there are the following consultations:
      | user_id | subject         |
      | 1       | Feet hurt       |
      | 2       | I've got a cold |
    Given there are the following messages:
      | consultation_id | user_id | message                            |
      | 1               | 1       | My feet really hurt, can you help? |
      | 2               | 2       | My feet really hurt, can you help? |
    And I am on the home page
    And I login as "gp@gpathome.com"
    
  Scenario: Assigning a consultation to myself
    When I follow "Consultations"
    And I follow "Assign" within "#consultation_1"
    Then I should see "Assigned to you!"
    And I should see "you" within "td.updated + td"
    
  Scenario: Un-assigning consultation
    When I follow "Consultations"
    And I follow "Assign" within "#consultation_1"
    Then I should see "Assigned to you!"
    Then I should see "Un-assign"
    When I follow "Un-assign" within "#consultation_1"
    Then I should see "Consultation un-assigned!"
    And I should see "" within "td.updated + td"