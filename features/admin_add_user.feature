Feature: Add GP account
  As an admin
  I need to be able to add a new GP

  Background:
    Given there are the following plans:
      | name       | description   | price  |
      | Individual | Description   | 300.00 |
    Given there are the following groups:
      | admin_id | plan_id |
      | 1        | 0       |
    Given there are the following users:
      | email               | password | role | first_name | last_name |
      | admin@gpathome.com  | password | 4    | James      | Duncombe  |
    Given there are the following credit types:
      | name                 | description                  |
      | Online Consultation  | Online consultations for you |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 3                 |
    And I am on the home page
    And I login as "admin@gpathome.com"
    Then I follow "Users"
    Then I follow "New User"

  @javascript
  Scenario: Add a new GP account
    And I fill in "Email" with "gp2@gpathome.com"
    And I select "Gp" from "Role"
    And I fill in "First name" with "James"
    And I fill in "Last name" with "Duncombe"

    # DOB
    And I select "16" from "user_profile_attributes_date_of_birth_3i"
    And I select "August" from "user_profile_attributes_date_of_birth_2i"
    And I select "1986" from "user_profile_attributes_date_of_birth_1i"

    And I select "Female" from "Gender"

    And I fill in "Phone number" with "01372 459858"

    # Address
    And I fill in "Address 1" with "line 1"
    And I fill in "Address 2" with "line 2"
    And I fill in "City" with "London"
    And I fill in "Post code" with "XXXX XXX"

    And I press "Create User"

    Then I should see "User was successfully created"
    And I follow "Logout"
    And "gp2@gpathome.com" opens the email with subject: "Reset password instructions"
    And I click the first link in the email
    And I fill in "New password" with "12345678"
    And I fill in "Confirm new password" with "12345678"
    And I press "Change my password"
    Then I should see "Your password was changed"
