Feature: Configuring user credit Plans
  As an admin
  I need to be able to configure the different user plans in my control panel
  
  Background:
    Given there are the following credit types:
      | name                | price |
      | Online Consultation | 100   |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 3                 |
      | 2       | 1         | 5                 |
      | 3       | 1         | 10                |
    Given there are the following plans:
      | name    | description             | price  | max_users | status |
      | Single  | Single plan for people  | 300.00 | 1         | 1      |
      | Couple  | Couples plan for people | 400.00 | 2         | 2      |
      | Family  | Plan for families       | 600.00 | 5         | 1      |
    Given there are the following users:
      | email				        | password | role  | first_name | last_name | group_id |
      | user@gpathome.com 	| password | 2     | James      | Duncombe  | 2        |
      | admin@gpathome.com  | password | 4     | James      | Duncombe  | 0        |
      | gp@gpathome.com     | password | 3     | Justine    | Setchell  | 0        |
    And I am on the home page
    
  Scenario: View the plans as a user
    When I follow "Plans"
    Then there should be the class ".couple.promoted"
    
  Scenario: Try to view the plans page as a regular GP
    When I follow "Login"
    When I fill in "Email" with "gp@gpathome.com"
    And I fill in "Password" with "password"
    And I press "Login"
    Then I should not see "Plans"
    
  Scenario: Try to view the plans page as an Administrator
    When I follow "Login"
    When I fill in "Email" with "admin@gpathome.com"
    And I fill in "Password" with "password"
    And I press "Login"
    Then I should see "Plans"
    When I follow "Plans"
    Then I should see "Name"
    Then I should see "Description"
    Then I should see "Price"
    Then I should see "Online Consultations"