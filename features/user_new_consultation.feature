Feature: New Consultation
  In order to send a message to the GPs
  As a user
  I need to be able to start a new consultation

  Background:
    Given there are the following plans:
      | name    | description             | price  |
      | Single  | Single plan for people  | 300.00 |
    Given there are the following credit types:
      | name                |
      | Online Consultation |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 3                 |
    Given there are the following groups:
      | admin_id | plan_id |
      | 2        | 0       |
      | 1        | 1       |
      | 3        | 1       |
    Given there are the following users:
      | email              | password | role | first_name | last_name | group_id |
      | user@gpathome.com  | password | 2    | James      | Duncombe  | 2        |
      | gp@gpathome.com    | password | 3    | Justine    | Setchell  | 1        |
      | user2@gpathome.com | password | 2    | James      | Duncombe  | 3        |
    Given a clear email queue
    And I am on the home page
    Given I login as "user@gpathome.com"
    Given I have accepted the medical terms
    When I follow "New Consultation"

  @selenium @attachment
  Scenario: Creating a new consultation with image attached
    When I fill in "Subject" with "This is a subject"
    And I fill in "Message" with "I've attached an image below of what I'm talking about."
    Then I attach an image file
    And I wait 2 seconds
    And I press "Send"
    Then I should see "Consultation Created."
    And there should be an image attached

  Scenario: Creating a new consultation
    And I fill in "Subject" with "Subject of consultation"
    And I fill in "Message" with "This is a message"
    And I press "Send"
    Then I should see "Consultation Created."
    Then "admin@gpathome.com" should receive no email with subject: "GP at Home - New Message"
    And "admin@gpathome.com" opens the email with subject: "GP at Home - New Consultation"
    Then I should see "Someone has just added a new consultation" in the email body
