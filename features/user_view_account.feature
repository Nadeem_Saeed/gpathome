Feature: View account
  In order to view my account
  As a user
  I need to be able to view the different sections of the Accounts page

  Background:
    Given there are the following plans:
      | name    | description             | price  |
      | Single  | Single plan for people  | 300.00 |
    Given there are the following credit types:
      | name                |
      | Online Consultation |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 5                 |
    Given there are the following groups:
      | admin_id | plan_id | group_type |
      | 1        | 1       | user       |
      | 2        | 0       | gp         |
    Given there are the following users:
      | email				        | password | role | first_name | last_name | group_id |
      | user@gpathome.com 	| password | 2    | James      | Duncombe  | 1        |
      | gp@gpathome.com     | password | 3    | Justine    | Setchell  | 0        |
    Given there are the following consultations:
      | user_id | subject   |
      | 1       | Feet hurt |
    Given there are the following messages:
      | consultation_id | user_id | message                            |
      | 1               | 1       | My feet really hurt, can you help? |
    And I am on the home page
    And I follow "Login"
    And I fill in "Email" with "user@gpathome.com"
    And I fill in "Password" with "password"
    And I press "Login"
    When I follow "Account"
    And I am on the account page
    
    Scenario: View current subscription plan
      Then I should see "You are currently on the "
      And I should see "Upgrade Plan"