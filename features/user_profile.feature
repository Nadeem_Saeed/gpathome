Feature: User profile
  In order to edit my profile
  As a user
  I need to be able to view and edit my profile

  Background:
    Given there are the following plans:
      | name    | description             | price  |
      | Single  | Single plan for people  | 300.00 |
    Given there are the following credit types:
      | name                |
      | Online Consultation |
      | Home Visit          |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 3                 |
      | 1       | 2         | 1                 |
    Given there are the following groups:
      | admin_id | plan_id |
      | 1        | 1       |
    Given there are the following users:
      | email				        | password | role | first_name | last_name | group_id |
      | user@gpathome.com 	| password | 2    | James      | Duncombe  | 1        |
      | member@gpathome.com | password | 1    | James      | Duncombe  | 1        |
      | dep@gpathome.com    | password | 6    | James      | Duncombe  | 1        |
      | gp@gpathome.com     | password | 3    | Justine    | Setchell  | 0        |
    And I am on the home page

  @slow @javascript
  Scenario Outline: Edit profile
    And I login as "user@gpathome.com"
    When I follow "Profile"
    When I fill in "First name" with "<first_name>"
    And I fill in "Last name" with "<last_name>"
    And I fill in "Phone" with "<phone>"
    And I fill in "Address 1" with "<address_1>"
    And I fill in "Postcode" with "<post_code>"
    And I select "<day>" from "user_profile_attributes_date_of_birth_3i"
    And I select "<month>" from "user_profile_attributes_date_of_birth_2i"
    And I select "<year>" from "user_profile_attributes_date_of_birth_1i"
    And I select "<gender>" from "Gender"
    And I press "Update"
    Then I should see "<should_see>"

  Examples:
    | first_name | last_name | phone        | sec_phone | address_1        | post_code | day | month  | year | gender | should_see             |
    | James      | Duncombe  | 07794 187581 |           | 4B Oakdene Close | kt23 4pt  | 16  | August | 1986 | Male   | Profile updated.       |
    | Lady       |           | 01372223333  |           | Another address  |           | 26  | May    | 1974 | Female | Profile not updated.   |

  @slow @javascript
  Scenario: Edit medical as a regular user
    Given I login as "member@gpathome.com"
    When I follow "Profiles"
    When I follow "Profile"
    Then I should not see "Select profile"

  @slow @javascript
  Scenario: Edit medical profile for dependent
    Given I login as "user@gpathome.com"
    When I follow "Profiles"
    When I follow "Profile"
    Then I should see "Select profile"
