Feature: Change user password
  In order to change my password
  As a user
  I need to be able to navigate to a screen where I can change my password

  Background:
    Given there are the following plans:
      | name    | description             | price  |
      | Single  | Single plan for people  | 300.00 |
    Given there are the following credit types:
      | name                | price |
      | Online Consultation | 100   |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 3                 |
    Given there are the following groups:
      | admin_id | plan_id |
      | 1        | 1       |
  	Given there are the following users:
      | email				        | password | role | first_name | last_name | group_id |
      | user@gpathome.com 	| password | 2    | James      | Duncombe  | 1        |
      | gp@gpathome.com     | password | 3    | Justine    | Setchell  | 0        |
    And I am on the home page
    And I follow "Login"
    And I fill in "Email" with "user@gpathome.com"
    And I fill in "Password" with "password"
    And I press "Login"
    When I follow "Account"
    And I am on the account page
    When I follow "Change Password"
    
    Scenario: Change password
      When I fill in "Current password" with "password"
      And I fill in "New password" with "password2"
      And I fill in "Confirm password" with "password2"
      And I press "Change Password"
      Then I should see "Password changed."
      
    Scenario: Change password with wrong password
      When I fill in "Current password" with "password234"
      And I fill in "New password" with "password2"
      And I fill in "Confirm password" with "password2"
      And I press "Change Password"
      Then I should see "Password not changed."
      And I should see "Current password is invalid"
      