Feature: Edit user profile
  As an admin
  I need to be able to update a users profile / medical profile if needed
  
  Background:
    Given there are the following plans:
      | name    | description             | price  | max_users |
      | Single  | Single plan for people  | 300.00 | 1         |
    Given there are the following credit types:
      | name                |
      | Online Consultation |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 3                 |
    Given there are the following groups:
      | admin_id | plan_id |
      | 1        | 2       |
      | 3        | 3       |
      | 5        | 1       |
      | 6        | 2       |
      | 7        | 0       |
    Given there are the following groups:
      | admin_id | plan_id |
      | 1        | 1       |
    Given there are the following users:
      | email               | password | role | first_name | last_name | group_id |
      | user@gpathome.com   | password | 2    | James      | Duncombe  | 1        |
      | admin@gpathome.com  | password | 4    | James      | Duncombe  | 0        |
      | gp@gpathome.com     | password | 3    | Justine    | Setchell  | 0        |
    Given there are the following medical profiles:
      | user_id | medical_history                                   | family_history      | regular_medication | allergies  | smoking | alcohol_units | exercise |
      | 1       | This is a really long and fun medical history...  | Lots of fun history | Paracetamol        | None       | Yes     | 10            | none     |
    And I am on the home page
    And I login as "admin@gpathome.com"
    Then I follow "Users"
    Then I follow "Edit" within "#user_1"

  @slow @javascript
  Scenario: Edit user profile
    And I select "16" from "user_profile_attributes_date_of_birth_3i"
    And I select "August" from "user_profile_attributes_date_of_birth_2i"
    And I select "1986" from "user_profile_attributes_date_of_birth_1i"
    And I fill in "Phone number" with "01372 459858"
    And I fill in "Address 1" with "4B Oakdene Close"
    And I fill in "Address 2" with "Great Bookham"
    And I fill in "City" with "Leatherhead"
    And I fill in "County" with "Surrey"
    And I fill in "Post code" with "KT23 4PT"
    And I fill in "Medical history" with "Long history..."
    And I fill in "Family history" with "Long history..."
    And I fill in "Regular medication" with "Long history..."
    And I fill in "Allergies" with "Many..."
    And I select "Yes" from "Smoking"
    And I press "Update User"
    Then I should see "User was successfully updated"