Feature: Invite 92 Premier user
  As an admin
  I need to be able to invite a user

  Background:
    Given there are the following plans:
      | name       | description                  | price  | max_users |
      | Individual | Single plan for people       | 300.00 | 1         |
      | Couple     | Couple plan for people       | 450.00 | 2         |
      | Family     | Family plan for people       | 650.00 | 5         |
    Given there are the following credit types:
      | name                 | description                  |
      | Online Consultation  | Online consultations for you |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 3                 |
      | 2       | 1         | 6                 |
      | 3       | 1         | 999               |
    Given there are the following groups:
      | admin_id | plan_id |
      | 1        | 0       |
    Given there are the following users:
      | email               | password | role | first_name | last_name |
      | admin@gpathome.com  | password | 4    | James      | Duncombe  |
    Given a clear email queue
    And I am on the home page
    And I login as "admin@gpathome.com"
    Then I follow "Users"
    Then I follow "Invite User"

  @slow @javascript
  Scenario: Add a new account
    And I fill in "Email" with "account_member@gpathome.com"
    And I fill in "First name" with "Account"
    And I fill in "Last name" with "Member"
    And I select "Individual" from "Plan"
    And I press "Invite User"
    Then I should see "User was invited"
    And I follow "Logout"

    # user gets the email invite
    And "account_member@gpathome.com" opens the email with subject: "Invitation to GP at Home"
    Then I should see "Someone has invited you to GP at Home, you can accept the invite through the link below" in the email body
    And I click the first link in the email
    And I fill in "Password" with "12345678" within ".edit_user"
    And I fill in "Password confirmation" with "12345678" within ".edit_user"
    And I press "Set my password"
    Then I should see "Please click here to confirm you have read and updated your medical profile"
    Then I should receive no emails with subject "GP at Home account has been approved"

    # The below should go into a new feature
    Then I follow "Account"
    Then I should not see "Billing Details"
    Then I should not see "Account Users"
