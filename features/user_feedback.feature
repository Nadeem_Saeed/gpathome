Feature: User feedback
  As a user
  I should be able to give feedback if I have a problem or other feedback

  Background:
    Given there are the following plans:
      | name    | description             | price  |
      | Single  | Single plan for people  | 300.00 |
    Given there are the following credit types:
      | name                | price |
      | Online Consultation | 100   |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 3                 |
    Given there are the following groups:
      | admin_id | plan_id |
      | 1        | 1       |
    Given there are the following users:
      | email				        | password | role | first_name | last_name | group_id |
      | user@gpathome.com 	| password | 2    | Patient    | Name      | 1        |
      | gp@gpathome.com 	  | password | 3    | Justine    | Setchell  | 0        |
      | admin@gpathome.com  | password | 4    | James      | Duncombe  | 0        |
    And I am on the home page
    And I follow "Login"

  @javascript @slow
  Scenario: Giving feedback
  	When I fill in "Email" with "user@gpathome.com"
    And I fill in "Password" with "password"
    And I press "Login"
  	And I am on the dashboard page
    And I click on "Site Feedback"
    And I wait 1 second
    Then I should see "Please write your feedback of the site below"
    When I fill in "feedback" with "Great site, I've got a problem with the Login though, can you help?"
    And I press "Send feedback"
    And I wait 2 seconds
    Then I should see "Thank you, we will get back to you soon!"
  
  @javascript @slow
  Scenario: View user feedback
    Given there are the following feedbacks:
      | feedback                              | user_id | url              | email          |
      | There's a problem on the system       | 1       | /path/to/problem |                |
      | Found an error                        | 3       | /another/path    |                |
      | Can't click this button               |         | /another/path2   | jimbo@blah.com |
      | Another error, you can't see me!      |         | /path            |                |
    When I fill in "Email" with "admin@gpathome.com"
    And I fill in "Password" with "password"
    And I press "Login"
    And I click on "Feedback"
    And I am on the feedback page
    Then I should see "There's a problem on the system"