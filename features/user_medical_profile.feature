Feature: User medical profile
  In order to edit my medical profile
  As a user
  I want to view and edit my medical profile

  Background:
    Given there are the following plans:
      | name    | description             | price  |
      | Single  | Single plan for people  | 300.00 |
    Given there are the following credit types:
      | name                |
      | Online Consultation |
      | Home Visit          |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 3                 |
      | 1       | 2         | 1                 |
    Given there are the following groups:
      | admin_id | plan_id |
      | 1        | 1       |
    Given there are the following users:
      | email               | password | role | first_name | last_name | group_id |
      | user@gpathome.com   | password | 2    | James      | Duncombe  | 1        |
      | member@gpathome.com | password | 1    | James      | Duncombe  | 1        |
      | dep@gpathome.com    | password | 6    | James      | Duncombe  | 1        |
      | gp@gpathome.com     | password | 3    | Justine    | Setchell  | 0        |
    And I am on the home page

  @slow @javascript
  Scenario Outline: Edit medical profile
    Given I login as "user@gpathome.com"
    When I follow "Profiles"
    When I follow "Medical Profile"
    When I fill in "Insurance company" with "<insurance_company>"
    And I fill in "Policy number" with "<policy_number>"
    And I select "<london_insurance>" from "London insurance?"
    And I fill in "Past medical history" with "<medical_history>"
    And I fill in "Family history" with "<family_history>"
    And I fill in "Drug history" with "<regular_medication>"
    And I fill in "Allergies" with "<allergies>"
    And I select "<smoking>" from "Do you smoke?"
    And I select "<alcohol_units>" from "Alcohol (units)"
    And I fill in "Exercise" with "<exercise>"
    And I select "<height>" from "Height (cm)"
    And I select "<weight>" from "Weight (kg)"
    And I check the "The information provided is correct to the best of my knowledge" checkbox
    And I press "Update"
    Then I should see "<should_see>"

  Examples:
    | insurance_company | policy_number | london_insurance | medical_history                                   | family_history      | regular_medication | allergies  | smoking | alcohol_units | exercise | height | weight | should_see                   |
    | Bupa              | 1234-23423dsd | Yes              | This is a really long and fun medical history...  | Lots of fun history | Paracetamol        | None       | Yes     | 10            | none     | 140    | 60     | Medical profile updated.     |
    | Another Med       | 1234_23423 | No              | This is a really long and fun medical history...  | Lots of fun history | Tramadol           | None       | No      | 20            | none     | 100    | 40     | Medical profile updated.     |

  @slow @javascript
  Scenario: Edit profile
    Given I login as "member@gpathome.com"
    When I follow "Profiles"
    When I follow "Medical Profile"
    Then I should not see "Select profile"

  @slow @javascript
  Scenario: Edit medical profile for dependent
    Given I login as "user@gpathome.com"
    When I follow "Profiles"
    When I follow "Medical Profile"
    Then I should see "Select profile"
