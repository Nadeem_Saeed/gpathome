Feature: Reply to Consultation
  In order to reply to consultations
  As an admin or gp
  I need to be able to reply to a consultation
  And I need to be automatically assigned to it on reply

  Background:
    Given there are the following plans:
      | name    | description             | price  |
      | Single  | Single plan for people  | 300.00 |
    Given there are the following credit types:
      | name                |
      | Online Consultation |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 3                 |
    Given there are the following groups:
      | admin_id | plan_id |
      | 1        | 1       |
    Given there are the following users:
      | email               | password | role | first_name | last_name |
      | user@gpathome.com   | password | 2    | James      | Duncombe  |
      | user2@gpathome.com  | password | 2    | Test       | Person    |
      | gp@gpathome.com     | password | 3    | Justine    | Setchell  |
    Given there are the following consultations:
      | user_id | subject         |
      | 1       | Feet hurt       |
      | 2       | I've got a cold |
    Given there are the following messages:
      | consultation_id | user_id | message                                                   |
      | 1               | 1       | My feet really hurt, can you help?                        |
      | 2               | 2       | Hi, I've had a cold for months. What can you do about it? |
    And I am on the home page
    And I login as "gp@gpathome.com"
    Given a clear email queue

  Scenario: Reply to Consultation
    When I follow "Reply"
    And I fill in "Message" with "Take some painkillers!"
    And I press "Send"
    Then I should see "Message has been sent."
    When I follow "Consultations"
    Then I should see "you" within "td.updated + td"
    And "user@gpathome.com" opens the email with subject: "New message waiting for you at GP at Home"
    Then I should see "Dr Setchell" in the email body
