Feature: Activate user
  As an admin
  I need to be able to activate a user

  Background:
    Given there are the following plans:
      | name    | description             | price  |
      | Single  | Single plan for people  | 300.00 |
    Given there are the following credit types:
      | name                |
      | Online Consultation |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 3                 |
    Given there are the following groups:
      | admin_id |
      | 1        |
    Given there are the following users:
      | email              | password | role | first_name | last_name | group_id |
      | user@gpathome.com  | password | 2    | James      | Duncombe  | 1        |
      | admin@gpathome.com | password | 4    | James      | Duncombe  | 0        |
    And I am on the home page
    And I login as "admin@gpathome.com"
    Then I follow "Users"

  Scenario: Activating and deactivating a user account
    And I follow "Deactivate" within "#user_1"
    Then I should see "User deactivated"

    And I should not see "user@gpathome.com"

    # This switches to the All Users view
    And I follow "All Users"

    Then I follow "Activate" within "#user_1"
    And I should see "User activated"
