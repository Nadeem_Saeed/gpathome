Feature: Dependent Consultations
  As an authorised user / account holder
  I should be able to create a consultation on behalf of a dependent user

  Background:
    Given there are the following plans:
      | name    | description             | price  | max_users |
      | Single  | Single plan for people  | 300.00 | 1         |
    Given there are the following credit types:
      | name                |
      | Online Consultation |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 3                 |
    Given there are the following groups:
      | admin_id | plan_id |
      | 1        | 2       |
      | 3        | 3       |
      | 5        | 1       |
      | 6        | 2       |
      | 7        | 0       |
    Given there are the following users:
      | email                      | password | role | first_name | last_name | group_id |
      | jill@gpathome.com          | password | 2    | Jill       | Jolly     | 1        |
      | bob@gpathome.com           | password | 1    | Bob        | Jolly     | 1        |
      |                            |          | 6    | Child      | Name      | 1        |
      | gp@gpathome.com            | password | 3    | Justine    | Setchell  | 5        |
    Given I am on the home page

  @slow @javascript
  Scenario: Create consultation on behalf of dependent user
    Given I login as "jill@gpathome.com"
    Given I have accepted the medical terms
    And I follow "New Consultation"
    Then I select "Child Name" from "consultation_user_id"
    And I fill in "Subject" with "My head hurts"
    And I fill in "Message" with "I've given him some paracetamol but it just won't shift. What can I do doc?"
    And I press "Send"
    Then I should see "Consultation Created"
    Then I follow "Consultations"
    Then I should see "Child Name"

  Scenario Outline: Check that another user can't view the dependents consultations
    Given there are the following consultations:
      | user_id | subject              |
      | 1       | Feet hurt            |
      | 2       | I've got a cold      |
      | 3       | Another consultation |
    Given there are the following messages:
      | consultation_id | user_id | message                                                   |
      | 1               | 1       | My feet really hurt, can you help?                        |
      | 2               | 2       | Hi, I've had a cold for months. What can you do about it? |
      | 3               | 3       | Another consultation message                              |
    Given I login as "<user>"
    Then I should see "<should_see>"
    And I should not see "<should_not_see>"

  Examples: User accounts trying to view consultations
    | user              | should_see           | should_not_see       |
    | jill@gpathome.com | Feet hurt            | I've got a cold      |
    | jill@gpathome.com | Another consultation | I've got a cold      |
    | bob@gpathome.com  | I've got a cold      | Another consultation |
