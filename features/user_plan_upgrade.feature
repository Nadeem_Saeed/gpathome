Feature: User plan upgrade
  As a user
  I need to be able to upgrade a plan

  Background:
    Given there are the following plans:
      | name       | description            | price | plan_code | max_users |
      | Individual | Single plan for people | 24.99 | ind001    | 1         |
      | Couple     | Couple plan for people | 34.99 | cpl001    | 2         |
      | Family     | Family plan for people | 44.99 | fam001    | 6         |
    Given there are the following credit types:
      | name                | price |
      | Online Consultation | 50    |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 5                 |
      | 2       | 1         | 6                 |
      | 3       | 1         | 8                 |
    Given there are the following groups:
      | admin_id | plan_id |
      | 2        | 1       |
      | 1        | 1       |
    Given there are the following users:
      | email               | password | role | first_name | last_name | group_id |
      | gp@gpathome.com     | password | 3    | Justine    | Setchell  | 2        |
      | user@gpathome.com   | password | 2    | James      | Duncombe  | 1        |
    Given there are the following consultations:
      | user_id | subject   |
      | 2       | Feet hurt |
    Given there are the following messages:
      | consultation_id | user_id | message                            |
      | 1               | 2       | My feet really hurt, can you help? |
    And I am on the home page
    And I login as "user@gpathome.com"
    When I follow "Account"

    @javascript
    Scenario: Upgrade plan
      And I wait 3 seconds
      Then I follow "Upgrade Plan"
      And I wait 3 seconds
      Then I follow "Upgrade"
      Then I wait 3 seconds
      And I press "Upgrade Plan"
      Then I should see "Plan upgraded to"
      Then I should see "Account Users"
