Feature: Credit a user
  As an admin
  I need to be able to credit a user
  
  Background:
    Given there are the following plans:
      | name    | description             | price  |
      | Single  | Single plan for people  | 300.00 |
    Given there are the following credit types:
      | name                |
      | Online Consultation |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 3                 |
    Given there are the following groups:
      | admin_id | plan_id |
      | 1        | 1       |
    Given there are the following users:
      | email               | password | role | first_name | last_name | group_id |
      | user@gpathome.com   | password | 2    | James      | Duncombe  | 1        |
      | admin@gpathome.com  | password | 4    | James      | Duncombe  | 0        |
      | gp@gpathome.com     | password | 3    | Justine    | Setchell  | 0        |
    Given there are the following medical profiles:
      | user_id | medical_history                                   | family_history      | regular_medication | allergies  | smoking | alcohol_units | exercise |
      | 1       | This is a really long and fun medical history...  | Lots of fun history | Paracetamol        | None       | Yes     | 10            | none     |
    And I am on the home page
    And I login as "admin@gpathome.com"
    Then I follow "Users"
    Then I follow "Credit" within "#user_1"

  @slow @javascript
  Scenario: Credit a user
    And I select "2" from "user_account_credit_quantity"
    And I fill in "Notes" with "These are some special notes..."
    And I press "Credit User"
    Then I should see "Account credited!"

  @slow @javascript
  Scenario: Remove a credit from a user
    And I select "2" from "user_account_credit_quantity"
    And I fill in "Notes" with "These are some special notes..."
    And I press "Credit User"
    Then I should see "Account credited!"
    And I follow "Delete" within "tbody"
    Then I should see "Credit removed"