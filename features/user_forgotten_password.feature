Feature: Forgotten Password
  I need to be able to reset my password as a user
  
  Background:
    Given there are the following groups:
      | admin_id | plan_id |
      | 1        | 0       |
    Given there are the following users:
      | email              | password | first_name | last_name | approved | group_id | role |
      | admin@gpathome.com | password | James      | Duncombe  | true     | 1        | 4    |
    Given there are the following plans:
      | name       | description             | price | plan_code |
      | Individual | Single plan for people  | 24.99 | ind001    |
      | Couple     | Couple plan for people  | 34.99 | cpl001    |
      | Family     | Family plan for people  | 44.99 | fam001    |
    Given there are the following credit types:
      | name                |
      | Online Consultation |
      | Home Visit          |
    Given there are the following plan credits:
      | plan_id | credit_id | number_of_credits |
      | 1       | 1         | 3                 |
      | 2       | 1         | 6                 |
      | 3       | 1         | 8                 |
    Given a clear email queue

  Scenario: Resetting user password
    Given I am on the home page
    When I follow "Login"
    And I follow "Forgot your password?"
    And I fill in "Email" with "admin@gpathome.com"
    And I press "Send me reset password instructions"
    Then I should see "You will receive an email with instructions about how to reset your password in a few minutes."
    And "admin@gpathome.com" opens the email with subject: "Reset password instructions"
    Then I should see "Change my password" in the email body
    And I follow "Change my password" in the email
    Then I should see "Change your password"