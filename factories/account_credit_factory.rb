FactoryGirl.define do
  factory :account_credit do
    quantity  1
    notes     'Credited one credit back for user'
  end
end