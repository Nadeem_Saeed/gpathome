FactoryGirl.define do
  factory :consultation do
    subject 'New subject'
    user_id 1
    # read_by_gp Time.now
    # read_by_user Time.now
  end

  factory :consultation_with_message, parent: :consultation do
    after :create do |c|
      FactoryGirl.create :message, consultation: c, user: c.user
    end
  end
end
