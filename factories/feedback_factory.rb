FactoryGirl.define do
  factory :feedback do
    feedback 'New feedback'
    url '/path/of/page'
    user_id 1
  end
end