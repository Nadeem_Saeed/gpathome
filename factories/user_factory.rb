FactoryGirl.define do
  factory :user do
    email     'user@gpathome.com'
    password  'password'
    role      1
  end

  factory :user_with_profile, parent: :user do
    profile { FactoryGirl.build(:profile) }
  end

  factory :user_with_profile_accepted, parent: :user do
    accepted_terms true
    plan 1
    role 1
    profile { FactoryGirl.build(:profile) }
  end

end
