FactoryGirl.define do
  factory :card_payment do
    payment_at  Date.today
    amount      60.00
  end
end
