FactoryGirl.define do
  factory :credit do
    name        'Online'
    description 'This is for online consultations'
    price       50.00
  end
end