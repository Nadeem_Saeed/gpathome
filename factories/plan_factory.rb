FactoryGirl.define do
  factory :plan do
    name        'Single'
    description 'Description'
    price       29.99
    max_users   1
    plan_code   'S001'
    status      1
  end

  # same again but with the plan credits filled out
  factory :plan_with_credits, parent: :plan do |plan|
    after :create do |p|
      FactoryGirl.create :plan_credit, plan: p
    end
  end
end
