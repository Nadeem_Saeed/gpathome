FactoryGirl.define do
  factory :profile do
    first_name       'Test'
    last_name        'User'
    phone_1          '011111 111111'
    date_of_birth    Time.now
    property_number  '4'
    street_address   'Street'
    postal_code      'XXX XXX'
  end
end