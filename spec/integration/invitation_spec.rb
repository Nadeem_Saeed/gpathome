require 'spec_helper'

describe 'Invitation emails' do

  def deliveries
    ActionMailer::Base.deliveries
  end

  def clear_queue
    deliveries.clear
  end

  def setup
    create :plan_with_credits, name: 'Individual'
    create :group, plan: Plan.first
    clear_queue
  end

  def first_user
    User.first
  end

  it "sends only one email when someone is invited" do
    setup
    Invites::Individual.invite(resource, Plan)
    ap deliveries
    # deliveries.last.subject.should_not match(/New Message/)
  end

end
