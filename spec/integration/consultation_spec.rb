require 'spec_helper'

describe 'Consultation emails' do

  def deliveries
    ActionMailer::Base.deliveries
  end

  def clear_queue
    deliveries.clear
  end

  def setup
    create :plan_with_credits
    create :group, plan: Plan.first
    create :user_with_profile_accepted, role: 2, group: Group.first, approved: true
    clear_queue
  end

  def first_user
    User.first
  end

  it "sends only one email on new consultation" do
    setup
    create :consultation_with_message, user_id: first_user.id
    deliveries.last.subject.should_not match(/New Message/)
  end

  it "sends a new message email if more than 1 message in the consultation" do
    setup
    create :consultation_with_message, user_id: first_user.id
    clear_queue
    create :message, consultation: Consultation.first, user: User.first
    deliveries.last.subject.should match(/New Message/)
  end
end
