require 'spec_helper'

describe Gpathome::Paymentable do

  let(:user) do
    FactoryGirl.create :credit
    FactoryGirl.create :plan_with_credits
    FactoryGirl.create :user_with_profile_accepted, role: 2,
                        card_payments: [FactoryGirl.build(:card_payment)]
  end

  describe '#last_card_payment_date' do
    it 'returns the last card payment date' do
      user
      ap User.last.last_card_payment_date
    end
  end
end
