require 'spec_helper'

describe Gpathome::AccountSetup::Gp do

  describe '#setup' do
    
    subject do
      FactoryGirl.create(:credit)
      FactoryGirl.create(:plan_with_credits)
      FactoryGirl.create(:user_with_profile_accepted, role: 3, email: 'gp@gp.com')
      User.first
    end

    it { subject.is_gp?.should be_true }
    it { subject.profile.full_name.should be_a String }
    it { subject.approved.should be_true }
    it { subject.group.should eq Group.find_by_group_type('gp') }

  end

end