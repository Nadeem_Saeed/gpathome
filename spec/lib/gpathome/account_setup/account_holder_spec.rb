require 'spec_helper'

describe Gpathome::AccountSetup::AccountHolder do

  describe '#setup' do

    subject do
      FactoryGirl.create(:credit)
      FactoryGirl.create(:plan_with_credits)
      FactoryGirl.create(:user_with_profile_accepted, role: 2)
      User.first
    end

    it { subject.profile.full_name.should be_a String }
    it { subject.medical_profile.should be_a MedicalProfile }
    it { subject.group.should eq Group.first }

  end

end