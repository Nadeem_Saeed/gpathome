require 'spec_helper'

describe Gpathome::AccountSetup::Dependent do

  describe '#setup' do
    
    subject do
      FactoryGirl.create(:credit)
      FactoryGirl.create(:plan_with_credits)
      FactoryGirl.create(:user_with_profile_accepted, role: 2, email: 'james@email.com')
      FactoryGirl.create(:user_with_profile_accepted, role: 6, invited_by: User.first)
      User.last
    end

    it { subject.is_dependent?.should be_true }
    it { subject.profile.full_name.should be_a String }
    #it { subject.approved.should be_true }
    it { subject.group.should eq User.find_by_email('james@email.com').group }

  end

end