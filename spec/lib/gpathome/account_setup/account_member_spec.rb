require 'spec_helper'

describe Gpathome::AccountSetup::AccountMember do

  describe '#setup' do
    
    subject do
      FactoryGirl.create(:credit)
      FactoryGirl.create(:plan_with_credits)
      FactoryGirl.create(:user_with_profile_accepted, role: 2, email: 'james@email.com')
      FactoryGirl.create(:user_with_profile_accepted, role: 1, invited_by: User.first)
      User.last
    end

    it { subject.is_account_member?.should be_true }
    it { subject.profile.full_name.should be_a String }
    it { subject.medical_profile.should be_a MedicalProfile }
    it { subject.group.should eq Group.first }

  end

end