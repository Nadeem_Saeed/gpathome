require 'spec_helper'

describe PlansController do

  include Devise::TestHelpers

  describe "Swtich plan" do

    before(:each) do
      @user = User.new({ :email => 'james@jamesduncombe.com', :password => 'password' })
      @user.role = 1
      @user.accepted_terms = true
      @user.build_profile(:first_name => 'James', :last_name => 'Duncombe')
      @user.build_medical_profile
      @user.save!
      @user.approve!
      sign_in @user
    end

    it "should upgrade plan" do
      params[:plan_code] = 'fam001'
      get :switch
      sign_out @user
    end

  end

end
