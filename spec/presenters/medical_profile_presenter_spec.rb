require 'spec_helper'

describe MedicalProfilePresenter do

  context 'with missing medical profile' do
    subject { MedicalProfilePresenter.new(build_stubbed(:user_with_profile_accepted)) }

    its(:full_name) { should eq('Test User') }
    its(:phone_number) { should eq('0011 11 1111111') }
    its(:height) { should eq('') }
    its(:weight) { should eq('') }
    its(:smoking) { should eq('') }
    its(:london_insurance) { should eq('') }
    its(:alcohol_units) { should eq('') }
    its(:gender) { should eq('') }
  end

  context 'with medical profile' do
    subject do
      MedicalProfilePresenter.new(
        build_stubbed(
          :user_with_profile_accepted,
          medical_profile: build_stubbed(:medical_profile, alcohol_units: 10, height: 140, weight: 200)
        )
      )
    end

    its(:height) { should eq('140 cm') }
    its(:weight) { should eq('200 kg') }
    its(:smoking) { should eq('') }
    its(:alcohol_units) { should eq(10) }
    its(:gender) { should eq('') }
  end

  describe '#smoking' do
    context 'set to true' do
      subject { MedicalProfilePresenter.new(build_stubbed(:user, medical_profile: build_stubbed(:medical_profile, smoking: true))).smoking }
      it { should eq('Yes') }
    end

    context 'set to false' do
      subject { MedicalProfilePresenter.new(build_stubbed(:user, medical_profile: build_stubbed(:medical_profile, smoking: false))).smoking }
      it { should eq('No') }
    end

    context 'set to nil' do
      subject { MedicalProfilePresenter.new(build_stubbed(:user, medical_profile: build_stubbed(:medical_profile, smoking: nil))).smoking }
      it { should eq('') }
    end
  end

  describe '#london_insurance' do
    context 'set to true' do
      subject { MedicalProfilePresenter.new(build_stubbed(:user, medical_profile: build_stubbed(:medical_profile, london_insurance: true))).london_insurance }
      it { should eq('Yes')}
    end

    context 'set to false' do
      subject { MedicalProfilePresenter.new(build_stubbed(:user, medical_profile: build_stubbed(:medical_profile, london_insurance: false))).london_insurance }
      it { should eq('No')}
    end

    context 'set to nil' do
      subject { MedicalProfilePresenter.new(build_stubbed(:user, medical_profile: build_stubbed(:medical_profile, london_insurance: nil))).london_insurance }
      it { should eq('') }
    end
  end
end