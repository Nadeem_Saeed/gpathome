require 'spec_helper'

describe Consultation do

  before(:all) do
    FactoryGirl.create(:plan_with_credits)
    user = FactoryGirl.create(:user_with_profile_accepted, role: 2)
    FactoryGirl.create(:consultation_with_message, user_id: user.id)
  end

  describe "#read_by_user?" do

    let(:consultation) { Consultation.last }

    context 'with blank read_by_user' do
      it 'returns false' do
        consultation.read_by_user = nil
        consultation.save!
        consultation.read_by_user?.should be_false
      end
    end

    context 'with read_by_user datetime' do
      it 'returns false' do
        consultation.read_by_user = Time.now
        consultation.save!
        consultation.read_by_user?.should be_false
      end
    end
  end
end
