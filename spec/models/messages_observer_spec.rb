require 'spec_helper'

describe MessageObserver do

  before(:all) do
    create(:plan_with_credits)
    user = create(:user_with_profile_accepted, role: 2)
    dependent = create(:user_with_profile_accepted, invited_by: user, role: 6)
    create(:consultation_with_message, user_id: dependent.id)
  end

  describe "#after_create" do

    context "with a dependent user" do
      it "gets called after a message is created" do
        Notifier.any_instance.should_receive(:new_message_admin_email).with(User.first)
        create(:message, consultation: Consultation.last, user: User.where(role: 6).first)
      end
    end

    context "with a regular user" do
      it "gets called after a message is created" do
        admin = create(:user_with_profile_accepted, email: 'admin@gpathome.com', role: 3)
        Notifier.any_instance.should_receive(:new_message_user_email).with(User.first, admin)
        create(:message, consultation: Consultation.last, user: admin)
      end
    end

  end
end
