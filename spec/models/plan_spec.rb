require 'spec_helper'

describe Plan do

  let(:user) do

    FactoryGirl.create :plan_with_credits, status: 2

    FactoryGirl.create :user_with_profile, accepted_terms: true, approved: true,
      plan: Plan.first.id, role: 2

  end

  before do
    user
  end

  describe '.public_plans' do
    it 'returns a list of public plans (not private or retired)' do
      Plan.public_plans.count.should == 1
    end
  end

end
