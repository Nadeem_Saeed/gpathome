require 'spec_helper'

describe NullMedicalProfile do

  subject { NullMedicalProfile.new }

  it { should respond_to(:height) }
  it { should respond_to(:weight) }
  it { should respond_to(:smoking) }
  it { should respond_to(:london_insurance) }

  its(:medical_history) { should eq('') }
  its(:family_history) { should eq('') }
  its(:alcohol_units) { should eq('') }
  its(:allergies) { should eq('') }
end