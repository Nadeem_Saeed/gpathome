require 'spec_helper'

describe User do

  let(:user) do

    # first create the types of credits
    FactoryGirl.create :credit

    # create the plan with credits
    plan = FactoryGirl.create :plan_with_credits

    # create the group with the plan attached
    group = FactoryGirl.create :group, plan: plan

    # create the user and attach to group
    FactoryGirl.create :user_with_profile, accepted_terms: true, approved: true, group: group

  end

  let(:setup_consultaions) do
    u = User.first
    f = FactoryGirl.create(:consultation)
    f.created_at = Time.now - 4.months
    f.save
    f = FactoryGirl.create(:consultation)
    f.created_at = Time.now - 5.months
    f.save
    f = FactoryGirl.create(:consultation)
    f.created_at = Time.now + 1.month
    f.save
    u.consultations = Consultation.all
    u.save!
  end

  before(:each) do
    user
    setup_consultaions
  end

  # Consultation methods

  describe '#number_of_online_consultations' do
    it 'returns the number of credits left' do
      u = User.first
      u.number_of_online_consultations.should eq 3
    end
  end

  describe '#remaining_online_consultations' do
    context 'with an credit rollover date that has just past' do
      it 'calculates how many credits are left for a user taking the rollover date into account' do
        g = Group.first
        g.created_at = (Time.now - 2.years) - 20.days
        g.save!
        u = User.first
        u.remaining_online_consultations.should eq 2
      end
    end
    context 'with an approaching credit rollover date' do
      it 'calculates how many credits are left for a user taking the rollover date into account' do
        g = Group.first
        g.created_at = (Time.now - 2.years) + 10.days
        g.save!
        u = User.first
        u.remaining_online_consultations.should eq 1
      end
    end
  end

  describe '#account_setup' do
    it 'sets up a users account with the correct role etc' do
      u = User.first
      puts u.role
    end
  end

end
