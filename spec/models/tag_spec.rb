require 'spec_helper'

describe Tag do

  before(:all) do
    3.times do |t|
      a = FactoryGirl.create(:asset, attachment_file_name: 'file_#{t}.jpg')
      ['medical', 'report', 'surgery', 'bloodtest'].each { |tag| FactoryGirl.create(:tag, name: tag) }
      # for some reason this seems to crash ruby if I put it into a loop
      a.tags << Tag.first
      a.tags << Tag.all[1]
      a.tags << Tag.last
    end
  end
  
  it 'creates a new tag' do
    FactoryGirl.create(:tag)
  end

  context 'adding a new tag to an asset' do
    it 'creates a new association' do
      Asset.first.tags.first.should eq Tag.first
    end
  end

  describe '#tag_list=' do
    it 'sets the correct tags from a comma seperated list' do
      a = FactoryGirl.create(:asset, attachment_file_name: 'file_2.jpg')
      a.tag_list = 't1, t2'
      a.save
      Asset.last.tag_list.should eq 't1, t2'
    end
  end

  describe '#tag_list' do
    it 'returns a list of tags comma seperated' do
      Asset.first.tag_list.should eq 'medical, bloodtest, report'
    end
  end

end