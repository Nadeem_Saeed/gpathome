require 'spec_helper'

describe Profile do

  it 'creates a new instance of Profile' do
    profile = FactoryGirl.create(:profile)
    profile.should be_an_instance_of Profile
  end

  describe '#phone_1' do
    context 'with normal UK phone number supplied' do

      let :profile do
        FactoryGirl.create(:profile, phone_1: '01372 459858')
      end

      it 'returns a correctly formatted UK phone number' do
        profile.phone_1.should eq '01372 459858'
      end

      it 'stores the number as an integer in the database' do
        profile[:phone_1].should be_an_instance_of Fixnum
      end

    end

    context 'with all 0s (invalid number)' do
      it 'correctly stores the number' do
        expect {
          FactoryGirl.create(:profile, phone_1: '0000 0000000')
        }.to raise_error ActiveRecord::RecordInvalid
      end
    end

  end

end