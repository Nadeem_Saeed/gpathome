require 'spec_helper'

describe ConsultationsHelper do

  let :setup do
    u = FactoryGirl.create(:user_with_profile, accepted_terms: true)
    u.consultations << FactoryGirl.create(:consultation_with_message)
    u.save
  end

  let(:user) { User.first }
  let(:consultation) { Consultation.first }
  let(:message) { consultation.messages.first }

  before(:each) { setup }
  
  describe '#from_who?' do
    it 'returns whether the consultation if from you or not' do
      helper.stub(:current_user) { user }
      helper.from_who?(message).should == 'you'
    end
  end

  describe '#assigned_to_who' do
    context 'consultation unassigned' do
      it { helper.assigned_to_who(Consultation.first).should eq 'nobody yet' }
    end
    context 'consultation assigned' do
      it 'returns the first name of the assigned GP' do
        consultation.update_attributes(assigned_to: User.first)
        helper.assigned_to_who(Consultation.first).should eq 'James'
      end
    end
  end

  describe '#mark_status' do
    context 'consultation closed' do
      it 'returns closed' do
        consultation.update_attributes(status: 2)
        helper.mark_status(consultation).should eq 'closed'
      end
    end
    context 'consultation open and not read by user' do
      it 'returns unread' do
        helper.mark_status(consultation).should eq 'unread'
      end
    end
  end

  describe '#view_or_reply_text' do
    context 'consultation closed' do
      it 'returns View' do
        helper.view_or_reply_text(consultation).should eq 'View / Reply'
      end
    end
    context 'consultation open' do
      it 'returns View / Reply' do
        consultation.update_attributes(status: 1)
        helper.view_or_reply_text(consultation).should eq 'View / Reply'
      end
    end
  end

  describe '#render_side_images' do
    context 'is admin / gp' do
      it 'render correct image' do
        message.user.stub(:is_admin_or_gp?).and_return(true)
        helper.render_side_images(message)
          .should eq image_tag('icons/gp-icon.png').gsub('images', 'assets')
      end
    end
    context 'is not admin / gp' do
      it 'render correct image' do
        helper.render_side_images(message)
          .should eq image_tag('icons/person-icon.png').gsub('images', 'assets')
      end
    end
  end

  describe '#message_time' do
    context 'as a gp' do
      it 'renders a full date time' do
        helper.stub(:current_user) { user }
        helper.current_user.stub(:is_admin_or_gp?).and_return(true)
        helper.message_time(message)
          .should eq message.created_at.strftime('%B %d, %Y %H:%M')
      end
    end
    context 'as a regular user' do
      it 'renders a friendly date / time' do
        helper.stub(:current_user) { user }
        helper.message_time(message)
          .should eq "#{distance_of_time_in_words(Time.now, message.created_at)} ago"
      end
    end
  end

end