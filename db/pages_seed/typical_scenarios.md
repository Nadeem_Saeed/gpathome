Caroline Barrington
-------------------

a busy working woman with cystitis symptoms

-   07:00 - Caroline wakes with cystitis symptoms.
-   07.15 - She logs on to GP at Home.
-   07.30 - GP at Home replies with symptomatic advice and instructions
    on what to do.
-   08:30 - Caroline collects sterile container from nearest pharmacy on
    way to work.
-   09:00 - Once at work, she provides a specimen. GP at Home arrange
    for it to be collected and couriered to lab and for antibiotics
    delivered to Caroline at work.
-   09:15 - Just 2 ½ hours after feeling unwell, Caroline starts her
    antibiotics and is on the road to recovery.

Receiving treatment early and efficiently meant she didn’t have to take
time off work and miss a key meeting.

48 hours later, she receives a follow up reply with the results of her
urine test.

Kirsty Selby
------------

gap year student

-   Kirsty's mum, who has family membership, contacts GP at Home
    regarding her daughter who is soon to travel through South America
    on her Gap Year.
-   Kirsty attends 92 Harley Street to discuss her itinerary and medical
    history in detail so that relevant advice regarding travel
    vaccinations can be given and prescriptions for any medications
    needed can be issued.
-   Kirsty takes away her personalised travel pack and is signed up to
    use GP at Home from anywhere in the world for the duration of her
    travel.

Steve Bond
----------

middle-aged man returning from ski holiday with sore knee

-   6pm Saturday - Steve contacts GP at Home with symptoms of a sore
    knee. GP at Home takes full history and gives symptomatic advice.
-   8am Wednesday - Steve’s symptoms don’t improve so he gets back in
    touch.
-   Later that day GP at Home arranges investigation with MRI at
    convenient private hospital.
-   Next day GP at Home arranges appointment for Steve to see a leading
    orthopaedic surgeon.
