<p></p>

We set up GP at Home because we felt there was an essential need for a
quick and convenient way of diagnosing medical problems and making
referrals or prescribing treatments. GP at Home is not an emergency
service but an additional reassurance for our registered patients.

GP at Home doctors are registered on the General Medical Council's GP
Register and have many years of experience working as General
Practitioners within the UK health care system as well as running their
own private clinic.

![Fiona-and-justine](/assets/static_pages/fiona-and-justine.jpg)

About the founders
------------------

Dr Setchell and Dr Payne established a private general practice at 92
Harley Street [GP at 92](http://www.gpat92.com) in October 2010 and
started GP at Home in 2012.

![Gpathome-about-us-dr-setchell](/assets/static_pages/gpathome-about-us-dr-setchell.jpg)

**Dr Justine Setchell** qualified from St. Mary’s Hospital Medical
School in 1990. Having completed her pre-registration training, she
embarked on her general practice training over the following 3 years,
covering trauma medicine, elderly medicine, psychiatry and obstetrics
and gynaecology, during which period she obtained her DRCOG and MRCGP.
She worked in NHS general practice for 14 years before moving into
private general practice in September 2009. She has been the
Occupational Health Physician for John Lewis, Oxford Street since 2008.

**Dr Fiona Payne** qualified from St Thomas’ Hospital Medical School in
1989. After completing pre-registration jobs, she became medical editor
for Dorling Kindersley, writing a series of books for the American
Medical Association, as well as The Family Medical Encyclopedia and
Picturepedia of the Human Body.

Dr Payne then returned to clinical medicine, working in General Surgery,
Oncology and Obstetrics and Gynaecology before completing her General
Practice training. She became a full time GP Principal in south-west
London in 1999 and moved into private General Practice in 2010. She
continues to do NHS sessions in South London.

![Gpathome-about-us-dr-payne](/assets/static_pages/gpathome-about-us-dr-payne.jpg)
