About the service
-----------------

#### What is GP at Home?

GP at Home is a unique bespoke, personalised service which allows you to
access e-Consultations with your private GP at a time convenient to you.

#### How does GP at Home work?

Your membership fee allows you online access to your private GP 7am-7pm
(GMT) 7 days a week.

#### What sort of problems can you deal with?

We have many years of General Practice experience and can deal with all
the usual General Practice complaints. For example, we can provide
symptomatic advice, prescribe medication, refer to specialists and
arrange investigations as necessary - all online.

About e-Consultations {#e-consultation}
---------------------

#### What hours are you available and how quickly will you respond?

The service is available 7am-7pm (GMT), 7 days a week. We will aim to
reply within 2 hours but will often be able to do so straight away. Any
requests received at night will take priority next morning.

#### Will I deal with the same doctor throughout my e-Consultation?

Yes, you’ll receive help and support from the same private doctor
throughout, circumstances permitting.

#### What if I need to be seen instead or need an investigation?

If this were the case, the GP would advise you accordingly. You would be
welcome to come and see us although you are not obliged to. You can see
whoever you would like to. We can arrange a full range of investigations
from our surgery but can also arrange various tests in the comfort of
your own home if requested (for an additional fee). Investigations such
as X-rays and scans can be arranged at a private hospital of your
choice.

#### How will I access a prescription if I need it?

We can arrange for your medication to be delivered via Independent
Dispensary (within 2 hours and free to London postcodes. Available to
other addresses within the M25 for a small fee and nationwide/ worldwide
by arrangement).

#### What if I need a doctor in an emergency?

GP at Home is not an emergency service. In an emergency situation, you
would need to go to A & E.

#### Can I access my records from anywhere in the world?

Yes, because your medical and prescription details are stored on a web
portal, you will always have access to them, wherever you are in the
world. We also offer an additional facility on request for you to store
useful information such as details of your private Health Insurer e.g.,
contact numbers, membership number, travel insurance details, EHIC
number etc.

About membership {#membership}
----------------

#### Why do I have to become a member?

When you join GP at Home, we ask you to complete a detailed health
questionnaire, which we can refer to during any current or future
consultations, ensuring that we can provide you with the best possible
medical care.

#### Will my membership be covered by my private medical insurance?

Private general practice services are not generally included in PMI
plans but some International plans do cover this. Onward referral to a
specialist is usually covered (please check your policy for details).

#### What if I want to cancel my membership?

You can cancel at any time but before you do, we would be happy to
discuss any concerns.

Privacy of information
----------------------

#### How will I know that my details are being stored securely?

All medical details are stored securely on a UK-based private database.

#### How do you prevent other people from looking at my records?

As a member of GP at Home, you have your own unique password and login.

#### What if my teenager wants to consult you privately?

We have a parental control on the family membership that allows you to
set the required privacy level for 14 -16 year olds.
