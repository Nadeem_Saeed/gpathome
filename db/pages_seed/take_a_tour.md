GP at Home is a unique online consultation service providing secure
one-to-one, medical advice, treatment and referal as needed.

Registering is simple
---------------------

Choose your [membership plan](/membership_plans).

You will create a personal secure record of your medical history,
allergies and medication.

Your private and confidential web portal showing your history of GP at
Home e-Consultations can only be accessed by you and your doctor.

![Screenshot-dashboard-gpathome](/assets/static_pages/screenshot-dashboard-gpathome.png)

Simply login each time you need to get in touch if you feel unwell or
have a concern. Please remember this is NOT an emergency service.

Provide as much detail as possible. Your private doctor will diagnose
the medical condition and prescribe treatment as required, and arrange
medication to be delivered if needed.

We're available 7am – 7pm (GMT), 7 days a week. We will respond within
1-2 hours during these hours. Any requests received at night will take
priority next morning.
