<p> </p>

GP at Home is an innovative [membership](/membership_plans) service that
gives you direct online access to your [private doctor](/about_us_raw).

Log on to your own secure portal and tell us about your illness, health
problem or concern. A private doctor will diagnose the medical condition
online and prescribe treatment as required, and arrange medication to be
delivered if needed.

Throughout your e-Consultation, you’ll receive all the one-to-one advice
and support you need from the same doctor.

Take a look at some [typical scenarios](/typical_scenarios_raw).

![Gpathome-our-services-1](/assets/static_pages/gpathome-our-services-1.jpg)

Membership benefits
-------------------

We can also arrange all of the following:

-   Rapid access to a wide range of top specialists and therapists
-   Referral to all the leading private hospitals
-   Immediate investigations in the convenience of your home, such as
    blood samples by a phlebotomist who will then take the sample
    straight to the lab for testing
-   Free prescription delivery, Monday to Friday, within 2 hours to
    London postcodes (other areas within the M25 for a small additional
    fee)
-   e-Consultations for students while abroad or for family members away
    on business
-   [Gap year/travel kits](/typical_scenarios_raw) (you may like to use
    a consultation for tailor-made medical advice before travelling in
    conjunction with a consultation for tailor-made medical advice
    before travelling).
-   General Medical Enquiries
-   Facility to store useful information such as passport, travel
    insurance details etc and access securely from anywhere in the
    world.

![Gpathome-our-services-2](/assets/static_pages/gpathome-our-services-2.jpg)