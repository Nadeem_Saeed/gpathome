Welcome to GP at Home
=====================

GP at Home is a unique, bespoke membership service offering personal
medical consultations online. Our e-Consultations provide expert,
secure, convenient and [cost effective advice](/membership_plans) on the
go.

Request an e-Consultation and our team of [Private Doctors](/about_us)
will respond quickly - often right away.

*Please note: This site is currently solely for the use of our
registered patients at [92 Harley Street](http://www.gpat92.com). If you
are not a registered patient and wish to use the service, please email
us at [practice@gpathome.com](mailto:practice@gpathome.com) to discuss
consultation options.*