STANDARD TERMS AND CONDITIONS OF SUPPLY OF SERVICES

By requesting us to provide any services to you, you hereby agree to be bound by these terms.


### 1. Interpretation

"the services" means the services offered by GPatHome through any name applicable at the time.

"you" means the person who has requested provision by us of the private medical service.

"third party products" means any third party products or services including (but not limited to) pathology, imaging, courier services, information technology providers.

"GP" any qualified General Medical Practitioner who supplies the service to you under this agreement.


### 2. Contracting Parties

Your contract is with GPatHome, which trades as GPatHome.com.  The business is a limited company registered in England and Wales with company number 7738367 and the registered office address is Maple House, Rookery Road, Monewden, Suffolk IP13 7DD.

1.  You are the person who has requested GPatHome to provide services to you.

2.  Persons who are not party to this agreement shall have no rights under the Contracts (Rights of Third Parties) ACT 1999 to enforce any term of this agreement. 


### 3. Our Professional Obligations

1.  We will supply you with a GP as set out below who will provide to you medical services with a reasonable standard of care and skill.

2.  All of our GPs providing the medical services to you do so as principal and we cannot be liable for any loss or damage howsoever caused by the GP. We will observe the requirements and regulations that apply to us from the Care Quality Commission and require the Medical Practitioners to comply with the principles and values on which good practice is founded, as laid down by the General Medical Council. 

3.  The Medical Practitioners are all subject to regular appraisal and adherence to the process of continuing medical education under the auspices of the relevant Royal Colleges.

4.  Our GPs are self-employed professionals acting as principals who are individually responsible to you as medical practitioners for the quality and provision of their services and are fully indemnified by their relevant Medical Defence Union.

5.  We comply with the requirements of the Data Protection Act 1990 and the principles in relation to data protection.  By agreeing these terms you are agreeing to the processing of personal data to enable us to provide the medical service to you.

6.  We will honour all rights confessed on you by the Distance Selling Regulations is so far as they apply to the services being supplied to you. 


### 4. The Services

1.  GPatHome employees and agents are not authorised to make any representations concerning the services unless confirmed by a duly authorised representative of GPatHome in writing.  These terms and conditions constitute the entire agreement between you and GPatHome with respect to the provision of medical services.

2.  You acknowledge that GPatHome gives no warranties or representations to you (whether express or implied) in respect of the services.  In particular, whilst every effort is made to achieve any consultation response times quoted by GPatHome, no warranty or guarantee is given that such consultation response time will be achieved in any particular instance. 

3.  GPatHome gives no warranty or guarantee as to the availability of the service or the locations that the service will be delivered in or from. 

4.  GPatHome reserves the right to refuse to administer a treatment or arrange any test for or to you. 

5.  The GPs are able to provide a private prescription as needed. The price of that medication will be clearly explained by Independent Dispensary and you will be invoiced for any medication provided.

6.  Please be aware that the GPs will not prescribe controlled drugs. 

7.  Although GPatHome will try and accommodate for your wishes, GPatHome cannot make any guarantees as to the gender or languages spoken of their doctors.


### 5. Your Obligations

1.  You shall provide to GPatHome such information as is reasonably necessary to enable the consultation to take place including but not restricted to contact details, medical history and details of your current illness, it is vital that this information is accurate and GPatHome accepts no responsibility for any adverse events or errors, which flow from the withholding of or provision of inaccurate information. 

2.  GPatHome is not an emergency service and is not able to respond rapidly to life threatening situations.  If you have a life threatening condition or one that requires very rapid response, you should either call 999 for an ambulance or go straight to the accident and emergency department of the nearest hospital, whichever is most appropriate.  Examples of this include chest pain and strokes. 

3.  GPatHome has a zero tolerance policy with respect to the abuse of our website or employees. Where a breach of this policy has occurred GPatHome reserves the right to refer complaints, together with appropriate supporting evidence to the police.      


### 6. Investigations

1.  Although most blood and laboratory test results are received within 24 hours, please be aware that certain test results may take longer to receive and some can only be disclosed subject to appropriate pre and post test counselling. 

2. The GP will make every effort to inform you of the investigation results, however, GPatHome takes no responsibility for the results if you fail to maintain your membership and we no longer have access to your portal.


### 7. Prices and Payment

1.  Prices are subject to change without prior notice and prices of medication and investigations may vary depending on type.

2.  Most prices will be published on the website but GPatHome will be able to advise where known, of other anticipated costs. 

3.  Quotes for services may change if the patients assumed diagnosis turns out to be different from or more complicated than that originally quoted for. 

4.  All membership fees must be paid for prior to an initial consultation.  

5.  GPatHome on its web site provides a list of prices, which are a guide, and subject to variation depending upon the treatment and services provided.

6.  There may also be additional charges made by the third parties after the consultation, which may for example include the cost of medication at a pharmacy, an investigation at a private, hospital or clinic.  You should seek advice on these changes direct from the third party

7.  No payment will be deemed to have been received until GPatHome have received the payment in full in cleared funds.

### 8. Communication Confidentiality and Privacy

1.  GPatHome fully complies with the Data Protection Legislation and Medical Confidentiality guidelines.  

2.  All medical information will be kept confidential and it will be only disclosed to those involved with your care or treatment, including your GP and to their employees or agents.  If you are seeking payment from a third party for the costs of your treatment details of your treatment may have to be disclosed to them.

3.  Information may be disclosed to others with a view to preventing fraud or improper claims. 

4.  All test results, invoices, investigation results and other information of a confidential nature received from a third party in respect of you will remain confidential.

5.  It will not, without your prior consent, disclose such confidential information about you, other than to its professional staff, doctors working with GPatHome, independent consultants and/or persons to whom it has delegated the running of aspects of the service and who require information so that the services can be provided to you.

6.  The restrictions in paragraph 8.1 do not apply to information which: (i) was in GPatHome’s possession prior to disclosure by you to us; (ii) hereafter comes into the public domain other than by default of GPatHome; or (iii) was lawfully received by GPatHome from a third party acting in good faith having a right of further disclosure or (iv) is required by law to be disclosed by GPatHome.


### 9.  Third Party Products

1.  The services supplied to you or the patient relies on the use of
    third party products and services.  These include, but are not
    limited to, medications, pathology and investigations.

2. Where these are supplied or offered, this will be accordance with
    the relevant licensor’s or third party’s standard terms and
    conditions. 

3.  Any lists of third party services provided by GPatHome serve as a
    guide only and GPatHome make no guarantee as to availability of
    services or accuracy as to contact details. 


4.  Where you use such third party products or services, you do so on
    the basis that GPatHome appoints the relevant supplier of the Third
    Party Product or Service as your agent, with a direct contractual
    relationship arising between you and the relevant supplies.  Whilst
    GPatHome shall make reasonable endeavours to ensure that any Third
    Party Product or Service are supplied to your satisfaction, GPatHome
    accepts no liability in this respect and you are responsible for
    checking compliance with their requirements and seeking any legal
    redress against the supplier in the event that any problem arises.

5.  The payment amount charged by the Third Party Product or Service
    will vary and the additional cost of the Third Party Product or
    Service will be paid directly to the supplier of the Third Party
    Product or Service. 

6.  It will be the responsibility of you to check the availability and
    price of any service with the third party supplier unless otherwise
    indicated by GPatHome and where necessary calling first to find out
    if an appointment is required.  The third party supplier on arrival
    or delivery of the service may require payment, which is your
    responsibility to pay.  If you have insurance that covers this fee,
    you should contact your insurer to explain the situation and get
    details of how payment will be processed when attending for the
    investigation. 

### 10. Limitation of Liability

1. You agree that if, as a matter of law, a duty of care, which would otherwise be owed to you by us, is hereby excluded.  You further agree that you will not bring any claim against GPatHome in respect of any loss or damage that you or any person or company associated with you suffer or incur, directly or indirectly, in connection in any way with the services, with any advice given to or with other services provided to you. 

2.  All warranties, conditions and other terms (whether implied by statute or otherwise) are, to the fullest extent permitted by law, excluded from the contract.

3.  Nothing in these conditions excludes any liability or claim that cannot be excluded under English law or any liability or claim that cannot be excluded under any relevant professional rile or regulation. 

4.  GPatHome will not be liable to you in contract, tort (including without limitation, negligence), misrepresentation or otherwise for any:

    - economic loss of any kind (including, without limitation, loss of use, profit, anticipated profit, business, contracts overhead recovery, revenue or anticipated savings);

    - any damage to your reputation or goodwill; or

    - any other special, indirect or consequential loss or damage (even if we have been advised of such loss or damage) arising out of or in connection with the contract. 

5.  GPatHome's total liability in contract, tort (including without limitation, negligence), misrepresentation or otherwise arising out of or in connection with this contract (a “default”) will be limited to the price paid or payable in respect of the services (or the relevant part of the service) to which the default arises. 

6.  The provisions of this clause shall survive the termination or expiry (for whatever reason) of this contract.


### 11. Force Majeure

If the performance of this agreement of any obligation under it is prevented, restricted or interfered with by reason of circumstances beyond the reasonable control of that party obliged to perform it (including, without limitation, flood, fire, storm, strike, lockout, sabotage, terrorist act, civil commotion and government intervention) the party so affected shall (upon giving prompt notice thereof to the other party) be excused from performance to the extent only of the prevention, restriction or interference, provided always that the party so affected shall use all reasonable endeavours to avoid or remove the causes of non-performance and shall continue performance as expeditiously as possible as soon as such causes have been removed.


### 12. Termination of Services

1. GPatHome reserves the right to terminate the services, or not offer a service to persons in situations that for example include but are not limited to:

    (a) Where there has been a previous abuse of the service, or

    (b) Where there has previously been a serious breakdown in the professional relationship with GPatHome and/or the Healthcare professional insomuch that safe and effective health care may not be able to be effectively delivered. 

2. Such a decision is at the discretion of the GP, the Directors or any other relevant representative of GPatHome, and may at their sole discretion extend to the previous clients and can include any of your known associates or family, for example another family member residing at the same address.


### 13. Marketing, Website and Medical Information

1. Any medical information contained within the GPatHome website, flyers or handouts, is to be treated for general information only and does not constitute business, medical or other professional advice, and is subject to change.   The content should not be used for diagnosis or treatment of any medical condition and GPatHome cannot be held liable for any diagnosis made based on the content of this website of for any loss, damage, injury or expense resulting from the use of any of the content of this website.  A responsible and licensed medical practitioner, whom you should consult if you have any concerns regarding your health, should make diagnosis.  Other appropriate professionals should also be consulted where appropriate.

2. GPatHome does not accept any liability for the contents of any external sites listed, nor does it endorse any commercial product or service mentioned or advised on the website.  While we have taken care to compile accurate information, we cannot guarantee its correctness and completeness. 

3. While we have taken every care to compile accurate information and to keep it up-to-date, we cannot guarantee its correctness and completeness.  The information provided on this site does not constitute business, medical or other professional advice, and is subject to change.  We do not accept responsibility for any loss, damage or expense resulting from the use of this information. 

4. We cannot guarantee uninterrupted access to this website, or the sites to which it links.  We accept no responsibility for any damages arising from the loss of use of this information.


### 14. Help us to give you the right service/Complaints procedure

If at any time you would like to discus with us how our service to you could be improved, or if you are dissatisfied with the service you have received, please let us know by writing to us at our address.


### 15. Our Complaints Procedure

We aim to improve our services at all times.  Complaints, comments or suggestions should be made to either of the Medical Directors in writing.

92 Harley Street,
London W1G 7HU.

Email [gpathome@me.com](mailto:gpathome@me.com)

Complaints can be made either verbally or in writing.  We also accept comments and complaints made by others acting on behalf of another person where that person lacks the confidence or capacity to make comment or complaint.  Please note that we will not discriminate against anyone who makes a comment or complaint and doing so will not have a negative impact on the care, treatment or support that is offered by GPatHome.  We aim to acknowledge all complaints within 2 working days of receipt.  We will then investigate the complaint fully, which will include contacting and interviewing any relevant persons.  We will aim to send a letter detailing the outcome within 20 working days.  If the investigation is still underway, we will inform you of this in writing.  You are then invited to write back or contact us again letting us know whether you are satisfied with the outcome. We will then aim to respond to this within another 14 days. In appropriate circumstances, we should be able to arrange face to face meetings with one or both of the Directors of GPatHome, and when possible, with any other involved parties.

### 16. General

1. You may not without prior written consent assign or transfer the contract or any part of it to any other person.

2. GPatHome may without your prior written consent assign, transfer or subcontract the contract or any other part of it to any other person.

3. Each of rights or remedies under these conditions are without prejudice to any other right or remedy which we may have under conditions or otherwise. 

4. Any notice or other document to be served under the contract must be in writing and may be delivered or sent prepaid first class post or facsimile transmission.  Any notice or document shall be deemed served, if delivered at the time of delivery, if posted, 48 hours after posting and if sent by facsimile transmission, at the time of transmission. 

5. If any provision of the contract is found by any court, tribunal or administrative body of competent jurisdiction to be wholly or partly illegal, invalid, void, unenforceable or unreasonable, it will, to the extent of such illegality, invalidity, voidness, unenforceability or unreasonableness, be deemed severable and the remaining provisions of the contract and the remainder of such provision shall continue in full force and effect. 

6. Failure or delay by either partying exercising any right or remedy provided by the contract or by law will not be construed as a waiver of such right or remedy or a waiver of any other right or remedy.

7. A person who is not a party to the contract will have no right under the contracts (Rights of Third Parties) Act 1999 to enforce any term of the contract. 

8. The contract will be governed by English Law and the parties submit to the exclusive jurisdiction of the English courts.

9. The terms and conditions can be changed at any time by GPatHome to adapt the changing statutory requirements and trends in the healthcare management. 

10. In performing the services, GPatHome may process personal data belonging to you and agrees that it will in respect of such personal data observe all the obligations pertaining to a data processor under the Data Protection Act 1998.