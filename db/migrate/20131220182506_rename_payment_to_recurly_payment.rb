class RenamePaymentToRecurlyPayment < ActiveRecord::Migration
  def up
    rename_table :payments, :recurly_payments
  end

  def down
    rename_table :recurly_payments, :payments
  end
end
