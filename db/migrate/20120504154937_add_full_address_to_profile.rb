class AddFullAddressToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :street_address, :string
    add_column :profiles, :extended_address, :string
    add_column :profiles, :locality, :string
    add_column :profiles, :region, :string
    add_column :profiles, :postal_code, :string
  end
end
