class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.text :message
      t.references :consultation

      t.timestamps
    end
    add_index :messages, :consultation_id
  end
end
