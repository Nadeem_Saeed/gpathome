class AddSubjectToConsultation < ActiveRecord::Migration
  def change
    add_column :consultations, :subject, :string
  end
end
