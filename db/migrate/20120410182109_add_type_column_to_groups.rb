class AddTypeColumnToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :group_type, :string
    add_index :groups, :group_type
  end
end
