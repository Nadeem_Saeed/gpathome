class AddPropertyNumberToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :property_number, :string
  end
end
