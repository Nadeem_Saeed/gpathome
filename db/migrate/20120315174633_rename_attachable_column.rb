class RenameAttachableColumn < ActiveRecord::Migration
  def change
    rename_column :assets, :attachable, :attachable_id
  end
end
