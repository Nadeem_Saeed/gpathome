class AddMedicalInsuranceToMedicalProfile < ActiveRecord::Migration
  def change
    add_column :medical_profiles, :insurance_company, :string
    add_column :medical_profiles, :policy_number, :string
    add_column :medical_profiles, :london_insurance, :boolean
  end
end
