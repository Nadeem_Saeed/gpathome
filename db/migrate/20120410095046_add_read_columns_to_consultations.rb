class AddReadColumnsToConsultations < ActiveRecord::Migration
  def change
    add_column :consultations, :read_by_user, :timestamp
    add_column :consultations, :read_by_gp, :timestamp
    add_index :consultations, [:read_by_user, :read_by_gp]
  end
end
