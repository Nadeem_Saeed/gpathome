class AddAssignedToToConsultations < ActiveRecord::Migration
  def change
    add_column :consultations, :assigned_to_id, :integer
  end
end
