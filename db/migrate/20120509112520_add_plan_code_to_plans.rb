class AddPlanCodeToPlans < ActiveRecord::Migration
  def change
    add_column :plans, :plan_code, :string
  end
end
