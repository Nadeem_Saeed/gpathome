class CreatePlanCredits < ActiveRecord::Migration
  def change
    create_table :plan_credits do |t|
      t.references :plan
      t.references :credit
      t.integer :number_of_credits

      t.timestamps
    end
    add_index :plan_credits, :plan_id
    add_index :plan_credits, :credit_id
  end
end
