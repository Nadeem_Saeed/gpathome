class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.text :body
      t.text :slug
      t.boolean :visible

      t.timestamps
    end
  end
end
