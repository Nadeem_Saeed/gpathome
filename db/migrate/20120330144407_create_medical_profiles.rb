class CreateMedicalProfiles < ActiveRecord::Migration
  def change
    create_table :medical_profiles do |t|
      t.text :medical_history
      t.text :family_history
      t.text :regular_medication
      t.text :allergies
      t.boolean :smoking
      t.integer :alcohol_units, :limit => 3
      t.text :exercise
      t.references :user

      t.timestamps
    end
    add_index :medical_profiles, :user_id
  end
end
