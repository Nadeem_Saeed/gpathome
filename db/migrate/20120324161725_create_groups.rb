class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.integer :admin_id
      t.references :plan

      t.timestamps
    end
    add_index :groups, :plan_id
  end
end
