class CreateCardPayments < ActiveRecord::Migration
  def change
    create_table :card_payments do |t|
      t.integer :user_id
      t.decimal :amount, :precision => 8, :scale => 2
      t.datetime :payment_at

      t.timestamps
    end
    add_index :card_payments, :user_id
  end
end
