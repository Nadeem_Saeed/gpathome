class RemovePhone2FromProfile < ActiveRecord::Migration
  def up
    remove_column :profiles, :phone_2
  end

  def down
    add_column :profiles, :phone_2, :integer, :limit => 8
  end
end
