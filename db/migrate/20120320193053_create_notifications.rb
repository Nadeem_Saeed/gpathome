class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.boolean :send_txt_message, :default => true
      t.boolean :send_email, :default => true
      t.references :user

      t.timestamps
    end
    add_index :notifications, :user_id
  end
end
