class CreateAccountCredits < ActiveRecord::Migration
  def change
    create_table :account_credits do |t|
      t.references :user
      t.references :group
      t.references :credit
      t.integer :quantity, :default => 1

      t.timestamps
    end
    add_index :account_credits, :user_id
    add_index :account_credits, :group_id
    add_index :account_credits, :credit_id
  end
end
