class AddMaxUsersColumnToPlan < ActiveRecord::Migration
  def change
    add_column :plans, :max_users, :integer
  end
end
