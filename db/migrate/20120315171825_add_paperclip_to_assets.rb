class AddPaperclipToAssets < ActiveRecord::Migration
  def change
    add_column :assets, :attachment_file_name, :string

    add_column :assets, :attachment_content_type, :string

    add_column :assets, :attachment_file_size, :integer

  end
end
