class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.references :user
      t.string :uuid
      t.string :state

      t.timestamps
    end
  end
end
