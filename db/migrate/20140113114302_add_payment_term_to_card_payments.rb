class AddPaymentTermToCardPayments < ActiveRecord::Migration
  def change
    add_column :card_payments, :payment_term, :integer
  end
end
