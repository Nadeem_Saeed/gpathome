class AddPriceToCredit < ActiveRecord::Migration
  def change
    add_column :credits, :price, :decimal, :precision => 8, :scale => 2

  end
end
