class AddNotesToAccountCredits < ActiveRecord::Migration
  def change
    add_column :account_credits, :notes, :text
  end
end
