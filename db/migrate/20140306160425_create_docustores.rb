class CreateDocustores < ActiveRecord::Migration
  def change
    create_table :docustores do |t|
      t.integer :user_id

      t.timestamps
    end
    add_index :docustores, :user_id
  end
end
