class AddHeightAndWeightToMedicalProfile < ActiveRecord::Migration
  def change
    add_column :medical_profiles, :height, :integer
    add_column :medical_profiles, :weight, :integer
  end
end
