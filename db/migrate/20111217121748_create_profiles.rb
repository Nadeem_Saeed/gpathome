class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :first_name
      t.string :last_name
      t.date :date_of_birth
      t.boolean :sex
      t.integer :phone_1, :limit => 8
      t.integer :phone_2, :limit => 8
      t.text :address
      t.references :user

      t.timestamps
    end
    add_index :profiles, :user_id
  end
end
