class AddReadAndUnderstoodToMedicalProfile < ActiveRecord::Migration
  def change
    add_column :medical_profiles, :read_and_understood, :boolean, default: false
  end
end
