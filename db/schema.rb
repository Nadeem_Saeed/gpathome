# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20161119074119) do

  create_table "account_credits", :force => true do |t|
    t.integer  "user_id"
    t.integer  "group_id"
    t.integer  "credit_id"
    t.integer  "quantity",   :default => 1
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
    t.text     "notes"
  end

  add_index "account_credits", ["credit_id"], :name => "index_account_credits_on_credit_id"
  add_index "account_credits", ["group_id"], :name => "index_account_credits_on_group_id"
  add_index "account_credits", ["user_id"], :name => "index_account_credits_on_user_id"

  create_table "assets", :force => true do |t|
    t.integer  "attachable_id"
    t.string   "attachable_type"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
  end

  create_table "card_payments", :force => true do |t|
    t.integer  "user_id"
    t.decimal  "amount",       :precision => 8, :scale => 2
    t.datetime "payment_at"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.integer  "payment_term"
  end

  add_index "card_payments", ["user_id"], :name => "index_card_payments_on_user_id"

  create_table "consultations", :force => true do |t|
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.string   "subject"
    t.integer  "user_id"
    t.integer  "status",         :default => 0
    t.integer  "assigned_to_id"
    t.datetime "read_by_user"
    t.datetime "read_by_gp"
  end

  add_index "consultations", ["read_by_user", "read_by_gp"], :name => "index_consultations_on_read_by_user_and_read_by_gp"

  create_table "credits", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.decimal  "price",       :precision => 8, :scale => 2
  end

  create_table "docustores", :force => true do |t|
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "docustores", ["user_id"], :name => "index_docustores_on_user_id"

  create_table "feedbacks", :force => true do |t|
    t.integer  "user_id"
    t.text     "feedback"
    t.text     "url"
    t.text     "meta"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "email"
  end

  create_table "groups", :force => true do |t|
    t.integer  "admin_id"
    t.integer  "plan_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "group_type"
  end

  add_index "groups", ["group_type"], :name => "index_groups_on_group_type"
  add_index "groups", ["plan_id"], :name => "index_groups_on_plan_id"

  create_table "medical_profiles", :force => true do |t|
    t.text     "medical_history"
    t.text     "family_history"
    t.text     "regular_medication"
    t.text     "allergies"
    t.boolean  "smoking"
    t.integer  "alcohol_units",       :limit => 3
    t.text     "exercise"
    t.integer  "user_id"
    t.datetime "created_at",                                          :null => false
    t.datetime "updated_at",                                          :null => false
    t.integer  "height"
    t.integer  "weight"
    t.string   "insurance_company"
    t.string   "policy_number"
    t.boolean  "london_insurance"
    t.boolean  "read_and_understood",              :default => false
  end

  add_index "medical_profiles", ["user_id"], :name => "index_medical_profiles_on_user_id"

  create_table "messages", :force => true do |t|
    t.text     "message"
    t.integer  "consultation_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.integer  "user_id"
  end

  add_index "messages", ["consultation_id"], :name => "index_messages_on_consultation_id"
  add_index "messages", ["user_id"], :name => "index_messages_on_user_id"

  create_table "pages", :force => true do |t|
    t.text     "body"
    t.text     "slug"
    t.boolean  "visible"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.text     "name"
  end

  create_table "plan_credits", :force => true do |t|
    t.integer  "plan_id"
    t.integer  "credit_id"
    t.integer  "number_of_credits"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "plan_credits", ["credit_id"], :name => "index_plan_credits_on_credit_id"
  add_index "plan_credits", ["plan_id"], :name => "index_plan_credits_on_plan_id"

  create_table "plans", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.decimal  "price",       :precision => 8, :scale => 2
    t.integer  "status"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.integer  "max_users"
    t.string   "plan_code"
  end

  create_table "profiles", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.date     "date_of_birth"
    t.boolean  "gender"
    t.integer  "phone_1",          :limit => 8
    t.integer  "user_id"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.string   "street_address"
    t.string   "extended_address"
    t.string   "locality"
    t.string   "region"
    t.string   "postal_code"
    t.string   "property_number"
  end

  add_index "profiles", ["user_id"], :name => "index_profiles_on_user_id"

  create_table "recurly_payments", :force => true do |t|
    t.integer  "user_id"
    t.string   "uuid"
    t.string   "state"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "taggings", :force => true do |t|
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tag_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "tags", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                               :default => "",    :null => false
    t.string   "encrypted_password",                  :default => ""
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.integer  "sign_in_count",                       :default => 0,     :null => false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                             :null => false
    t.datetime "updated_at",                                             :null => false
    t.integer  "role",                   :limit => 1, :default => 1
    t.integer  "group_id"
    t.boolean  "approved",                            :default => false, :null => false
    t.string   "invitation_token"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.boolean  "active",                              :default => true,  :null => false
    t.boolean  "accepted_terms",                      :default => false
    t.datetime "invitation_created_at"
  end

  add_index "users", ["approved"], :name => "index_users_on_approved"
  add_index "users", ["email"], :name => "index_users_on_email"
  add_index "users", ["invitation_token"], :name => "index_users_on_invitation_token"
  add_index "users", ["invited_by_id"], :name => "index_users_on_invited_by_id"
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
