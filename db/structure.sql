CREATE TABLE `account_credits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `credit_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `index_account_credits_on_credit_id` (`credit_id`),
  KEY `index_account_credits_on_group_id` (`group_id`),
  KEY `index_account_credits_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `assets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attachable_id` int(11) DEFAULT NULL,
  `attachable_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `attachment_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment_file_size` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `card_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `amount` decimal(8,2) DEFAULT NULL,
  `payment_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `payment_term` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_card_payments_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `consultations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `assigned_to_id` int(11) DEFAULT NULL,
  `read_by_user` datetime DEFAULT NULL,
  `read_by_gp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_consultations_on_read_by_user_and_read_by_gp` (`read_by_user`,`read_by_gp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `credits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `price` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `docustores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_docustores_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `feedbacks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `feedback` text COLLATE utf8_unicode_ci,
  `url` text COLLATE utf8_unicode_ci,
  `meta` text COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) DEFAULT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `group_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_groups_on_group_type` (`group_type`),
  KEY `index_groups_on_plan_id` (`plan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `medical_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `medical_history` text COLLATE utf8_unicode_ci,
  `family_history` text COLLATE utf8_unicode_ci,
  `regular_medication` text COLLATE utf8_unicode_ci,
  `allergies` text COLLATE utf8_unicode_ci,
  `smoking` tinyint(1) DEFAULT NULL,
  `alcohol_units` mediumint(9) DEFAULT NULL,
  `exercise` text COLLATE utf8_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `height` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `insurance_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `policy_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `london_insurance` tinyint(1) DEFAULT NULL,
  `read_and_understood` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_medical_profiles_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text COLLATE utf8_unicode_ci,
  `consultation_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_messages_on_consultation_id` (`consultation_id`),
  KEY `index_messages_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `plan_credits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_id` int(11) DEFAULT NULL,
  `credit_id` int(11) DEFAULT NULL,
  `number_of_credits` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_plan_credits_on_credit_id` (`credit_id`),
  KEY `index_plan_credits_on_plan_id` (`plan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `price` decimal(8,2) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `max_users` int(11) DEFAULT NULL,
  `plan_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `gender` tinyint(1) DEFAULT NULL,
  `phone_1` bigint(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `street_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `extended_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locality` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `property_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_profiles_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `recurly_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `uuid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `schema_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `taggings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taggable_id` int(11) DEFAULT NULL,
  `taggable_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `encrypted_password` varchar(128) COLLATE utf8_unicode_ci DEFAULT '',
  `reset_password_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `role` tinyint(4) DEFAULT '1',
  `group_id` int(11) DEFAULT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `invitation_token` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invitation_sent_at` datetime DEFAULT NULL,
  `invitation_accepted_at` datetime DEFAULT NULL,
  `invitation_limit` int(11) DEFAULT NULL,
  `invited_by_id` int(11) DEFAULT NULL,
  `invited_by_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `accepted_terms` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_users_on_reset_password_token` (`reset_password_token`),
  KEY `index_users_on_approved` (`approved`),
  KEY `index_users_on_email` (`email`),
  KEY `index_users_on_invitation_token` (`invitation_token`),
  KEY `index_users_on_invited_by_id` (`invited_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO schema_migrations (version) VALUES ('20111115143756');

INSERT INTO schema_migrations (version) VALUES ('20111115154751');

INSERT INTO schema_migrations (version) VALUES ('20111115155210');

INSERT INTO schema_migrations (version) VALUES ('20111119171356');

INSERT INTO schema_migrations (version) VALUES ('20111119171503');

INSERT INTO schema_migrations (version) VALUES ('20111119171504');

INSERT INTO schema_migrations (version) VALUES ('20111120135331');

INSERT INTO schema_migrations (version) VALUES ('20111123164218');

INSERT INTO schema_migrations (version) VALUES ('20111129113422');

INSERT INTO schema_migrations (version) VALUES ('20111129120022');

INSERT INTO schema_migrations (version) VALUES ('20111217121748');

INSERT INTO schema_migrations (version) VALUES ('20111230112100');

INSERT INTO schema_migrations (version) VALUES ('20120209172105');

INSERT INTO schema_migrations (version) VALUES ('20120215163357');

INSERT INTO schema_migrations (version) VALUES ('20120215163849');

INSERT INTO schema_migrations (version) VALUES ('20120228195937');

INSERT INTO schema_migrations (version) VALUES ('20120309181948');

INSERT INTO schema_migrations (version) VALUES ('20120315170652');

INSERT INTO schema_migrations (version) VALUES ('20120315171825');

INSERT INTO schema_migrations (version) VALUES ('20120315174633');

INSERT INTO schema_migrations (version) VALUES ('20120320193053');

INSERT INTO schema_migrations (version) VALUES ('20120324161258');

INSERT INTO schema_migrations (version) VALUES ('20120324161725');

INSERT INTO schema_migrations (version) VALUES ('20120330144407');

INSERT INTO schema_migrations (version) VALUES ('20120331200944');

INSERT INTO schema_migrations (version) VALUES ('20120402200341');

INSERT INTO schema_migrations (version) VALUES ('20120407151639');

INSERT INTO schema_migrations (version) VALUES ('20120409223527');

INSERT INTO schema_migrations (version) VALUES ('20120410095046');

INSERT INTO schema_migrations (version) VALUES ('20120410114311');

INSERT INTO schema_migrations (version) VALUES ('20120410182109');

INSERT INTO schema_migrations (version) VALUES ('20120419093526');

INSERT INTO schema_migrations (version) VALUES ('20120421143501');

INSERT INTO schema_migrations (version) VALUES ('20120421161132');

INSERT INTO schema_migrations (version) VALUES ('20120425110836');

INSERT INTO schema_migrations (version) VALUES ('20120427130549');

INSERT INTO schema_migrations (version) VALUES ('20120504143823');

INSERT INTO schema_migrations (version) VALUES ('20120504144619');

INSERT INTO schema_migrations (version) VALUES ('20120504154937');

INSERT INTO schema_migrations (version) VALUES ('20120504155754');

INSERT INTO schema_migrations (version) VALUES ('20120509112520');

INSERT INTO schema_migrations (version) VALUES ('20120510192818');

INSERT INTO schema_migrations (version) VALUES ('20120529162040');

INSERT INTO schema_migrations (version) VALUES ('20120531091730');

INSERT INTO schema_migrations (version) VALUES ('20120625105601');

INSERT INTO schema_migrations (version) VALUES ('20120719133219');

INSERT INTO schema_migrations (version) VALUES ('20120803170705');

INSERT INTO schema_migrations (version) VALUES ('20121218114106');

INSERT INTO schema_migrations (version) VALUES ('20130226163037');

INSERT INTO schema_migrations (version) VALUES ('20130226164431');

INSERT INTO schema_migrations (version) VALUES ('20131220182506');

INSERT INTO schema_migrations (version) VALUES ('20131220183341');

INSERT INTO schema_migrations (version) VALUES ('20140113114302');

INSERT INTO schema_migrations (version) VALUES ('20140117121747');

INSERT INTO schema_migrations (version) VALUES ('20140207155602');

INSERT INTO schema_migrations (version) VALUES ('20140306160425');