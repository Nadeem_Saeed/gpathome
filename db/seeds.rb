# ADMIN_EMAIL = ENV.fetch('ADMIN_EMAIL').dup
ADMIN_EMAIL = "mn.saeed91@gmail.com"
ActionMailer::Base.default_url_options = { :host => 'localhost:3000',:protocol => 'http' }

##########
# Groups #
##########

# First we'll create some base Groups, one for admins, one for gps
Group.create(:group_type => 'admin')
Group.create(:group_type => 'gp')

#########
# Admin #
#########

@admin_group = Group.find_by_group_type('admin')

# Create the admin user
@admin = User.new(:email => ADMIN_EMAIL, :password => 'Password1', :role => 4)
@admin.build_profile(:first_name => 'Admin', :last_name => 'Admin')
@admin.profile.skip_profile_validation = true
@admin.approved = true
@admin.accepted_terms = true
@admin.group_id = @admin_group.id
@admin.save!

# Add the admin to the admin group
@admin_group.update_attributes :admin_id => User.first.id, :plan_id => 0

###########
# Credits #
###########

Credit.create :name => 'Online Consultation', :description => '', :price => 50.00

#########
# Plans #
#########

Plan.create :name => 'Individual', :description => '', :price => 0.00, :status => 1, :max_users => 1
Plan.create :name => 'Couple', :description => '', :price => 0.00, :status => 2, :max_users => 2
Plan.create :name => 'Family', :description => '', :price => 0.00, :status => 1, :max_users => 6

Plan.all.each do |plan|
  plan.plan_credits.build(credit: Credit.last, number_of_credits: 999)
  plan.save!
end

#########
# Pages #
#########

pages = [
  { slug: 'home', name: 'Home' },
  { slug: 'take_a_tour', name: 'Take a Tour' },
  { slug: 'our_service', name: 'Our Service' },
  { slug: 'faqs', name: 'FAQs' },
  { slug: 'typical_scenarios', name: 'Typical Scenarios' },
  { slug: 'about_us', name: 'About Us' },
  { slug: 'contact_us', name: 'Contact Us' },
  { slug: 'terms_and_conditions', name: 'Terms and Conditions' }
]

def create_page(page)
  body_data = ''
  File.open("./db/pages_seed/#{page[:slug]}.md", 'r') do |f|
    body_data = f.read
  end
  Page.create :name => page[:name], :visible => true, :slug => page[:slug], :body => body_data
end

pages.each { |page| create_page(page) }

