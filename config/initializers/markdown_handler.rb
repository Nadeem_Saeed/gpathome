module MarkdownHandler

  def self.erb
    @erb ||= ActionView::Template.registered_template_handler(:erb)
  end

  def self.call(template)
    compiled = erb.call(template)
    "Maruku.new(begin;#{compiled};end).to_html"
  end
end

ActionView::Template.register_template_handler :md, MarkdownHandler
