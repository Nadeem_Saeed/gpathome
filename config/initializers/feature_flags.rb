# Ability to set feature flags in app

POSSIBLE_FLAGS = %w(
  SHOW_PAYMENT_BUTTON
  SHOW_INSURANCE
  SHOW_ADDITIONAL
)

POSSIBLE_FLAGS.each do |ff|
  Object.const_set(ff, ENV.fetch(ff) { false })
end

# Payment button address
PAYMENT_BUTTON_ADDRESS = ENV.fetch('PAYMENT_BUTTON_ADDRESS') { '' }


membership_plans = ENV.fetch('SHOW_MEMBERSHIP_PLANS') { true }
SHOW_MEMBERSHIP_PLANS = membership_plans == 'false' ? false : true
