require File.expand_path('../boot', __FILE__)

require 'rails/all'

if defined?(Bundler)
  # If you precompile assets before deploying to production, use this line
  Bundler.require(*Rails.groups(:assets => %w(development test)))
  # If you want your assets lazily compiled in production, use this line
  # Bundler.require(:default, :assets, Rails.env)
end

#::APP_URL = ENV.fetch('APP_URL') { raise ArgumentError, 'Please provide APP_URL' }
#::APP_HOST = URI.parse(APP_URL).host
#::ADMIN_EMAIL = ENV.fetch('ADMIN_EMAIL') { raise ArgumentError, 'Please set ADMIN_EMAIL' }
#::PRACTICE_EMAIL = ENV.fetch('PRACTICE_EMAIL') { raise ArgumentError, 'Please set PRACTICE_EMAIL' }
::APP_URL = 'http://localhost:3000'
::APP_HOST = 'localhost'
::ADMIN_EMAIL = "mn.saeed91@gmail.com" #{ raise ArgumentError, 'Please set ADMIN_EMAIL' }
::PRACTICE_EMAIL = "nadeem42u@gmail.com" #{ raise ArgumentError, 'Please set PRACTICE_EMAIL' }

module Gpathome
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Custom directories with classes and modules you want to be autoloadable.
    config.autoload_paths += %W(#{config.root}/lib #{config.root}/app/presenters)
    #config.eager_load_paths += %W(#{config.root}/lib)
    #config.autoload_paths << Rails.root.join('lib')

    # Only load the plugins named here, in the order given (default is alphabetical).
    # :all can be used as a placeholder for all plugins not explicitly named.
    # config.plugins = [ :exception_notification, :ssl_requirement, :all ]

    # Activate observers that should always be running.
    # config.active_record.observers = :cacher, :garbage_collector, :forum_observer
    config.active_record.observers = :user_observer, :consultation_observer, :message_observer

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    #config.time_zone = 'London'
    config.time_zone = 'Kolkata'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Configure the default encoding used in templates for Ruby 1.9.
    config.encoding = "utf-8"

    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters += [:password, :subject, :message, :family_history, :medical_history, :drug_history, :policy_number]

    # Enable the asset pipeline
    config.assets.enabled = true

    # Version of your assets, change this if you want to expire all your assets
    config.assets.version = '1.1'

    config.middleware.use JQuery::FileUpload::Rails::Middleware
    #config.middleware.use ActionDispatch::Cookies
    #config.middleware.use ActionDispatch::Session::CookieStore
  end
end
