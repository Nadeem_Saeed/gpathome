Gpathome::Application.routes.draw do

  # ActiveAdmin routes
  ActiveAdmin.routes(self)

  # devise setup for users
  # redirect after sign up
  # see: http://stackoverflow.com/questions/3827011/devise-custom-routes-and-login-pages
  devise_for :users, :controllers => { :registrations => 'registrations', :invitations => 'invitations' },
  :path => '',
  :path_names => { :sign_in => 'login', :sign_out => 'logout', :sign_up => 'sign_up' }

  # scope for devise routes
  devise_scope :user do
    match 'payment' => 'registrations#payment'
    match 'confirm' => 'registrations#confirm'
    match 'accepted' => 'invitations#accepted'
  end

  # on successful login
  namespace :user do
    root :to => 'dashboard#index'
  end

  resource :user do
    # user credits (top up etc)
    resources :credits do
      member do
        post 'top_up'
      end
    end
    # user account - what plan they are on, payments, expiration dates etc
    resource :account do
      member do
        get 'billing_details'
      end
      resource :password
      resource :notification
      resource :group
    end
  end

  get '/user/docustore' => 'docustore#index', :as => :docustore

  # path to upgrade current plan
  post '/plan/switch' => 'plans#switch', :as => :switch_plan

  # dependent profiles
  resources :profiles
  resources :medical_profiles

  # add the put method to /user/profile/edit for the user profiles page under the dashboard
  put '/user/account/password/edit', :as => :edit_user_account_password, :to => 'passwords#update'

  # check for new messages - via ajax
  get '/check_for_messages' => 'messages#check_for_messages'

  # user feedback
  resource :feedback

  # add the put method to /user/profile/edit for the user profiles page under the dashboard, do the same for medical profiles
  put '/profiles/:id/edit', :as => :edit_profile, :to => 'profiles#update'
  put '/medical_profiles/:id/edit', :as => :edit_medical_profile, :to => 'medical_profiles#update'

  # devise for admins
  #devise_for :admin_users, ActiveAdmin::Devise.config

  # assets
  resources :assets

  slugs = %w(take_a_tour our_service terms_and_conditions faqs typical_scenarios contact_us about_us)
  slugs.each do |slug|
    get "#{slug}_raw" => "static##{slug}", as: slug
  end

  get ':slug', to: 'pages#show',
    constraints: ->(req){ slugs.include? req.params[:slug] },
    as: :pages

  # main user dashbaord
  get 'dashboard' => 'dashboard#index'

  # static routes
  if SHOW_MEMBERSHIP_PLANS
    get 'membership_plans' => 'static#membership_plans'
  end
  post 'request_invite' => 'static#request_invite'

  resources :consultations do
    resources :messages
  end

  # payments - recurly
  namespace :payments do
    get 'success'
  end

  # currently routing to static / index
  root :to => 'static#index'

end
