module DashboardHelper

  def can_create_consultation?
    true if current_user.remaining_online_consultations > 0
  end
  
end