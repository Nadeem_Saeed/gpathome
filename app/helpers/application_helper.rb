module ApplicationHelper

  # side wide title change
  def title(*parts)
    unless parts.empty?
      content_for :title do
        (parts << "GP at Home").join(" - ") unless parts.empty?
      end
    end
  end

  # allows adding a body class
  def body_class(class_name)
    unless class_name.nil?
      content_for :body_class do
        class_name + '-page'
      end
    end
  end

  def account_subpage?
    true if current_page? :controller => 'passwords', :action => 'edit' or
      current_page? :controller => 'notifications', :action => 'edit' or
      current_page? :controller => 'groups', :action => 'show'
  end

  # consultation status class
  def mark_status(consultation)
    'closed' if consultation.closed?
  end

  def current_or_upgrade(plan)
    if current_user
      if current_user.group.plan
        if current_user.group.plan.id == plan.id
          'Current Plan'
        elsif current_user.group.plan.max_users > plan.max_users
          # don't let people downgrade plans for now - may need to alter this at some point as it's only
          # check the max number of users allowed on the account
          # to me a plan has to have some kind of 'weight / value' added to it otherwise the system
          # can't tell that a Couple plan is a 'lower' plan than a Family plan
          ''
        else
          # must be a downgrade / upgrade
          link_to 'Upgrade', switch_plan_path(:plan => plan.plan_code), :class => 'btn btn-primary btn-large upgrade-plan'
        end
      end
    else
      # must not be signed in so send them to the main sign up page
      link_to 'Request Invite', new_user_registration_path(:plan => plan.id), :class => 'btn btn-primary btn-large'
    end
  end

  def markdown(content)
    Maruku.new(content).to_html.html_safe
  end
  alias :md :markdown

  def global_nav(slug)
    page = Page.find_by_slug(slug)
    if page.present? and page.visible
      content_tag :li, class: ('active' if params[:slug] == slug) do
        link_to page.name, pages_path(slug: page.slug)
      end
    end
  end

  def header_logo
    link = if user_signed_in?
              current_user.is_admin_or_gp? ? admin_dashboard_path : dashboard_path
            else
              '/'
            end

    content_tag :a, href: link do
      image_tag '/custom_assets/header-logo.png', alt: 'Logo'
    end
  end

end
