module ConsultationsHelper

  def from_who?(message)
  	if message.user.id === current_user.id
  	  'you'
  	else
  	  message.user.profile.first_name unless message.user.profile.nil?
  	end
  end
  
  def assigned_to_who(consultation)
    if consultation.assigned_to
      consultation.assigned_to.profile.first_name
    else
      'nobody yet'
    end
  end
  
  # return closed if consultation is closed
  def mark_status(consultation)
    if consultation.closed?
      'closed'
    elsif consultation.read_by_user?
      'unread'
    end
  end

  def view_or_reply_text(consultation)
    if consultation.status === 2
      'View'
    else
      'View / Reply'
    end
  end

  def render_side_images(message)
    if message.user.is_admin_or_gp?
      image_tag 'icons/gp-icon.png'
    else
      image_tag 'icons/person-icon.png'
    end
  end

  def message_time(message)
    if current_user.is_admin_or_gp?
      message.created_at.strftime('%B %d, %Y %H:%M')
    else
      "#{distance_of_time_in_words(Time.now, message.created_at)} ago"
    end
  end
  
end
