ActiveAdmin.register Feedback do

  # check users acccess to the page
  menu :label => 'Feedback', :if => proc { can?(:manage, Feedback) }
  controller.authorize_resource

  # remove the filter sidebar for now until we know what we want to filter!
  config.clear_sidebar_sections!

  # just show these actions
  actions :index, :show, :destroy

  index :page_title => 'asdasd' do
    column "User's Name" do |feedback|
      if feedback.user.nil?
        if feedback.email.blank?
          'Anonymous'
        else
          mail_to feedback.email, nil, :subject => 'GP at Home - Feedback'
        end
      else
        link_to feedback.user.profile.full_name, admin_user_path(feedback.user)
      end
    end
    column 'Feedback', :feedback
    column 'URL', :sortable => :url do |feedback|
      unless feedback.url.nil?
        link_to feedback.url, feedback.url
      end
    end
    column 'When was it logged?', :created_at
    default_actions
  end


end
