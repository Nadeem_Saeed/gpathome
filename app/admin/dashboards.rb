ActiveAdmin.register_page 'Dashboard' do

  menu :priority => 1, :label => proc{ I18n.t("active_admin.dashboard") }

  content :title => proc{ I18n.t("active_admin.dashboard") } do

  # TODO: Ability to add new MediPrompts in control panel (v2)

    # Consultations

    panel 'Consultations' do
      table_for Consultation.where('consultations.status != ?', 2).order('updated_at DESC').each do |consultation|
        column 'Status', :status do |consultation|
          case consultation.status
          when 0
            status_tag consultation.status?, :warning
          when 1
          	status_tag consultation.status?, :ok
          when 2
            status_tag consultation.status?
          end
        end
        column :patient do |consultation|
          consultation.user.profile.full_name
        end
        column :subject do |consultation|
          link_to consultation.subject, reply_admin_consultation_path(consultation)
        end
        column :updated_at
        column 'Assigned to', :assigned_to_id do |consultation|
            assigned_to_who(consultation)
        end
        column 'Actions' do |consultation|
          link_to 'Reply', reply_admin_consultation_path(consultation), :title => 'Click here to reply to consultation'
        end
      end
    end

    # New users

    panel 'Awaiting Approval' do
      form do |f|
        table_for User.awaiting_approval.each do |user|
          column 'Name' do |user|
            link_to(user.profile.full_name, admin_user_path(user), :title => 'View user profile') if user.profile
          end
          column 'Actions' do |user|
             link_to 'Approve', approve_admin_user_path(user), :title => 'Approve this user'
          end
        end
      end
    end

  end

end
