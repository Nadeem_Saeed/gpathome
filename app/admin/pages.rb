ActiveAdmin.register Page do

  menu :if => proc{ can?(:manage, Page) }, :priority => 10
  controller.authorize_resource

  actions :index, :edit, :update

  # remove the filter sidebar for now
  config.clear_sidebar_sections!

  member_action :enable do
    @page = Page.find(params[:id])
    @page.visible = true
    @page.save
    redirect_to :back, notice: 'Page enabled'
  end

  member_action :disable do
    @page = Page.find(params[:id])
    @page.visible = false
    @page.save
    redirect_to :back, notice: 'Page disabled'
  end

  form :partial => 'form'

  index do
    column 'Name' do |page|
      (if page.name =~ /Home/
        link_to page.name, "/"
      else
        link_to page.name, "/"+page.slug
      end)
    end
    column 'Visible' do |page|
      page.visible ? 'Yes' : 'No'
    end
    column 'Actions' do |page|
      (if page.visible
        link_to('Disable', disable_admin_page_path(page), class: 'member_link')
      else
        link_to('Enable', enable_admin_page_path(page), class: 'member_link')
      end)+
      link_to('Edit', edit_admin_page_path(page))
    end

  end

end
