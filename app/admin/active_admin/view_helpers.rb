module ActiveAdmin::ViewHelpers
  
  
  def assigned_to_who(consultation)
    unless consultation.assigned_to_id.nil?
      if consultation.assigned_to == current_user
        'you'
      else
        consultation.assigned_to.profile.first_name
      end
    end
  end

end