ActiveAdmin.register Plan do

  # check users acccess to the page
  menu :if => proc{ can?(:manage, Plan) }, :priority => 4
  controller.authorize_resource

  # remove the filter sidebar for now until we know what we want to filter!
  config.clear_sidebar_sections!

  # just show these actions
  actions :index, :edit, :update

  # create a new plan form
  # calls: /app/views/admin/plans/_form.html.haml
  form :partial => 'form'

  # setup main view page
  index do
    column :name
    column :description
    column :price do |plan|
      "&pound; #{number_to_currency(plan.price, :unit => '')}".html_safe
    end
    column 'Maximum users allowed', :max_users
    Credit.all.each do |credit_type|
      column credit_type.name.pluralize, :plan_credits do |plan|
      plan.plan_credits.find_by_credit_id(credit_type.id).number_of_credits if plan.plan_credits.find_by_credit_id(credit_type.id)
      end
    end
    column :status do |plan|
      plan.status?
    end
    default_actions
  end


end
