ActiveAdmin.register Consultation do

  # FIX: Links not working in reply. Seems to be to do with auto_link not working?
  require 'rails_autolink'

  # remove the filter sidebar for now
  config.clear_sidebar_sections!

  # check users acccess to the page
  menu :if => proc{ can?(:manage, Consultation) }, :priority => 2
  controller.authorize_resource

  actions :index, :show, :destroy, :create, :new

  # scopes for filtering
  scope :open, :order => :updated_at, :default => true do |consultation|
    consultation.where('consultations.status != ?', 2)
  end

  scope :un_assigned do |consultation|
    consultation.where(:assigned_to_id => nil)
  end

  scope :assigned_to_you do |consultation|
    consultation.where(:assigned_to_id => current_user.id)
  end

  scope :new do |consultation|
    consultation.where(:status => '0')
  end

  scope :in_progress do |consultation|
    consultation.where(:status => '1')
  end

  scope :closed do |consultation|
    consultation.where(:status => '2')
  end

  form partial: 'form'

  member_action :reply do
    @consultation = Consultation.find(params[:id])

    # mark this consultation as read (up to this date) for this user
    if current_user.is_admin_or_gp?
      @consultation.read_by_gp!
    else
      @consultation.read_by_user!
    end

    @old_consultations = @consultation.user.consultations.order('created_at DESC')
    @message           = @consultation.messages.build
    @patient           = @consultation.user
    @assets            = Asset.new
    @medical_profile   = MedicalProfilePresenter.new(@consultation.user)
    @page_title        = 'Reply to consultation'

  end

  member_action :close do
    @consultation = Consultation.find(params[:id])

    if @consultation.closed?
      # if the consultation is closed, re-open it
      @consultation.update_attribute(:status, 1)
      redirect_to :back, :notice => 'Consultation re-opened!'
    else
      # if the consultation is open, close it
      @consultation.update_attribute(:status, 2)
      redirect_to :back, :notice => 'Consultation closed!'
    end
  end

  # custom action to assign task to current GP/Admin that's logged in
  member_action :assign do
    @consultation = Consultation.find(params[:id])

    if @consultation.assigned_to.nil?
      # if the consultation is currently un-assigned, assign it to the current user
      #@consultation.update_attribute(:assigned_to, current_user.id)
      @consultation.update_attribute(:assigned_to_id, current_user.id)
      redirect_to :back, :notice => 'Assigned to you!'
    else
      # otherwise, un-assign it
      @consultation.update_attribute(:assigned_to_id, nil)
      redirect_to :back, :notice => 'Consultation un-assigned!'
    end
  end

  # index action
  index do
    column 'Status', :status do |consultation|
      case consultation.status
      when 0
        status_tag consultation.status?, :warning
      when 1
        status_tag consultation.status?, :ok
      when 2
        status_tag consultation.status?
      end
    end
    column 'New Messages?' do |consultation|
      if consultation.read_by_gp?
        'Yes'
      else
        'No'
      end
    end
    column :patient, :user do |consultation|
      link_to consultation.user.profile.full_name, admin_user_path(consultation.user)
    end
   column :subject do |consultation|
      link_to consultation.subject, reply_admin_consultation_path(consultation)
    end
    column 'Updated', :updated_at
    column 'Assigned to', :assigned_to_id do |consultation|
      unless consultation.assigned_to_id.nil?
        if consultation.assigned_to_id == current_user.id
          'you'
        else
          consultation.assigned_to.profile.first_name
        end
      end
    end
    column 'Actions' do |consultation|
      assigned_to_text = consultation.assigned_to_id.nil? ? 'Assign' : 'Un-assign'
      close_text = consultation.closed? ? 'Re-open' : 'Close'

      link_to(assigned_to_text, assign_admin_consultation_path(consultation), :class => 'member_link') +
      link_to('Reply', reply_admin_consultation_path(consultation), :class => 'member_link') +
      link_to(close_text, close_admin_consultation_path(consultation), :class => 'member_link') +
      (link_to('Delete', admin_consultation_path(consultation), :confirm => 'Are you sure?', :method => :delete) unless cannot? :destroy, consultation)
    end
  end

end
