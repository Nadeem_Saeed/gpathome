ActiveAdmin.register User do

  # check users acccess to the page
  menu :if => proc{ can?(:manage, User) }, :priority => 3
  controller.authorize_resource

  # remove the filter sidebar for now until we know what we want to filter!
  config.clear_sidebar_sections!

  config.sort_order = 'profiles.last_name_asc'

  # just show these actions
  actions :index, :show, :edit, :update, :new

  # invite user
  action_item :only => :index do
    link_to 'Invite User', invite_admin_users_path
  end

  scope :active, default: true do |users|
    users.where(active: true)
  end

  scope :all_users do |users|
    users
  end

  if ActiveRecord::Base.connection.tables.include?('plans')
    Plan.pluck(:name).each do |plan|
      scope "#{plan.downcase}_plan_users".to_sym do |user|
        ids = Plan.where(name: plan).first.groups.map(&:id)
        user.where(group_id: ids)
      end
    end
  end

  # override new action
  controller do

    def scoped_collection
      resource_class.includes(:profile)
    end

    def new
      @user = User.new
      @user.build_profile
    end

    def create     
      build_resource
      resource.accepted_terms = true
      resource.active = true
      resource.password = Devise.friendly_token

      if resource.save
        redirect_to admin_users_path, :notice => 'User was successfully created'
      else
        render :new, :alert => 'User not created'
      end
    end

  end

  # invite user
  collection_action :invite do
    @user = User.new
    @user.build_profile
  end

  collection_action :invite_user, method: :post do   
    build_resource
    plan_name   = params[:user][:plan]
    user_record = "Invites::#{plan_name}".constantize.invite(resource, Plan)
    if user_record.valid?
      redirect_to admin_users_path, notice: 'User was invited'
    else
      redirect_to :back, notice: 'There was a problem creating the invitation'
    end
  end

  # reply action
  # /admin/user/:id/approve
  member_action :approve do
    @user = User.find(params[:id])
    if @user.approved?
      @user.unapprove!
      redirect_to :back, :notice => 'User unapproved!'
    else
      @user.approve!
      redirect_to :back, :notice => 'User approved!'
    end
  end

  # action to credit a user a credit
  # /admin/user/:id/credit_user
  member_action :credit do
    @user = User.find(params[:id])
  end

  member_action :credit_user, method: :post do
    @user = User.find(params[:id])

    @user.account_credits.build params[:user][:account_credit]

    if @user.save
      redirect_to credit_admin_user_path(@user), :notice => 'Account credited!'
    else
      redirect_to credit_admin_user_path(@user), :notice => 'Sorry, we had a problem this end'
    end
  end

  # action to credit a user a credit
  # /admin/user/:id/decredit_user
  member_action :decredit do
    @account_credits = AccountCredit.find(params[:id])
    @account_credits.destroy
    redirect_to credit_admin_user_path(@account_credits.user), :notice => 'Credit removed'
  end

  # action view / add payments
  # /admin/user/:id/payments
  member_action :payments do
    @user = User.find(params[:id])
  end

  member_action :add_payments, method: :post do
    @user = User.find(params[:id])
    if @user.update_attributes(params[:user])
      redirect_to payments_admin_user_path(@user),
        notice: 'Added payment to account'
    else
      flash[:error] = 'Sorry, there was a problem adding payment (did you enter a decimal number in the amount box?)'
      redirect_to payments_admin_user_path(@user)
    end
  end

  member_action :delete_payment do
    @user = User.find(params[:id])
    @payment = @user.card_payments.where(id: params[:payment_id])
    if @payment.first.destroy
      redirect_to payments_admin_user_path(@user),
        notice: 'Payment deleted'
    else
      redirect_to payments_admin_user_path(@user),
        error: 'Payment not deleted'
    end
  end

  # action to activate/deactivate a user
  # /admin/user/:id/activate
  member_action :activate do
    @user = User.find(params[:id])
    if @user.active?
      @user.active = false
      @user.save
      redirect_to :back, notice: 'User deactivated'
    else
      @user.active = true
      @user.save
      redirect_to :back, notice: 'User activated'
    end
  end

  show do
    h2 "Profile for #{user.profile.first_name}"

    div :class => 'user-profile' do

      attributes_table_for user do
        row :email
        row 'Sign up date' do |u|
          u.created_at
        end
        row 'User role' do |u|
          u.role?.humanize
        end
        row 'User\'s Plan' do |u|
          u.group.plan.name if u.group.plan
        end
      end

    end

    div :class => 'user-profile' do

      h3 'User profile'

      attributes_table_for user.profile do
        row :first_name
        row :last_name
        row :date_of_birth
        row 'Gender' do |p|
          p.gender_humanize
        end
        row 'Phone number' do |p|
          p.phone_1
        end
        row 'Address 1' do |a|
          a.street_address
        end
        row 'Address 2' do |a|
          a.extended_address
        end
        row 'City' do |a|
          a.locality
        end
        row 'County' do |a|
          a.region
        end
        row 'Post code' do |a|
          a.postal_code
        end
      end

    end

    unless user.is_admin_or_gp?

      div :class => 'user-profile' do

        h3 'Medical profile'

        attributes_table_for MedicalProfilePresenter.new(user) do
          row :medical_history
          row :family_history
          row :regular_medication
          row :allergies
          row :smoking
          row 'Alcohol units (per week)' do |mp|
            mp.alcohol_units
          end
          row :exercise
          row :height
          row :weight
        end

        if SHOW_INSURANCE
          div :class => 'medical-insurance' do
            h3 'Medical Insurance'
            attributes_table_for MedicalProfilePresenter.new(user) do
              row :insurance_company
              row :policy_number
              row :london_insurance
            end
          end
        end

      end

    end

    div :class => 'actions buttons approve' do
      approved_text = User.find(params[:id]).approved? ? 'Unapprove' : 'Approve'
      (link_to approved_text, approve_admin_user_path(params[:id]), :class => 'commit button') +
      (link_to 'Cancel', admin_plans_path)
    end

  end

  form do |f|

    if f.object.is_admin_or_gp? || f.object.new_record?
      f.inputs 'Base details' do
        f.input :email
        f.input :role, collection: User.top_level_roles.invert.map { |r| [ r[0].titleize, r[1] ] }, as: :select, include_blank: false
      end
    else
      f.inputs 'Base details' do
        f.input :email
      end
    end

    f.inputs 'Profile', :for => :profile do |p|
      p.input :first_name
      p.input :last_name
      p.input :date_of_birth, :start_year => Time.now.year, :end_year => Time.now.year - 100, :order => [:day, :month, :year]
      p.input :gender, :as => :select, :collection => { 'Male' => true, 'Female' => false }
      p.input :phone_1, :as => :string, :label => 'Phone number', input_html: { value: p.object.phone_1 }
    end

    f.inputs 'Address', :for => :profile do |p|
      p.input :street_address, :label => 'Address 1'
      p.input :extended_address, :label => 'Address 2'
      p.input :locality, :label => 'City'
      p.input :region, :label => 'County'
      p.input :postal_code, :label => 'Post code'
    end

    if f.object.medical_profile
      f.inputs 'Medical Profile', :for => :medical_profile, :class => 'inputs medical-profile-attributes' do |mp|
        mp.input :medical_history, :input_html => { rows: 10 }
        mp.input :family_history, :input_html => { rows: 10 }
        mp.input :regular_medication, :input_html => { rows: 5 }
        mp.input :allergies, :input_html => { rows: 5 }
        mp.input :smoking, :as => :select
        mp.input :alcohol_units, :label => 'Alcohol units (per week)', :as => :select, :collection => (1..50).to_a
        mp.input :exercise, :input_html => { rows: 5 }
        mp.input :height, :label => 'Height (cm)', :as => :select, :collection => (30..210).to_a
        mp.input :weight, :label => 'Weight (kg)', :as => :select, :collection => (1..150).to_a
        mp.input :read_and_understood, :label => 'Read and understood medical profile'
      end

      if SHOW_INSURANCE
        f.inputs 'Insurance Details', :for => :medical_profile, :class => 'inputs insurance-details' do |mp|
          mp.input :insurance_company
          mp.input :policy_number
          mp.input :london_insurance, :as => :select
        end
      end
    end

    f.actions

  end

  index do
    column 'First name', sortable: 'profiles.first_name' do |user|
      user.profile.first_name
    end
    column 'Last name', sortable: 'profiles.last_name' do |user|
      user.profile.last_name
    end
    column 'Email', :email
    column 'Sign up date', :created_at
    column 'Role' do |user|
      user.role?.titleize
    end
    column do |user|
      activated_text = user.active? ? 'Deactivate' : 'Activate'
      link_to(activated_text, activate_admin_user_path(user), :class => 'member_link') +
      link_to('View', admin_user_path(user), :class => 'member_link') +
      link_to('Edit', edit_admin_user_path(user), :class => 'member_link') +
      link_to('Credit', credit_admin_user_path(user), :class => 'member_link') +
      (link_to('Payments', payments_admin_user_path(user), :class => 'member-link') if user.is_account_holder?)
    end
  end

end
