# Asset management

do (window) ->

  class window.Uploader

    $container: null
    $attachments: null
    $attachmentArray: null

    processOptions: [
        {
          action: 'load'
          fileTypes: /^image\/(gif|jpeg|png)$/
          maxFileSize: 20000000
        },
        {
          action: 'resize'
          maxWidth: 600
          maxHeight: 600
          minWidth: 600
          minHeight: 600
        },
        {
          action: 'save'
        }
      ]

    onAdd: (e, data) ->
      types = /(\.|\/)(gif|jpe?g|png|pdf|doc|docx)$/i
      file = data.files[0]
      if types.test(file.type) || types.test(file.name)
        data.context = $(tmpl('template-upload', file))
        @$container.find('.attachments').before(data.context)
        @$container.fileupload('process', data).done ->
          data.submit()
      else
        alert("#{file.name} is not an image or document file")

    onProgress: (e, data) ->
      if data.context
        progress = parseInt(data.loaded / data.total * 100, 10)
        data.context.find('.bar').css('width', progress + '%')

    onDone: (e, data) ->

      $('.upload').fadeOut('fast')

      $('#new_asset .attachments')
        .append("#{data.result.template}")
        .hide()
        .fadeIn('slow')

      # if this is inside the admin interface don't try and update the attachments array
      unless @$container.parents('.attributes_table').length
        @$attachments.trigger 'update_attached_assets'


jQuery ->

  # enlarging the image for users
  $('.attachments').on 'click', '.enlarge-attachment', (e) ->
    e.preventDefault()
    enlarged_image = $(this).attr('href')
    $enlarge = $(this).parents('.message:eq(0)').find('.enlarged')
    $enlarge.html "<img src='#{enlarged_image}' title='Click to close' />"
    $enlarge.show()

  # close enlarged image
  $('.enlarged').on 'click', (e) ->
    e.preventDefault()
    $(@).hide()


  #
  # Admins
  #

  if $('body').hasClass('active_admin') || $('body').hasClass('consultations')

    # append a modal to handle enlarged images
    $('body').append """
      <div class="modal-enlarged-image modal hide in">
        <div class="modal-body"></div>
        <div class="modal-footer">
          <a class="btn" href="#close-modal">Close</a>
        </div>
      </div>
    """
    $modal = $('body').find('.modal-enlarged-image')


    $('.attributes_table .attachments, .gp-attachments').on 'click', '.image-attachment', (e) ->
      e.preventDefault()
      $img = $(this).parents('.attachment').find('img')
      if $img.length
        src = $(this).attr('href')
        $modal.find('.modal-body').html "<img src=\"#{src}\" />"
        $modal.modal('show')


    $('.modal-footer').on 'click', 'a', (e) ->
      e.preventDefault()
      $('.modal-enlarged-image').modal('hide')


  # modal window for tags

  if $('.tags_modal').length

    $('.modal-footer').find('a').on 'click', (e) ->
      e.preventDefault()
      $('.tags_modal').modal('hide')


  # jQuery upload

  if $('#new_message').length || $('#new_consultation').length || $('.dropzone').length

    uploader = new Uploader()
    uploader.$container = $('#new_asset')
    uploader.$attachments = uploader.$container.find('.attachments')

    if $('#message_attachments').length
      uploader.$attachmentArray = $('#message_attachments')
    else
      uploader.$attachmentArray = $('#consultation_messages_attributes_0_attachments')


    # setup a new event listening for the update_array call for assets
    uploader.$attachments.on 'update_attached_assets', (e) ->
      arr = []
      uploader.$attachments.find('.attachment').each ->
        arr.push($(this).data('id'))
      uploader.$attachmentArray.val(arr.join(','))


    # setup jQuery uploader
    uploader.$container.fileupload
      formData:
        attachable_id: uploader.$container.find('#asset_attachable_id').val()
        attachable_type: uploader.$container.find('#asset_attachable_type').val()
      dataType: 'json'
      process: uploader.processOptions
      add: (e, data) ->
        uploader.onAdd(e, data)
      progress: (e, data) ->
        uploader.onProgress(e, data)
      done: (e, data) ->
        uploader.onDone(e, data)
