jQuery ->

  unless $('body').hasClass 'active_admin'

    $('.upgrade-plan').on 'click', (e) ->
      e.preventDefault()
      plan = $(this).parents('.plan:eq(0)').data('plan-name')
      setTimeout (->
        $('#upgrade_to').val(plan)
        return
      ), 200      
      $('#upgrade_plan').modal()
