jQuery ->

  $('select#user_profile').change ->
    document.location = '/profiles/' + $(@).val() + '/edit'

  # if the user has the same address hide the extra form fields for address
  if $('#user_has_same_address').length

    $('#user_has_same_address').change ->
      if $('#user_has_same_address').is(':checked')
        $('.address-details').hide()
      else
        $('.address-details').show()