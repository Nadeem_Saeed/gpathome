# All JS for adding a new dependent user
jQuery ->

  if $('#new_user').length > 0
    $new_user = $('#new_user')
    $('#new_user .controls input:radio').change () ->
      if $('#user_role_6').attr('checked')
        $new_user.find('input[type="submit"]').val('Add Dependent')
        $new_user.find('#user_email').parents('.control-group').hide()
      else if !$('#user_role_6').attr('checked')
        $new_user.find('input[type="submit"]').val('Send Invite')
        $new_user.find('#user_email').parents('.control-group').show()