
jQuery ->

  unless $('html').hasClass 'ie7'

    # append the modal page
    $('.container-fluid').append '<div class="feedback_modal modal hide fade in feedback-modal" />'
    $('.feedback_modal').load('/feedback/new')

    # append a feedback button on the page
    #$('header .actions').append '<a href="#send_feedback" class="feedback">Site Feedback</a>'

    # when we click the feedback button launch our feedback modal
    $('.feedback').on 'click', (e) ->
      e.preventDefault()

      # find 'this' and launch our modal
      $('.feedback-modal').modal('show')

    $('.feedback-modal .modal-footer').find('a').on 'click', (e) ->
      e.preventDefault()
      $('.feedback-modal').modal('hide')
