jQuery ->

  if $('#new_message').length

    # Ajax poler - to check for new messages on the server

    messages = []

    $first_message        = $('.message').filter('[data-message_id]').first()
    consultation_id       = $first_message.data('consultation_id')
    since_id              = $first_message.data('message_id')
    $new_message_waiting  = $('.new-message-waiting')
    
    # this is a hack, needs removing
    $new_message_waiting.css('display', 'block').hide()

    $new_message_waiting.on 'click', (e) ->
      e.preventDefault()
      html = ''
      for message in messages
        html += message.template

      # finally insert the new messages
      $new_message_waiting.parent().after(html)

      $new_message_waiting.fadeOut('slow')


    check_for_messages = ->

      # get consultation id and message id
      since_id = $('.message').filter('[data-message_id]').first().data('message_id')

      $.getJSON '/check_for_messages.json?since_id='+since_id+'&consultation_id='+consultation_id, (json) ->
        # if there are some
        if json.length > 0
          messages = json
          # then show an alert box showing that some exist
          $new_message_waiting.html 'There are new messages waiting... click here to view them.'
          $new_message_waiting.fadeIn('slow')


    # set the interval of the ajax request
    setInterval check_for_messages, 10000