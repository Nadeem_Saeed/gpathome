
window.onload = ->

  return unless $('#wmd-input').length

  converter1 = Markdown.getSanitizingConverter()
  converter1.hooks.chain "preBlockGamut", (text, rbg) ->
    text.replace /^ {0,3}""" *\n((?:.*?\n)+?) {0,3}""" *$/g, (whole, inner) ->
      "<blockquote>" + rbg(inner) + "</blockquote>\n"


  editor1 = new Markdown.Editor(converter1)
  editor1.run()

  help = ->
    alert "Do you need help?"
    return

  options =
    helpButton:
      handler: help

    strings:
      quoteexample: "whatever you're quoting, put it right here"
