jQuery ->

  if $('body').hasClass('membership-plans-page')

    $('.plan a').on 'click', (e) ->
      return if $(e.target).hasClass('upgrade-plan')
      e.preventDefault()
      plan = $(this).parents('.plan:eq(0)').data('plan-name')
      $modal = $('.request-invite-modal')
      $modal.find('.plan-name').text plan
      setTimeout (->
        $modal.find('input[type="hidden"]').val plan
        return
      ), 200
      $modal.modal()

    $('.request-invite-modal .modal-footer').find('a').on 'click', (e) ->
      e.preventDefault()
      $('.request-invite-modal').modal()

    $('.request-invite-modal form').on 'ajax:success', (e, json) ->
      statusText = json.text
      $modalBody = $(this).parents('.modal-body:eq(0)')
      $modalBody.find('.flash').html """
        <p>#{statusText}</p>
      """
      $modalBody.addClass('hide-form-etc')
      $modalBody.append """
        <p><a href="" data-dismiss="modal">Close</a></p>
      """

    $('.request-invite-modal form').on 'ajax:error', (e, jqXHR) ->
      statusText = $.parseJSON(jqXHR.responseText).text
      $(this).parents('.modal-body:eq(0)').find('.flash').html """
        <p>#{statusText}</p>
      """
