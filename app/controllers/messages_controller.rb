class MessagesController < ApplicationController

  load_and_authorize_resource :except => [:create]

  before_filter :find_consultation

  def create

    # SEE: http://stackoverflow.com/questions/7015301/getting-cancans-load-and-authorize-resource-working-within-a-custom-create-acti
    authorize! :create, Message

    @message = @consultation.messages.new(params[:message].merge!(:user => current_user))

    if @message.save
      flash[:success] = 'Message has been sent.'
    else
      flash[:alert] = 'Message was not sent.'
    end

    # we re-route people here depending on whether they are a gp / admin or a regular user
    if current_user.is_admin_or_gp?
      set_consultation_state
      redirect_to reply_admin_consultation_path(@consultation)
    else
      redirect_to @consultation
    end

  end

  def check_for_messages
    messages = build_json_for_response
    @consultation.read_by_user! if messages.any?
    render :json => messages
  end

  private

    def find_consultation
      @consultation = Consultation.find(params[:consultation_id])
    end

    def set_consultation_state
      # if the status isn't set to in-progress (1) then set it now
      @consultation.update_attribute(:status, 1) if @consultation.status.zero?
      @consultation.update_attribute(:assigned_to_id, current_user.id) if @consultation.assigned_to_id.nil?
    end

    # TODO: Make sure this is bullet proof! MUST only be able to access ones own consultations
    def build_json_for_response
      messages = Message.where("id > ? AND consultation_id = ?", params[:since_id], params[:consultation_id]).reverse

      # map the messages to something we want
      messages.map! do |message|
        {
          :template => render_to_string( '_message', layout: false, formats: [:html], locals: { message: message, admin_view: false } )
        }
      end
    end

end
