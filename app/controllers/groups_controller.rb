class GroupsController < ApplicationController

  # check whoever it is has access to groups
  load_and_authorize_resource

  before_filter :find_group

  def show
  end

  private

    def find_group
      @group = current_user.group
    end

end
