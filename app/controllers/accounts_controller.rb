class AccountsController < ApplicationController

  # automatically calls authorize!(:action, :account)
  # SEE: https://github.com/ryanb/cancan/wiki/Non-RESTful-Controllers
  authorize_resource :class => false

  before_filter :find_user

  def billing_details

    # get current billing details
    account       = Recurly::Account.find(@user.id)
    billing_info  = account.billing_info

    # get users profile details
    plan_code = @user.group.plan.plan_code

    @payment = {
      signature:    Recurly.js.sign(:subscription => { :plan_code => plan_code }),
      plan_code:    plan_code,
      account_code: @user.id,
      first_name:   billing_info.first_name,
      last_name:    billing_info.last_name,
      email:        billing_info.account.email,
      phone:        billing_info.phone,
      address1:     billing_info.address1,
      address2:     billing_info.address2,
      city:         billing_info.city,
      zip:          billing_info.zip,
      state:        billing_info.state,
      country:      billing_info.country
    }

  end

end
