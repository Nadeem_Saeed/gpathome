class PaymentsController < ApplicationController

  # SEE: http://stackoverflow.com/questions/5669322/turn-off-csrf-token-in-rails-3
  protect_from_forgery :except => :success

  # this was called on a post but has been reused to handle the Recurly hosted payment pages
  # it now responds to GET request with the following params
  # https://www.gpathome.com/payments/success?account={{account_code}}&plan={{plan_code}}
  def success

    # get the result of the Recurly payment
    #result = Recurly.js.fetch params[:recurly_token]

    # find the result
    account = Recurly::Account.find(params[:account_code].to_i)
    result  = account.transactions.first

    # find our user using the account code posted back from Recurly
    #user = User.find(result.account.account_code.to_i)

    # we'll cast the GET param to an int here to be sure
    user = User.find(params[:account_code].to_i)

    # build a new transaction record
    user.recurly_payments.build(
      :uuid   => result.uuid,
      :state  => result.status
    )

    # save record to db
    user.save

    if current_user
      # if the user is signed in (they're approved at this point) then redirect them straight to their dashboard
      redirect_to dashboard_path
    else
      # render the success page
      render 'registrations/confirm'
    end

  end

end
