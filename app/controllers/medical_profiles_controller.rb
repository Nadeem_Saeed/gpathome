class MedicalProfilesController < ApplicationController
	
  load_and_authorize_resource

  before_filter :find_medical_profile

  def update
    if @medical_profile.update_attributes(params[:medical_profile])
      flash[:success] = 'Medical profile updated.'
      redirect_to :action => :edit
    else
      flash.now[:alert] = 'Medical profile not updated.'
      render :edit
    end
  end
	
	private
		
    def find_medical_profile
      @medical_profile = MedicalProfile.find(params[:id])
    end

end