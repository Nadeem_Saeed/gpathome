class InvitationsController < Devise::InvitationsController

  authorize_resource :class => false

  def new
    @user = User.new
    @profile = @user.build_profile :skip_auth => true
  end

  def create    
    # if the user has the same address, copy it over
    if params[:user_has_same_address] == 'yes'
      params[:user][:profile_attributes].merge! current_user.profile.full_address
    end

    # here's where we have to split the logic
    # if it's an authorised user or responsible user then we continue as usual
    if params[:user][:role] == '6'
      build_resource
      if resource.save
        flash[:success] = 'New dependent user created'
        redirect_to new_user_invitation_path
      else
        clean_up_passwords resource
        respond_with resource
      end
    else
      self.resource = resource_class.invite!(params[resource_name], current_inviter)

      if resource.errors.empty?
        set_flash_message :success, :send_instructions, :email => self.resource.email
        respond_with resource, :location => new_user_invitation_path
      else
        respond_with_navigational(resource) { render :new }
      end

    end
  end

  def update
    self.resource = resource_class.accept_invitation!(params[resource_name])
    if resource.errors.empty?
      if resource.is_account_holder?
        sign_in resource
        redirect_to dashboard_path
      else
        redirect_to accepted_path
      end
    else
      respond_with_navigational(resource){ render :edit }
    end
  end

  def accepted
  end

end
