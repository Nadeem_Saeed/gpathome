class ApplicationController < ActionController::Base

  protect_from_forgery

  # global after sign in, sorts out all the redirections
  # see: https://github.com/plataformatec/devise/wiki/How-To:-Redirect-to-a-specific-page-on-successful-sign-in
  def after_sign_in_path_for(resource)
    if current_user.is_admin_or_gp?
      admin_dashboard_path
    else
      dashboard_path
    end
  end

  # CanCan setup
  # see: https://github.com/gregbell/active_admin/wiki/How-to-work-with-cancan
  rescue_from CanCan::AccessDenied do |exception|
    # if someone tries to access something they shouldn't we need to whip them away to the sign in page
    redirect_to new_user_session_path, :alert => exception.message
  end

  def current_ability
    @current_ability ||= Ability.new(current_user)
  end

  # to check for admin, this is called in the admin dashboard
  # this is to do with ActiveAdmin - see /config/initializers/active_admin.rb
  def is_admin?
    unless current_user.is_admin_or_gp?
      redirect_to dashboard_path
    end
  end

  # TODO: Move this into it's own presenter logic
  def consultations_for_user
    if current_user.show_dependent_consultations?
      Consultation.find_all_by_user_id(User.dependents_and_self(current_user).pluck(:id))
    else
      Consultation.find_all_by_user_id(current_user)
    end
  end
  helper_method :consultations_for_user

  private

    # global method to find users
    def find_user
      @user = current_user
    end

end
