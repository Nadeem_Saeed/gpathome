class AssetsController < ActionController::Base

  before_filter :find_asset, except: [:create]

  def create
    if params[:attachable_id] && params[:attachable_type]
      params[:asset].merge!({
        attachable_id: params[:attachable_id],
        attachable_type: params[:attachable_type]
      })
    end
    @asset = Asset.create(params[:asset])
    render json: { status: 200, template: asset_template }
  end

  def edit
  end

  def update
    if @asset.update_attributes(params[:asset])
      flash[:success] = 'Tags updated successfully. Please close the window.'
    else
      flash[:error] = 'There was a problem updating the attachment'
    end
    @asset
  end

  def destroy
    @asset.destroy if destroyable?
  end

  private

  def find_asset
    @asset = Asset.find(params[:id])
  end

  def asset_template
    render_to_string '_asset', locals: { asset: @asset }
  end

  def destroyable?
    is_admin_attachment? || is_user_attachment?
  end

  def is_admin_attachment?
    @asset.attachable_type == 'Consultation' && current_user.is_admin_or_gp?
  end

  def is_user_attachment?
    @asset.attachable_type == 'Message' && !current_user.is_admin_or_gp?
  end
end
