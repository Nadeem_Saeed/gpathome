class DocustoreController < ApplicationController

  def index
    @docustore = current_user.docustore || Docustore.create(user_id: current_user.id)
    @assets = @docustore.assets.build
  end

end
