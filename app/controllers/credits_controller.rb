class CreditsController < ApplicationController

  load_and_authorize_resource :except => [:top_up]

  # turn off CSRF token for payments
  protect_from_forgery :except => :top_up

  before_filter :find_user
  before_filter :find_credits

  def index

    profile = @user.profile

    # create a new payment hash
    @payment = {
      signature:  Recurly.js.sign(
        :account => {
          :account_code => @user.id
        },
        :transaction => {
          :amount_in_cents => 5000,
          :currency => 'GBP'
        }
      ),
      first_name: profile.first_name,
      last_name:  profile.last_name,
      email:      @user.email,
      address1:   profile.street_address,
      address2:   profile.extended_address,
      city:       profile.locality,
      zip:        profile.postal_code,
      state:      profile.region
    }

  end

  def top_up

    authorize! :top_up, Credit

    # Currently we only have the one credit type. This can expand in the future.

    credit    = @credits.first
    credit_id = credit.id
    result    = Recurly.js.fetch params[:recurly_token] # get Recurly response

    # build the new credit record
    @credit = current_user.group.account_credits.build(
      :credit_id => credit_id,
      :quantity => 1
    )

    # build a new transaction record
    @payment = current_user.recurly_payments.build(
      :uuid   => result.uuid,
      :state  => result.state
    )

    if @credit.save && @payment.save
      # if it saved on then send out a message
      flash[:success] = "Topped up by 1 #{credit.name} credit!"
    else
      flash[:alert] = 'Sorry, there was a problem, please contact us at help@gpathome.com'
    end

    # send them back to the credits page
    redirect_to user_credits_path

  end

    private

      def find_user
        @user = current_user
      end

      def find_credits
        @credits = Credit.all
      end

end
