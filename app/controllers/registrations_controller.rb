class RegistrationsController < Devise::RegistrationsController
require "byebug"
  # User signups

  #before_filter :find_plan, :except => [:confirm]

  def new
    @user = User.new
    @profile = @user.build_profile
  end

  def create
    build_resource
    resource.role = 2 # set default role to Account Holder on signup
    if resource.save
      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_navigational_format?
        sign_up(resource_name, resource)
        respond_with resource, :location => after_sign_up_path_for(resource)
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_navigational_format?
        expire_session_data_after_sign_in!
        respond_with resource, :location => after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      respond_with resource
    end
  end

  # Old action for Recurly JS - users are currently routed through Recurly's hosted
  # payment pages - see below
  def payment

    # create new signature
    # need to get the new users details along with what plan they are on
    user      = User.find(params[:user])
    profile   = user.profile
    plan      = Plan.find(params[:plan])
    plan_code = plan.plan_code

    # OPTIMIZE: Add some sanitization for plans / users in here, make sure
    # people are not playing with us

    @payment = {
      signature:    Recurly.js.sign(:subscription => {
          :plan_code => plan_code
        }, :account => { :account_code => user.id }
      ),
      plan_code:    plan_code,
      first_name:   profile.first_name,
      last_name:    profile.last_name,
      email:        user.email,
      phone:        profile.phone_1,
      address1:     profile.street_address,
      address2:     profile.extended_address,
      city:         profile.locality,
      zip:          profile.postal_code,
      state:        profile.region
    }

  end

  private

    # SEE: https://github.com/plataformatec/devise/wiki/How-To:-Redirect-to-a-specific-page-on-successful-sign-up-(registration)

    # Currently this is re-routed to the hosted payment pages on Recurly. Once Recurly JS
    # can handle 3D secure payment we can reroute this back through our site.

    # Start of Recurly JS code
#    def after_sign_up_path_for(resource)
#      payment_path(:plan => @plan_id, :user => resource.id)
#    end
#
#    def after_inactive_sign_up_path_for(resource)
#      payment_path(:plan => @plan_id, :user => resource.id)
#    end
    # End of Recurly JS code

    # Amended to re-route through hosted payment page at Recurly
    def after_sign_up_path_for(resource)
      # find our user and plan details
      user      = User.find(resource.id)
      plan      = Plan.find(params[:user][:plan])
      plan_code = plan.plan_code
      # return complete Recurly hosted payment address
      # note there's no need to call redirect_to here
      "https://gpathome.recurly.com/subscribe/#{plan_code}/#{resource.id}?first_name=#{user.profile.first_name}&last_name=#{user.profile.last_name}&email=#{user.email}"
    end

    def after_inactive_sign_up_path_for(resource)
      # call same method as above
      after_sign_up_path_for(resource)
    end

    def find_plan
      # The following is for the view
      # find the plan they are on
      @plan_id = params[:plan] || params[:user][:plan]
      @plan = Plan.find(@plan_id)
    end

end
