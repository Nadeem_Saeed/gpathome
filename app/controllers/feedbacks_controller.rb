class FeedbacksController < ApplicationController

  # layout will be ajax which has not wrap around layouts
  layout 'ajax'

  def new
    @feedback = Feedback.new
    respond_to :js # only respond to Ajax requests
  end

  def create

    if current_user
      @feedback = current_user.feedbacks.build(params[:feedback])
    else
      @feedback = Feedback.new(params[:feedback])
    end
  
    respond_to do |format|
      if @feedback.save
        @message = 'Thank you, we will get back to you soon!'
        format.js
      else
        @message = 'Sorry, there was a problem sending the form.'
        format.js
      end
    end
  
  end

end