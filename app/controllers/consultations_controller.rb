class ConsultationsController < ApplicationController

  load_and_authorize_resource

  def index
    @consultations = Consultation.all
  end

  def new
    @consultation = Consultation.new
    @message = @consultation.messages.build
    @assets = Asset.new
  end

  def create
    @user = User.find(params[:consultation][:user_id])
    attribute_messages_to_consultation
    @consultation = Consultation.new(params[:consultation].merge!(:read_by_user => Time.now))

    if @consultation.save && update_all_attachments
      flash[:success] = 'Consultation Created.'
      redirect_to @consultation
    else
      flash.now[:alert] = 'Consultation has not been created.'
      render :new
    end
  end

  def show

    @consultation = Consultation.find(params[:id])

    # mark this consultation as read (up to this date) for this user
    if current_user.is_gp?
      @consultation.read_by_gp!
    else
      @consultation.read_by_user!
    end

    @message = @consultation.messages.build
    @assets = Asset.new

  end

  private

  def update_all_attachments
    Asset.where(id: attachments, attachable_type: nil).update_all(attachable_type: 'Message', attachable_id: message_id)
  end

  def message_id
    @consultation.messages.first.id
  end

  def attachments
    params[:consultation][:messages_attributes].map do |key, message|
      message[:attachments].split(',').map(&:to_i)
    end.flatten!
  end

  def attribute_messages_to_consultation
    # attributing the user to the new message under the new consultation
    # see: http://stackoverflow.com/questions/2721732/problems-with-build-params-for-accepts-nested-attributes-for
    params[:consultation][:messages_attributes].each do |key, message|
      message[:user_id] = params[:consultation][:user_id]
    end if params[:consultation][:messages_attributes]
  end

end
