class ProfilesController < ApplicationController

  load_and_authorize_resource

  before_filter :find_user
  
  def update
    @profile = @user.profile
    if @user.update_attributes(params[:user]) && @profile.update_attributes(params[:user][:profile_attributes])
      flash[:success] = 'Profile updated.'
      redirect_to :action => :edit
    else
      flash.now[:alert] = 'Profile not updated.'
      render :edit
    end
  end
  
  private
  
    def find_user
      @profile = Profile.find(params[:id])
      @user = @profile.user
    end

end
