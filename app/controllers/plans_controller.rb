class PlansController < ApplicationController

  authorize_resource :class => false

  def switch
    plan = Plan.find_by_name(params[:upgrade_to])
    current_user.group.plan_id = plan.id

    if current_user.group.save
      flash[:success] = "Plan upgraded to the #{plan.name} plan."
    else
      flash[:error] = 'Sorry, there seems to have been a problem upgrading your plan.'
    end

    redirect_to user_account_path

  end

end
