class PasswordsController < ApplicationController

  # automatically calls authorize!(:action, :password)
  # see : https://github.com/ryanb/cancan/wiki/Non-RESTful-Controllers
  authorize_resource :class => false

  before_filter :find_user

  def update

    if @user.update_with_password(params[:user])
      # Sign in the user by passing validation in case his password changed
      sign_in @user, :bypass => true
      flash[:success] = 'Password changed.'
      redirect_to :action => :edit
    else
      flash.now[:alert] = 'Password not changed.'
      render :edit
    end
  end

  private

    def find_user
      @user = current_user
    end

end
