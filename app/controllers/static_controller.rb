class StaticController < ApplicationController

  skip_authorization_check

  before_filter :check_if_user, :except => :membership_plans
  before_filter :find_user, :only => :membership_plans

  def index
    @page = Page.find_by_slug('home')
  end

  def faqs
    @page = Page.find_by_slug('faqs')
  end

  def membership_plans
    @plans = Plan.public_plans
  end

  def request_invite
    if params[:plan_name].present? && params[:phone_number].present? && params[:email].present?
      Notifier.request_invite_email(params).deliver
      render json: { text: 'Thank you, your request has been sent. GP at Home will be in touch shortly...' }
    else
      render json: { text: 'Sorry, there was a problem, did you enter an email address and phone number?' }, status: :bad_request
    end
  end

  private

    def check_if_user
      redirect_to dashboard_path if user_signed_in? && !current_user.is_admin_or_gp?
    end

end
