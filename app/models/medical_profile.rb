class MedicalProfile < ActiveRecord::Base

  belongs_to :user

  attr_accessible :medical_history, :family_history, :regular_medication, :allergies, :smoking,
                  :alcohol_units, :exercise, :height, :weight, :insurance_company, :policy_number,
                  :london_insurance, :read_and_understood

end
