class ConsultationObserver < ActiveRecord::Observer

  observe :consultation

  def after_create(consultation)
    user = consultation.user
    Notifier.new_consultation_email(user).deliver
  end

end