class Tag < ActiveRecord::Base
  attr_accessible :name

  # associations
  has_many :taggings, :dependent => :destroy

  # attachments
  has_many :attachments, :through => :taggings, :source => :taggable, :source_type => 'Asset'

end