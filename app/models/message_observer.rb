class MessageObserver < ActiveRecord::Observer

  observe :message

  def after_create(message)

    return if message.consultation.messages.count == 1

    user = message.user
    consultation_owner = message.consultation.user

    # if this is for a dependent then set the user back to the account holder
    if consultation_owner.is_dependent?
      consultation_owner = consultation_owner.group.admin
    end

    if user.is_admin_or_gp?
      gp = user
      Notifier.new_message_user_email(consultation_owner, gp).deliver
    else
      Notifier.new_message_admin_email(consultation_owner).deliver
    end

  end

end
