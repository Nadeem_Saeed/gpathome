class Asset < ActiveRecord::Base

  # polymorphic association so that other models can use this to store attachments
  belongs_to :attachable, :polymorphic => true

  has_many :taggings, :as => :taggable, :dependent => :destroy
  has_many :tags, :through => :taggings, :dependent => :destroy

  delegate :url, :to => :attachment

  attr_accessible :tag_list, :attachable_type, :attachable_id, :attachable_file_name, :attachment

  has_attached_file :attachment,
    :styles => {
      :thumb => '100x100#',
      :original => '600x600'
    }

  before_post_process :skip_file_type

  # TODO: Add file type / size / amount validations

  def file_type
    case self.attachment_content_type
    when /image/
      :image
    when /application/
      :file
    end
  end

  def doc_type
    case self.attachment_content_type
    when /pdf/
      :pdf
    else
      :msword
    end
  end

  # tags
  # TODO: Abstract this out into it's own module if it's going to be used elsewhere
  def tag_list=(names)
    self.tags = names.split(',').map do |n|
      Tag.where(name: n.strip).first_or_create!
    end
  end

  def tag_list
    tags.map(&:name).join(', ')
  end

  private

    # skip processing:
    # .pdf, .doc, .docx
    def skip_file_type
      ! %w(application/pdf application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document).include?(attachment_content_type)
    end

end
