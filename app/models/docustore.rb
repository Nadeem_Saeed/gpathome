class Docustore < ActiveRecord::Base
  belongs_to :user

  # ability to find all assets
  has_many :assets, :as => :attachable, :dependent => :destroy

  attr_accessible :user_id
end
