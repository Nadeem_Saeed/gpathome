class AccountCredit < ActiveRecord::Base
  # AccountCredit model handles the balance for the user
  belongs_to :user
  belongs_to :group
  belongs_to :credit

end