class User < ActiveRecord::Base

  include Gpathome::Roleable
  include Gpathome::Creditable
  include Gpathome::Paymentable

  # TODO: Disallow old passwords to be used again
  # TODO: Inforce secure password > 8 characters, upper case, lower case, numeric

  delegate :full_name, to: :profile

  # used later to attribute user to plan
  attr_accessor :plan

  belongs_to :group

  has_many :consultations, :dependent => :destroy
  has_many :messages

  has_one :docustore

  # admin user that it's assigned to
  has_many :assigned_to, :class_name => 'Consultation', :foreign_key => 'assigned_to_id'

  has_one :profile, :dependent => :destroy

  has_one :medical_profile, :dependent => :destroy

  has_many :recurly_payments
  has_many :card_payments

  has_many :feedbacks, :dependent => :destroy

  # now we can manage profile & notifications through user model
  accepts_nested_attributes_for :profile, :medical_profile, :recurly_payments,
                                :card_payments

  # TODO: Fix timeout of user session
  # Include default devise modules
  devise  :database_authenticatable, :registerable, :recoverable, :trackable,
          :invitable, :timeoutable

  # setup accessible (or protected) attributes for your model
  attr_accessible :email,
                  :password,
                  :password_confirmation,
                  :remember_me,
                  :approved,
                  :group_id,
                  :plan,
                  :accepted_terms,
                  :invited_by_id,
                  :profile_attributes,
                  :notification_attributes,
                  :medical_profile_attributes,
                  :recurly_payments_attributes,
                  :card_payments_attributes


  # scopes
  scope :awaiting_approval, where(:approved => false, :active => true)

  # validators
  # using with_options here as we only want to run these if we're creating user that can login
  with_options :unless => :is_dependent? do |user|
    user.validates_presence_of      :password, :if => :password_required?
    user.validates_confirmation_of  :password, :if => :password_required?
    user.validates_length_of        :password, :within => 8..24, :allow_blank => true
    user.validates :email, :presence => true, :email => true, :uniqueness => true
  end

  validates_inclusion_of :accepted_terms, :in => [true]

  def group_users_remaining?
    group.plan.max_users - group.users.count
  end

  def is_invited_account_holder?
    invited_to_sign_up? && role == 2
  end

  def is_premier_plan_user?
    group.plan.name == 'Premier'
  end

  # Devise

  def active_for_authentication?
    super && approved? && active?
  end

  def active?
    active
  end

  # message for an inactive account
  def inactive_message
    if !approved?
      :not_approved
    elsif !active?
      :disabled
    else
      super # Use whatever other message
    end
  end

  def approve!
    self.approved = true
    self.save!
  end

  def unapprove!
    self.approved = false
    self.save!
  end

  private

    # from devise, see: https://github.com/plataformatec/devise/blob/master/lib/devise/models/validatable.rb:53
    def password_required?
      !persisted? || !password.nil? || !password_confirmation.nil?
    end

end
