class Consultation < ActiveRecord::Base

  belongs_to :user
  belongs_to :assigned_to, :class_name => 'User'

  validates_presence_of :subject

  has_many :messages, :dependent => :destroy

  accepts_nested_attributes_for :messages

  has_many :assets, :as => :attachable, :dependent => :destroy

  STATUSES = {
    0 => 'New',
    1 => 'In Progress',
    2 => 'Closed'
  }

  def status?
    STATUSES[status]
  end

  def closed?
    if status == 2
      true
    else
      false
    end
  end

  def from?
    messages.last.consultation.user.profile.first_name
  end

  def read_by_user!
    self.update_attribute(:read_by_user, Time.now)
  end

  def read_by_gp!
    self.update_attribute(:read_by_gp, Time.now)
  end

  def read_by_gp?
    if read_by_gp.nil?
      false
    else
      read_by_gp < messages.last.created_at && read_by_gp != messages.last.created_at
    end
  end

  def read_by_user?
    if read_by_user.nil?
      false
    else
      read_by_user < messages.last.created_at && read_by_user != messages.last.created_at
    end
  end

end
