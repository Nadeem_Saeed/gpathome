class Message < ActiveRecord::Base

  after_create :attach_attachment

  belongs_to :consultation
  belongs_to :user

  # ability to find all assets
  has_many :assets, :as => :attachable, :dependent => :destroy

  # validators
  validates_presence_of :message

  # virtial attribute for attachments
  attr_accessor :attachments

  private

    def attach_attachment
      self.attachments.split(',').each do |asset|
        a = Asset.find(asset.to_i)
        a.update_attributes(:attachable_id => self.id, :attachable_type => 'Message')
      end
    end

end