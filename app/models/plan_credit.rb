class PlanCredit < ActiveRecord::Base
  
  # this is the join model and has things like how many credits each plan has etc
  belongs_to :plan
  belongs_to :credit
  
  def credit_name
    Credit.find_by_id(credit_id).name
  end

end
