class Plan < ActiveRecord::Base

  has_many :groups

  has_many :plan_credits, :dependent => :destroy
  has_many :credits, :through => :plan_credits

  # whitelist of attributes
  attr_accessible :name, :plan_code, :description, :price, :status, :max_users, :plan_credits_attributes

  accepts_nested_attributes_for :plan_credits

  scope :public_plans, -> { where(status: [1,2]).order(:price) }

  # status codes mapped out
  STATUSES = {
    0 => 'Retired',
    1 => 'Live',
    2 => 'Promoted',
    3 => 'Private'
  }

  # virtual attribute for statuses - returns 'human' status codes
  def status?
    STATUSES[status]
  end

end
