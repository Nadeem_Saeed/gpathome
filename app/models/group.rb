class Group < ActiveRecord::Base

  # Group belongs to a plan
  # Reason: because the group will share the plans credits etc
  # Example: Couple plan, one admin of the group and a regular user,
  # both sharing plan credits
  belongs_to :plan

  belongs_to :admin, class_name: 'User', foreign_key: 'admin_id'

  # Group has many users
  # one of which will be the admin for the group (only one account holder)
  has_many :users

  has_many :account_credits

  attr_accessible :user_attributes, :admin_id, :plan_id, :group_type

  accepts_nested_attributes_for :users, :account_credits

end
