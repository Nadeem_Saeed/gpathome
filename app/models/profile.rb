class Profile < ActiveRecord::Base

  belongs_to :user

  # protected attributes
  attr_accessible :first_name, :last_name, :gender, :date_of_birth, :phone_1, :property_number,
                  :street_address, :extended_address, :locality, :region, :postal_code

  attr_accessor :skip_profile_validation, :validate_names

  def skip_profile_validation?
    @skip_profile_validation ||= false
  end

  def validate_names?
    @validate_names ||= false
  end

  # validators
  with_options unless: Proc.new{ |a| a.skip_profile_validation? } do |p|
    p.validates_presence_of :first_name, :last_name, :phone_1, :date_of_birth,
                          :street_address, :postal_code
    p.validates_length_of :phone_1, :in => 7..32
  end

  with_options if: Proc.new{ |a| a.validate_names? } do |p|
    p.validates_presence_of :first_name, :last_name
  end

  # SEE: http://stackoverflow.com/questions/3368016/rails-on-ruby-validating-and-changing-a-phone-number
  def phone_1=(num)
    num.gsub!(/\D/, '') if num.is_a?(String)
    super(num)
  end

  # if the phone number is not blank pad out the front of the integer with a 0
  # and insert a space in the middle
  def phone_1
    return "0#{super}".insert(5, ' ') unless super.blank? || super.zero?
    super
  end

  def full_name
    "#{self[:first_name]} #{self[:last_name]}"
  end

  def gender_humanize
    return '' if self.gender.nil?
    self.gender ? 'Male' : 'Female'
  end

  # virtual attribute to return complete address as hash
  def full_address
    {
      street_address:   self.street_address,
      extended_address: self.extended_address,
      locality:         self.locality,
      region:           self.region,
      postal_code:      self.postal_code
    }
  end

end
