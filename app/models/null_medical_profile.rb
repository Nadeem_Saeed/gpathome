class NullMedicalProfile

  def height
    nil
  end

  def weight
    nil
  end

  def smoking
    nil
  end

  def london_insurance
    nil
  end

  def method_missing(m, *args, &block)
    ''
  end

end