class CardPayment < ActiveRecord::Base
  attr_accessible :amount, :payment_at, :payment_term
  belongs_to :user

  validates_numericality_of :amount
  validates_presence_of :payment_term

  PAYMENT_TERMS = {
    6  => '6 months',
    12 => '12 months'
  }

  def expiry_date
    (payment_at + payment_term.months).strftime("%d/%m/%Y")
  end
end
