class UserObserver < ActiveRecord::Observer

  observe :user

  def after_update(user)
    return if user.is_account_holder?

    if user.approved_changed? && user.approved? && !user.is_dependent?
      Notifier.approved_email(user).deliver
    end
  end

  def after_create(user)
    account_setup(user)
    return if user.is_invited_account_holder?
    notify_admins(user)
    notify_user(user)
  end

  private

    def account_setup(user)
      setup_class = "Gpathome::AccountSetup::#{user.role_to_class}"
      setup_class.constantize.new(user).setup
    end

    def notify_admins(user)
      Notifier.new_user_email(user).deliver
    end

    def notify_user(user)
      Notifier.welcome_email(user).deliver if user.is_account_holder?
    end

end
