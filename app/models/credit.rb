class Credit < ActiveRecord::Base

  has_many :plan_credits
  has_many :plans, :through => :plan_credits

  has_many :account_credits
  has_many :users, :through => :account_credits

end