class Ability
  include CanCan::Ability

  def initialize(user)

    # See the wiki for details: https://github.com/ryanb/cancan/wiki/Defining-Abilities

    user ||= User.new  # guest user (not logged in)

    if user.new_record?

      cannot :manage, :all

      # this might be a new legitimate user so let them accept their invitation
      can [:update, :accepted], :invitation

    else

      case user.role?

      when 'admin'
        # an admin can manage everything
        can :manage, :all

      when 'gp'
        # gp's can do most things, apart from manage users
        can :manage, :all
        cannot :manage, Plan
        cannot [:destroy, :edit], Consultation

      when 'account holder'
        # super user / account holder role

        # inherit the user role permissions
        user_role(user)

        can :show, Group if user.group.plan_id != 1

        # build on those permissions
        can :create, User

        can :top_up, Credit

        can :switch, :plan

        # ability to allow changing billing details for the account
        can :billing_details, :account

        can [:edit, :update], Profile do |profile|
          profile.user.group_id === user.group.id
        end

        can [:edit, :update], MedicalProfile do |medical_profile|
          medical_profile.user.group_id === user.group.id
        end

        can [:index, :show, :create], Consultation do |consultation|
          consultation.user.group.id === user.group_id
        end

        # allow sending out invites
        can :manage, :invitation

      when 'authorised'

        # user permissions
        user_role(user)

        can [:index, :show, :create], Consultation do |consultation|
          consultation.user.group.id === user.group_id
        end

        # build on those permissions
        #can [:create], User
        can :update, Profile

      when 'account member'
        # user permissions
        user_role(user)

      end

    end

  end

  def user_role(user)
    # users can view their dashboard
    can :index, :dashboard

    # users can only do a limited number of things
    can [:index, :show, :create], Consultation, :user_id => user.id
    can [:show, :edit, :update, :create], Profile, :user_id => user.id
    can [:show, :edit, :update, :create], MedicalProfile, :user_id => user.id
    can [:index, :show, :create, :check_for_messages], Message, :user_id => user.id

    # can add new attachments
    can [ :create ], Asset

    # users can view their account
    can :show, :account

    # users can edit their own passwords
    can [:edit, :update], :password

    # users can top up their credits
    can [:index, :show], Credit, :user_id => user.id

    can [:edit, :update, :accepted], :invitation
  end

end
