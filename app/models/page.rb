class Page < ActiveRecord::Base
  attr_accessible :body, :slug, :visible, :name
end
