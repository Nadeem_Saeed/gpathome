class MedicalProfilePresenter

  def initialize(user)
    @user = user
    @medical_profile = @user.medical_profile || NullMedicalProfile.new
  end

  def height
    return '' if @medical_profile.height.nil?
    "#{@medical_profile.height} cm"
  end

  def weight
    return '' if @medical_profile.weight.nil?
    "#{@medical_profile.weight} kg"
  end

  def smoking
    bool_to_human(@medical_profile.smoking)
  end

  def alcohol_units
    @medical_profile.alcohol_units
  end

  def full_name
    @user.full_name
  end

  def phone_number
    return '' if @user.profile.phone_1.nil?
    "0#{@user.profile.phone_1}".insert(4, ' ')
  end

  def gender
    @user.profile.gender_humanize
  end

  def date_of_birth
    @user.profile.date_of_birth.strftime('%b. %d, %Y') if @user.profile.date_of_birth
  end

  def medical_history
    @medical_profile.medical_history
  end

  def family_history
    @medical_profile.family_history
  end

  def regular_medication
    @medical_profile.regular_medication
  end

  def allergies
    @medical_profile.allergies
  end

  def exercise
    @medical_profile.exercise
  end

  def insurance_company
    @medical_profile.insurance_company
  end

  def policy_number
    @medical_profile.policy_number
  end

  def london_insurance
    bool_to_human(@medical_profile.london_insurance)
  end

  private

  def bool_to_human(s)
    return '' if s.nil?
    s ? 'Yes' : 'No'
  end

end