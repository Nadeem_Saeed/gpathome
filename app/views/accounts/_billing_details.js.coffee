jQuery ($) ->
  
  # Recurly Form

  # Form re-formatting - we can override anything here including form labels etc
  reformat = (formEl) ->
    $(formEl)
      .find('.submit')
      .text('Update details')

  Recurly.config
    subdomain:  'gpathome'
    currency:   'GBP'
    
  Recurly.buildBillingInfoUpdateForm
    target:         '#recurly-form'
    enableCoupons:  false
    planCode:       '<%= @payment[:plan_code] %>'
    successURL:     '/user/account/billing_details'
    signature:      '<%= @payment[:signature] %>'
    beforeInject:   reformat
    accountCode:    '<%= @payment[:account_code] %>'
    account: 
      firstName:    '<%= @payment[:first_name] %>'
      lastName:     '<%= @payment[:last_name] %>'
      email:        '<%= @payment[:email] %>'
    billingInfo:
      firstName:    '<%= @payment[:first_name] %>'
      lastName:     '<%= @payment[:last_name] %>'
      address1:     '<%= @payment[:address1] %>'
      address2:     '<%= @payment[:address2] %>'
      city:         '<%= @payment[:city] %>'
      zip:          '<%= @payment[:zip] %>'
      state:        '<%= @payment[:state] %>'
      country:      '<%= @payment[:country] %>'
