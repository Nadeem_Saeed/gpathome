jQuery ($) ->
  
  # Recurly Form

  # Form re-formatting - we can override anything here including form labels etc
  reformat = (formEl) ->
    $(formEl)
      .find('.submit')
      .text('Top Up')

  Recurly.config
    subdomain:  'gpathome'
    currency:   'GBP'
    
  Recurly.buildTransactionForm
    target:       '#recurly-form'
    enableCoupons: false
    successURL:   '/user/credits/1/top_up'
    signature:    '<%= @payment[:signature] %>'
    beforeInject: reformat
    account:
      firstName:  '<%= @payment[:first_name] %>'
      lastName:   '<%= @payment[:last_name] %>'
      email:      '<%= @payment[:email] %>'
    billingInfo:
      firstName:  '<%= @payment[:first_name] %>'
      lastName:   '<%= @payment[:last_name] %>'
      address1:   '<%= @payment[:address1] %>'
      address2:   '<%= @payment[:address2] %>'
      city:       '<%= @payment[:city] %>'
      zip:        '<%= @payment[:zip] %>'
      state:      '<%= @payment[:state] %>'
