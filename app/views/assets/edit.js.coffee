$mc = $('body').find('.modal-container')

if $mc.length
  $mc.html('<%= j render "tag" %>')
else
  $('body').append """
  <div class="modal-container">
    <%= j render "tag" %>
  </div>
  """
$('.tags_modal').modal()