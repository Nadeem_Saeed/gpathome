class Notifier < ActionMailer::Base

  def admin_email_address
    @admin_email ||= ADMIN_EMAIL
  end

  def practice_email_address
    @practice_email ||= PRACTICE_EMAIL
  end

  default from: ADMIN_EMAIL

  # new user email to admins when a user signs up
  def new_user_email(user)
    @user = user
    mail :to => admin_email_address,
         :subject => 'GP at Home - User Awaiting Approval'
  end

  def welcome_email(user)
    @user = user
    mail :to => @user.email,
         :subject => 'Welcome to GP at Home'
  end

  def request_invite_email(details)
    @email        = details[:email]
    @plan_name    = details[:plan_name]
    @phone_number = details[:phone_number]
    @first_name   = details[:first_name]
    @last_name    = details[:last_name]
    mail :to => practice_email_address,
         :subject => 'New patient requesting invite'
  end

  def approved_email(user)
    @user = user
    mail :to => @user.email,
         :subject => 'GP at Home account has been approved'
  end

  def new_consultation_email(user)
    @user = user
    mail :to => admin_email_address,
         :subject => 'GP at Home - New Consultation'
  end

  def new_message_user_email(user, gp)
    @gp_name = gp.profile.last_name
    mail :to => user.email,
         :subject => 'New message waiting for you at GP at Home'
  end

  def new_message_admin_email(user)
    @user = user
    mail :to => admin_email_address,
         :subject => 'GP at Home - New Message'
  end

end
