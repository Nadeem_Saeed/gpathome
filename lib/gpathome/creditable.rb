module Gpathome::Creditable

  extend ActiveSupport::Concern

  included do

    # account credits either for this user or for his group
    has_many :account_credits
    has_many :credits, :through => :account_credits

    accepts_nested_attributes_for :account_credits

  end

  def total_topped_up_credits
    group_credits = self.group.account_credits.count
    user_credits = self.account_credits.count
    group_credits + user_credits
  end

  def consultations_within_this_year
    consultations.where(:created_at => date_range(self)).count
  end

  def plan_credits
    group.plan.plan_credits
  end

  def number_of_plan_credits
    plan_credits.find_by_credit_id(Credit.first.id).number_of_credits
  end

  def number_of_group_credits
    group.account_credits.sum(:quantity)
  end

  def number_of_user_credits
    account_credits.sum(:quantity)
  end

  def number_of_online_consultations
    number_of_plan_credits + number_of_group_credits + number_of_user_credits
  end

  def remaining_online_consultations
    number_of_online_consultations - consultations_within_this_year
  end

  private

    def date_range(user)
      # if the date is in the past then count forwards, if in the future, count backwards
      c = user.group.created_at
      date_line = c.change(:year => Time.now.year)
      if date_line.future?
        # then the subscription must rollover later
        date_line.change(:year => Time.now.year-1)..date_line
      else
        # then we count backwards
        date_line..date_line.change(:year => Time.now.year+1)
      end
    end

end