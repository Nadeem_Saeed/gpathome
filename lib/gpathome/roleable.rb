module Gpathome::Roleable

  extend ActiveSupport::Concern

  included do

    # user roles mapped out
    ROLES = {
      1 => 'account member',
      2 => 'account holder',
      3 => 'gp',
      4 => 'admin',
      5 => 'authorised',
      6 => 'dependent'
    }

    # scopes
    scope :admins, where(:role => 4)
    scope :gps, where(:role => 3)
    scope :account_holders, where(:role => 2)
    scope :user, where(:role => 1)
    scope :authorised, where(:role => 5)
    scope :dependents, where(:role => 6)
    scope :dependents_and_self, lambda { |user|
      where('(role = ? OR id = ?) AND group_id = ?', 6, user.id, user.group_id)
    }

    attr_accessible :role

    ROLES.each do | key, role_name |
      define_method "is_#{role_name.gsub(/\s/, '_')}?" do
        role == key
      end
    end

    def self.top_level_roles
      ROLES.select { |r| [3,4].include? r  }
    end

  end

  # virtual attribute for roles - returns 'human' role codes
  # OPTIMIZE: Change method attribute name as this is incorrect in Ruby
  def role?
    ROLES[role]
  end
  alias role_to_s role?

  # group of methods to check user roles

  def is_admin_or_gp?
    role == 3 || role == 4
  end

  def show_dependent_consultations?
    role == 2 || role == 5
  end

  def role_to_class
    role_to_s.gsub(/\s/, '_').classify
  end

end
