module Gpathome::Paymentable

  def last_card_payment_date
    card_payments.last.payment_at
  end

  def within_payment?
    last_card_payment_date > 11.months.ago
  end

end
