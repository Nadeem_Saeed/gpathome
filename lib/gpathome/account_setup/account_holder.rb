module Gpathome::AccountSetup
  class AccountHolder < Gpathome::AccountSetup::Base

    def setup
      assign_to_group
      build_medical_profile
      account.save!
    end

    private

    def assign_to_group
      group = Group.create(:group_type => 'user', :plan_id => account.plan, :admin_id => account.id)
      account.group_id = group.id
    end

    def build_medical_profile
      # we don't collect any medical info on signup so we have to manually create a new med profile
      account.build_medical_profile
    end

  end
end
