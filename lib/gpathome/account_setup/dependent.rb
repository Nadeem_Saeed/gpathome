module Gpathome::AccountSetup
  class Dependent < Gpathome::AccountSetup::Base

    def setup
      assign_to_group
      build_medical_profile
      account.save!
    end

    private

      def assign_to_group
        account.group_id = User.find(account.invited_by_id).group.id
      end

      def build_medical_profile
        account.build_medical_profile
      end

  end
end