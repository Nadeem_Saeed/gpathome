module Gpathome::AccountSetup
  class Gp < Gpathome::AccountSetup::Base

    def setup
      assign_to_group
      approve_user
      account.save!
      account.send_reset_password_instructions
    end

    private

    def assign_to_group
      group = Group.find_or_create_by_group_type('gp', { :plan_id => 0 })
      account.group_id = group.id
    end

    def approve_user
      account.approved = true
      account.active = true
      account.accepted_terms = true
    end

  end
end
