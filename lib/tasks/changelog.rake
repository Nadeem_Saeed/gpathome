#
# Created by JD - 18/04/12
#
# Description: Generates changes.html from Changelog.md
# relies on maruku for converting Markdown in to HTML
#

namespace :changelog do
  desc "Creates changelog HTML in /public/"
  task :create do
    sh "maruku Changelog.md -o public/changes.html"
    sh "launchy public/changes.html"
    puts "Changelog HTML file created :)"
  end
end