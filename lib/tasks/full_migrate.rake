#
# Created by JD - 04/05/12
#
# Description: Runs rake db:migrate and rake db:test:prepare
#

desc "Runs rake db:migrate and rake db:test:prepare"
task :full_migrate do
  sh "rake db:migrate && rake db:test:prepare"
end
