module Invites
  class Individual < Base
    def self.invite(klass, plan_klass)
      klass.plan = plan_klass.find_by_name('Individual').id
      klass = self.set_base_attributes(klass)
      klass.invite!
      klass
    end

  end
end
