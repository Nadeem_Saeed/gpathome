module Invites
  class Couple < Base
    def self.invite(klass, plan_klass)
      klass.plan = plan_klass.find_by_name('Couple').id
      klass = self.set_base_attributes(klass)
      klass.invite!
      klass
    end
  end
end
