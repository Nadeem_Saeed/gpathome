module Invites
  class Base
    def self.set_base_attributes(klass)
      klass.role = 2
      klass.profile.skip_profile_validation = true
      klass.profile.validate_names = true
      klass.accepted_terms = true
      klass.approved = true
      klass
    end
  end
end
