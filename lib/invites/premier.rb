module Invites
  class Premier < Base

    def self.invite(klass, plan_klass)
      @klass = klass
      @klass.plan = plan_klass.find_by_name('Premier').id
      @klass.invite!
      @klass
    end

  end
end
