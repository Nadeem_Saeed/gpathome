Title: Changelog for GP at Home
CSS: simple_style.css

# Changelog for GP at Home
Last updated: 13/12/2013

  <hr>

### Version 20
Released 13/12/2013

- Upgraded Rails to v3.2.16
- Updated how user signups are handled

### Version 19
Released 03/04/2013

- Added local resizing of image attachments on browsers that support it

### Version 18
Released 04/02/2013

- Attachment upload now works on Android Tablet, Android Phone, Internet Explorer 7, 8, 9, Firefox and Safari
 

### Version 17
Released 27/02/2013

- Added ability to attach files to consultations and messages
- Added ability to tag attachments
- Changed design of admin control panel for consultation replies


### Version 16
Released 14/01/2013

- Upgraded Rails to v3.2.11 see: [http://weblog.rubyonrails.org/2013/1/8/Rails-3-2-11-3-1-10-3-0-19-and-2-3-15-have-been-released/](http://weblog.rubyonrails.org/2013/1/8/Rails-3-2-11-3-1-10-3-0-19-and-2-3-15-have-been-released/)


### Version 15
Released 18/12/2012

- Made Date of Birth, first line of address and postal code mandatory
- Allowed GPs to edit user profiles
- Added ability to credit users (through their groups) credits


### Version 14
Released 31/07/2012

- Added insurance details to medical profiles page


### Version 13
Released 11/07/2012

- Corrected error checking in Intelligent-ID module, also updated RSpec test

### Version 12
Released 29/06/2012

- Added company address on contact page for SagePay hold condition
- Rerouted payments through Recurly hosted payment pages
- Updated Recurly's hosted payment page to show our logo and colour scheme
- Disabled additional credit functionality due to hosted payment pages not supporting one off payments

### Version 11
Released 25/06/2012

- Added coupons to sign up page
- Corrected AR relationship for account credit histories
- Amended IE7 CSS for payment form


### Version 10
Released 19/06/2012

- Amended ordering of patient consultations on user dashboard
- Amended permissions for viewing consultations
- Amended new consultation email 'there' changed to 'their'


### Version 9
Released 18/06/2012

- Fixed correct person icon to show up when different types of users are using the site
- Added partial patient medical profile to right of consultation reply screen for quick access for GPs
- Added link to full profile of patient inside GPs consultation screen


### Version 8
Released 15/06/2012

- Added credit for JK Photography
- Amended our services page
- Removed 'use_salt_as_remember_token' from devise.rb as it's deprecated
- Updated Bootstrap-SASS to latest version 2.0.3.1
- Updated formatting for password input on IE7-8 - it was showing no password
- Updated admin dashboard and consultation screen to only show open consultations
- Updated admin consultation view to show previous consultation with patient
- Updated admin user screen to display roles as text
- Updated admin dashboard screen to allow easier user approval


### Version 7
Released 31/05/2012

- Added - Timeout for user sessions - currently set to 30 minutes
- Added - Validation of Date of Birth on Medical Profile


### Version 6
Released 31/05/2012

- Installed - Recurly gem
- Added - Recurly.js for payments page
- Added - Recurly.js for credits page
- Added - Ability to upgrade/downgrade plan
- Added - New message notification via AJAX when users are on consultation screen
- Removed - Limit on credit top up
- Added - Ability to pay on sign up
- Added - Ability to top up credits with Recurly payments
- Added - Ability to amend payment details for plan payments
- Design - Changed payment form design, removed 'Required labels' for now
- Added - People can't downgrade plans for now
- Created - Module for handling Intelligent-ID
- Added field for property number
- Fixed - Spelling on payment page
- Fixed - Test to allow for Intelligent-id auto-approving users
- Amended - Email copy so that it includes 'Dr X'
- Amended - all emails to include correct https address for site
- Fixed - Emails sent to just assigned Doctor not to all Doctors
- Added - Typical scenarios page
- Added - Weight & Height to Medical Profile
- Fixed - Active page marker for Medical Profiles page
- Added - Confirmation modal for users upgrading plans
- Added - Images to Take a Tour page
- Added - Images to Our Services page
- Added - Images to About Us page
- Fixed - Layout on About Us page for GP profiles
- Installed - New Relic monitoring



### Beta - Version 5
Released 04/05/2012

- Added - Roles for different users: 'authorized', and dependent
- Added - Additional scopes to User model for new roles
- Added - Ability to add dependent account
- Added - Ability to edit dependent profile
- Added - Ability to edit dependent medical profile
- Added - Ability to send consultation on behalf of a dependent
- Added - Ability to reply to consultations on behalf of a dependent
- Fix - Normal users aren't allowed to top up credits, account holder must do this
- Added - FAQs - 'circumstances permitting' under same doctor section
- Removed - Notifications screen and dropped table
- Removed - Secondary phone number from profile
- Removed - Notice under address field for visiting people
- Fix - Some small formatting errors on pages
- Added - Profile - Full address fields, street_address, extended_address, locality, region, postal_code
- Added - Signup - Full address


### Beta - Version 4
Released 30/04/2012

- Added - Contact Us - Telephone number added
- Changed - Twitter logo in footer


### Beta - Version 3
Released 27/04/2012

- Design - Membership Plans - Changed button text and plan layout
- Fix - Medical Profile - Changed number of alcohol_units to a select box - updated MedicalProfile model to reflect this
- Added - If user isn't logged in then the feedback form will ask for a contact email
- Added - Terms & Conditions
- Added - Checkbox to accepts Terms & Conditions


### Beta - Version 2
Released 19/04/2012

- Added - Contact Us - Harley Street address
- Added - Footer - Twitter logo & link
- Fix - FAQs - Links added for GP at 92 and Inteligent-ID
- Fix - Plans - Plans are now all one height
- Fix - Plans - Added extra padding to plans
- Fix - Plans - Corrected styling for monthly price
- Fix - Changed all instances of Sign in to Login (all one word), everything's now consistent
- Fix - Home page - Changed layout of 'Sign Up' and 'Take a Tour'
- Added - Link to about us page
- Added - About Us - Image of Fiona and Justine
- Added - Footer - Logo & link to Intelligent-ID
- Added - Droid Sans typeface via @font-face (this was disabled previously) - loading from Google's Webfonts via HTTPS
- Fix - Corrected group model to allow group_type to be updated
- Removed - Removed the 'Remember me option on login', updated User model to reflect this change
- Added - Admin - Link to homepage (title is not a link)
- Added - Admin - User - Plan name to user's profile page
- Fix - User now locked into Dashboard - cannot view front of site. Admin can view both
- Fix - Admin - Can 'unapprove' user
- Fix - User - Active page state design changed for sub page of Account or Profiles

Looking for the main site? [Click here](https://www.gpathome.com).
